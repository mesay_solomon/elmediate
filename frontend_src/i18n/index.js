import Vue from 'vue'
import VueI18n from 'vue-i18n'
import store from '../store'

import {en} from '../assets/js/i18n/en'

Vue.use(VueI18n)

var currentLocale = store.state.persistedState.lang ? store.state.persistedState.lang : 'en'

var i18n = new VueI18n({
    locale: currentLocale,
    messages: store.state.persistedState.messages
})

export default i18n
