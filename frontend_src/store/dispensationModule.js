export default {
    state:{
        currentClient: {},
        numberOfDays: 0,
        regimenProducts: [],
        productBatches: [],
        prescriptionProducts:[]
    },
    mutations:{
        setCurrentClient(state, value){
            state.currentClient = value
        },
        setNumberOfDays(state, value){
            state.numberOfDays = value
        },
        setRegimenProducts(state, value){
            state.regimenProducts = value
        },
        setProductBatches(state, value){

            var matchWasFound = false
            if(state.productBatches){
                for(var i=0; i<value.length;i++){
                    for(var j=0; j<state.productBatches.length;j++){
                        if(value[i].productCode === state.productBatches[j].productCode){
                            state.productBatches[j].batches = value[i].batches
                            matchWasFound = true
                            continue
                        }
                    }
                    if(!matchWasFound){
                        state.productBatches.push(value[i])
                    }
                    matchWasFound = false
                }
            }else{
                state.productBatches = value
            }

        },
        setPrescriptionProducts(state, value){
            state.prescriptionProducts = value
        }
    },
    actions:{
        setCurrentClient({commit}, value){
            commit('setCurrentClient', value)
        },
        setNumberOfDays({commit}, value){
            commit('setNumberOfDays', value)
        },
        setRegimenProducts({commit}, value){
            commit('setRegimenProducts', value)
        },
        setProductBatches({commit}, value){
            commit('setProductBatches', value)
        },
        setPrescriptionProducts({commit}, value){
            commit('setPrescriptionProducts', value)
        }
    },
    getters:{
        productBatches: state=>{
            return state.productBatches
        },
        currentClient : state=>{
            return state.currentClient
        },
        prescriptionProducts: state=>{
            return state.prescriptionProducts
        }
    }
}

