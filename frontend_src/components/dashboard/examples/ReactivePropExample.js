import BarChart from 'vue-chartjs/src/BaseCharts/Bar'
import reactiveProp from 'vue-chartjs/src/mixins/reactiveProp'

export default {
  extends: BarChart,
  mixins: [reactiveProp],

  mounted () {
    this.renderChart(this.chartData, {responsive: true, maintainAspectRatio: false})
  }
}
