// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import i18n from './i18n'
import VueResource from 'vue-resource'
import store from './store'
import App from './App'
import router from './router'
import Navigation from './components/layout/Navigation'
import ProductsTable from './components/layout/ProductsTable'
import TransactionProductBatch from './components/layout/TransactionProductBatch'
import ProgressIndicator from './components/layout/ProgressIndicator'
import Alert from './components/layout/Alert'
import Datepicker from './components/common/Datepicker'
import LocaleSelector from './components/layout/LocaleSelector'
import moment from 'moment'
import VeeValidate from 'vee-validate'
import { sync } from 'vuex-router-sync'
import vSelect from 'vue-select'

import FadeLoader from 'vue-spinner/src/ScaleLoader'
import underscore from 'vue-underscore'
require('../node_modules/bootstrap/dist/css/bootstrap.css')
require('../node_modules/font-awesome/css/font-awesome.css')
require('./assets/css/global/main.css')
require('./assets/css/global/menu.css')
require('./assets/css/sb-admin.css')
require('./assets/css/AdminLTE.min.css')
require('../node_modules/vue-multiselect/dist/vue-multiselect.min.css')

Vue.config.productionTip = true

Vue.filter('formatDate', function(value) {
  if (value != null) {
    return moment(new Date(value) ).format('YYYY-MM-DD')
  }else {
      return 'N/A'
  }
})

Vue.filter('formatDateSimple', function(value) {
    if (value != null) {
        return moment(new Date(value) ).format('MM/DD/YY')
    }else {
        return 'N/A'
    }
})



Vue.use(VueResource)
Vue.use(underscore);
Vue.use(VeeValidate)
Vue.component('v-select', vSelect)
/* eslint-disable no-new */
Vue.component('navigation', Navigation)
Vue.component('products-table', ProductsTable)
Vue.component('transaction-product-batch', TransactionProductBatch)
Vue.component('fade-loader', FadeLoader)
Vue.component('progress-indicator', ProgressIndicator)

Vue.component('alert', Alert)
Vue.component('localeSelector', LocaleSelector)
Vue.component('datepicker', Datepicker)
Vue.mixin({
    methods:{
        getProductBatchBalance(batchName){
            return Math.floor((Math.random() * 10) + 1);
        },
        hasPermission: function(rightName){

            return store.state.persistedState.userNodeRoleRights.includes(rightName)
        },
        today: function () {
            return moment(new Date()).format('YYYY-MM-DD')
        },
        parseDate: function(dateString, format){
            return moment(dateString, format)
        },
        printJSON: function(json){
            console.log( JSON.stringify(json, null, 3) )
        },

        isEmpty(obj){
            return Object.keys(obj).length === 0
        },
        isNullOrUndefined(obj){
            return obj===null || obj===undefined
        },
        timeTravel(direction, numberOfDays){
            var destinationTime = new Date()
            var distance = numberOfDays*24*60*60*1000
            if(direction === -1){
                destinationTime.setTime(destinationTime.getTime()-distance)
            }else{
                destinationTime.setTime(destinationTime.getTime()+distance)
            }
            return moment(destinationTime).format('DD-MMM-YYYY')
        },
        isNumber(event){
            var el = event.target
            if(parseFloat(el.value) === NaN){
                el.classList.add('is-danger')
            }else{
                el.classList.remove('is-danger')
            }
            console.log(el.classList)
        },
        isInstallationType(type){

            var installationType = store.state.persistedState.systemSettings.find(function (systemSetting) {
                return systemSetting.key === 'installation.type'
            })

            if(installationType){
                return installationType.value === type
            }
            return false
        },
        formatDate(value) {
            if (value != null) {
                return moment(new Date(value) ).format('MMM-DD-YYYY')
            }else {
                return 'N/A'
            }
        },
        toISODateFormat(value) {
            if (value != null) {
                return moment(new Date(value) ).format('YYYY-MM-DD')
            }else {
                return 'N/A'
            }
        }
    }
})

sync(store, router)

new Vue({
  el: '#app',
  store,
  router, i18n,
  template: '<App/>',
  components: { App }
});
