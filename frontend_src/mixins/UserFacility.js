
import {getHttpAuthenticated} from '../assets/js/http_with_token'
import {_} from 'vue-underscore';

var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        loadFacilities: function(){

            var geographicZoneId =context.$store.state.persistedState.facility.geographicZoneId;
            getHttpAuthenticated().get(" /all-facilities/"+geographicZoneId).then(function(response) {
                context.facilities = response.data
                context.facilities.forEach(value => {
                    value.label=value.name;
                    value.value=value.id;
                })
            }).catch(function(e) {
                context.http_errors.push(e)
            })
        }
    }

}
