import {getHttpAuthenticated} from '../assets/js/http_with_token'
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods:{
        loadAllAdjustemntTypes: function(){
            getHttpAuthenticated().get("/all-adjustments").then(function(response) {
                context.adjustmentTypeList = response.data

            }).catch(function(e) {
                context.http_errors.push(e)
            })
        }
    }
}
