import {getHttpAuthenticated, getHttpAuthenticatedWithLoadingFlag, getHttpAuthenticatedWithCallBack} from '../assets/js/http_with_token'
import {convertToFacilityApprovedProduct} from '../assets/js/products_helper'
import store from "../store";
var context = {}
export default{
    beforeMount(){
        context = this
    },
    methods : {
        loadNodeProducts: function(nodeId, programCode){
            if(programCode){
                getHttpAuthenticated().get("/facility-approved-node-products?nodeId=" + nodeId
                    + "&programCode="+programCode).then(function(response) {

                    var nodeProducts = response.data
                    context.baseProductList = []
                    for(var i=0;i<nodeProducts.length;i++){
                        context.baseProductList.push(convertToFacilityApprovedProduct(nodeProducts[i]))
                    }

                }).catch(function(e) {
                    console.log(e)
                })
            }

        },
        loadFacilityApprovedNodeProducts(nodeId, programCode){
            if(programCode){
                getHttpAuthenticatedWithCallBack(context.facilityApprovedNodeProductsLoaded)
                    .get("/facility-approved-node-products?nodeId=" + nodeId
                        + "&programCode="+programCode)
            }

        },
        facilityApprovedNodeProductsLoaded(nodeProducts){
            context.baseProductList = []
            var facilityCode  = store.state.persistedState.facility.code
            getHttpAuthenticatedWithLoadingFlag()
                .get("/facility-approved-products-in-program/" + facilityCode + '/' + context.programArea)
                .then(function(response) {
                var facilityApprovedProducts = response.data
                facilityApprovedProducts.forEach(fap => {
                    fap.id = fap.productId
                    fap.code = fap.productCode
                    fap.primaryName = fap.productName

                    var matchingNodeProduct = nodeProducts.find( np => {
                        return np.programProduct.product.code === fap.productCode
                    })

                    if(matchingNodeProduct!==undefined){
                        fap.nodeProduct = matchingNodeProduct
                    }

                    context.baseProductList.push(fap)
                })

            }).catch(function(e) {
                console.log(e)
            })
        }
    }
}
