import {getHttpAuthenticated, getHttpAuthenticatedWithLoadingFlag} from '../assets/js/http_with_token'
var context = {}
export default {
    beforeMount(){
        context = this
    },
    computed:{
        productBatchList(){
            var batch = {}
            var productBatchList = []
            for(var i=0;i<context.batchList.length;i++){
                batch = context.batchList[i]
                var batchLineItem = {
                    id: batch.id,
                    name: batch.name,
                    description: batch.description,
                    batchNumber: batch.name,
                    expiryDate: new Date(batch.expiryDate),
                    expiryDateInput: context.toISODateFormat(new Date(batch.expiryDate)),
                    quantity: '',
                    remark: '',
                    hasErrors: false,
                    adjustmentType:undefined
                }

                if(context.transactionProductsToEdit){

                    var editingTxnProduct = context.transactionProductsToEdit.find(function (txnProd) {
                        return txnProd.batchNumber === batchLineItem.batchNumber
                    })

                    if (editingTxnProduct) {
                        batchLineItem.quantity = editingTxnProduct.quantity
                        batchLineItem.remark = editingTxnProduct.remark
                        if(editingTxnProduct.adjustmentType)
                        {
                            batchLineItem.adjustmentType = editingTxnProduct.adjustmentType
                        }
                    }
                }

                if(context.selectedProduct.nodeProduct){
                    var nppbs = context.selectedProduct.nodeProduct.nodeProgramProductBatches
                    var nppbMatch = nppbs.find ((nppb) => {
                        return parseInt(nppb.programProductBatchId) === parseInt(batchLineItem.id)
                    })

                    if(nppbMatch){
                        batchLineItem.nppbId = nppbMatch.id
                        batchLineItem.programProductBatchId = nppbMatch.programProductBatchId
                    }


                    if(context.selectedProduct.balances!==undefined)
                    {
                        var nPProductBatchBalances = context.selectedProduct.balances.nodeProgramProductBatchBalances
                        var nPProductBatchBalanceMatch = nPProductBatchBalances.find(function (nPProductBatchBalance) {
                            return parseInt(nPProductBatchBalance.nodeProgramProductBatch.id) === parseInt(batchLineItem.nppbId)
                        })

                        if(nPProductBatchBalanceMatch){
                            batchLineItem.balance = nPProductBatchBalanceMatch.balance!==null ? parseFloat(nPProductBatchBalanceMatch.balance) : parseFloat(0)
                            batchLineItem.usableBalance = nPProductBatchBalanceMatch.usableBalance!==null ? parseFloat(nPProductBatchBalanceMatch.usableBalance) : parseFloat(0)
                        }else{
                            batchLineItem.balance = 0
                            batchLineItem.usableBalance = 0
                        }
                    }else{
                        batchLineItem.balance = 0
                        batchLineItem.usableBalance = 0
                    }

                }

                if(context.transactionType === 'ADJUSTMENT' || context.transactionType === 'PHYSICAL_COUNT'){
                    productBatchList.push(batchLineItem)
                }/*else if(context.transactionType === 'PHYSICAL_COUNT'){
                    if(batchLineItem.expiryDate > new Date().getTime() && batchLineItem.nppbId !== undefined)
                    {
                        productBatchList.push(batchLineItem)
                    }
                }*/else{
                    if(batchLineItem.expiryDate > new Date().getTime())
                    {
                        productBatchList.push(batchLineItem)
                    }
                }


            }
            return productBatchList
        }
    },
    methods:{
        getProductBatchesByProductCode(productCode){
            if(productCode === undefined){
                context.batchList = []
            }else{
                getHttpAuthenticatedWithLoadingFlag().get("/rest-api/batch/" + productCode).then(function(response) {
                    context.batchList = response.data
                }).catch(function(e) {
                    console.log(e)
                })
            }

        }
    }
}
