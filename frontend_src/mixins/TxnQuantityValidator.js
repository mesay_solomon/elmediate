var context = {}
var alertErrorFn = undefined
export default{
    beforeMount(){
        context = this
    },
    mounted(){
        if(context.alertError !== undefined){
            alertErrorFn = context.alertError
        }
        if(context.alertErrorHandler !== undefined){
            alertErrorFn = context.alertErrorHandler
        }
    },
    methods:{
        validateQuantity(event, batchLineItem){

            var targetElement = event.target
            var quantity = batchLineItem.quantity
            if(quantity === ""){
                batchLineItem.hasErrors = false
            }else if(isNaN(quantity) || parseFloat(quantity) < 0.0){
                if(alertErrorFn!==undefined)
                {
                    alertErrorFn("Only positive numbers are allowed!")
                }else{
                    alert("Only positive numbers are allowed!")
                }
                batchLineItem.hasErrors = false
                batchLineItem.quantity = ''
                targetElement.value = ''
            }else{
                if(context.transactionType === 'RECEIVING'){
                    batchLineItem.hasErrors = false
                }else if(context.transactionType === 'PHYSICAL_COUNT'){
                    var matchingProductBatch = context.transactionProducts.find(function(txnPrd){
                        return txnPrd.productCode === context.selectedProduct.productCode
                            && txnPrd.batchNumber === batchLineItem.batchNumber
                    })
                    if(matchingProductBatch!==undefined){
                        alertErrorFn("Physical count already entered for this batch : " + batchLineItem.batchNumber)
                        batchLineItem.hasErrors = false
                        batchLineItem.quantity = ''
                        targetElement.value = ''
                    }else{
                        console.log(parseFloat(quantity).toFixed(2))
                        batchLineItem.hasErrors = parseFloat(quantity).toFixed(2)
                            !== parseFloat(batchLineItem.balance).toFixed(2)
                        if(batchLineItem.hasErrors){
                            var confirmAdjustment = confirm("Quantity entered " + quantity
                                + " is different from system calculated balance which is "
                                + batchLineItem.balance + ", would you like to do an adjustment?")
                            batchLineItem.hasErrors = false
                            if(confirmAdjustment){
                                context.currentBatchLineItem = batchLineItem
                                context.expectedAdjustedQty = parseFloat(quantity).toFixed(2) - parseFloat(batchLineItem.balance).toFixed(2)
                                context.showChangeAdjustmentModal = true
                            }else{
                                batchLineItem.quantity = ''
                                targetElement.value = ''
                            }
                        }
                    }

                }else if(context.transactionType === 'ISSUING' || context.transactionType === 'DISPENSATION' || context.transactionType === 'DISPENSING'){
                    var matchingTxnProductBatch = context.filteredTransactionProducts.find( (tp) => {
                        return tp.programProductBatchId === batchLineItem.programProductBatchId
                    })

                    if(matchingTxnProductBatch !== undefined){
                        var totalAddedQty = parseFloat(matchingTxnProductBatch.quantity) + parseFloat((batchLineItem.quantity))
                        if(totalAddedQty > parseFloat(batchLineItem.usableBalance)){
                            if(alertErrorFn!==undefined) {
                                alertErrorFn("Cannot continue Quantity entered will make the total quantity " + totalAddedQty
                                    + " which is more than system calculated balance " + batchLineItem.usableBalance
                                    + " Please correct that to continue!")
                            }else{
                                alert("Cannot continue Quantity entered will make the total quantity " + totalAddedQty
                                    + " which is more than system calculated balance " + batchLineItem.usableBalance
                                    + " Please correct that to continue!")
                            }
                            batchLineItem.hasErrors = false
                            batchLineItem.quantity = ''
                            targetElement.value = ''
                        }
                    }else{
                        batchLineItem.hasErrors = parseFloat(quantity).toFixed(2) > parseFloat(batchLineItem.usableBalance)
                        if(batchLineItem.hasErrors){
                            if(alertErrorFn!==undefined)
                            {
                                alertErrorFn("Cannot continue Quantity entered " + quantity
                                    + " is more than system calculated balance "+ batchLineItem.usableBalance
                                    + " Please correct that to continue!")
                            }else{
                                alert("Cannot continue Quantity entered " + quantity
                                    + " is more than system calculated balance "+ batchLineItem.usableBalance
                                    + " Please correct that to continue!")
                            }
                            batchLineItem.hasErrors = false
                            batchLineItem.quantity = ''
                            targetElement.value = ''
                        }
                    }



                }
            }
            if(batchLineItem.hasErrors){
                targetElement.classList.add("is-danger")
            }else{
                targetElement.classList.remove("is-danger")
            }
        }
    }
}
