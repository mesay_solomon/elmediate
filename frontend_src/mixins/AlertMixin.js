var context = {}
export default{
    data(){
        return {
            alertTitle: 'eLMIS Facility Edition',
            alertMessage:'',
            showSuccess: false,
            showError: false,
            showConfirm: false,
            okConfirmHandler: undefined
        }
    },
    beforeMount(){
        context = this
    },
    mounted(){
        //console.log("AlertMixin mounted!!!")
    },
    methods:{
        alertInfo: function(message){
            context.alertMessage = message
            context.showSuccess = true
        },
        alertError: function(message){
            context.alertMessage = message
            context.showError = true
        },
        confirm: function(message, okCallback){
            context.okConfirmHandler = okCallback
            context.alertMessage = message
            context.showConfirm = true
        },
        closeAlert: function () {
            context.showSuccess = false
            context.showError = false
            context.showConfirm = false
        },
        okConfirmClicked: function(){
            context.showConfirm = false
            context.okConfirmHandler()
        }
    }
}
