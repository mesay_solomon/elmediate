var context = {}
export default{
    data(){
        return {
            productBatchPageNumber: 1,
            productBatchTotalNumberOfPages: 0,
            productBatchPageSize: 5,
        }
    },
    beforeMount(){
        context = this
    },
    computed:{
        batchListFiltered(){
            this.filteredList = this.batchList
            var pages = this.filteredList.length / this.productBatchPageSize
            this.productBatchTotalNumberOfPages = Number.isInteger(pages) ? pages : (parseInt(pages) + 1)
            var productBatchPageSize = parseInt(context.productBatchPageSize)
            var productBatchPageNumber = parseInt(context.productBatchPageNumber) > parseInt(context.productBatchTotalNumberOfPages) ? parseInt(context.productBatchTotalNumberOfPages) : parseInt(context.productBatchPageNumber)
            if (productBatchPageNumber > context.productBatchTotalNumberOfPages) {
                productBatchPageNumber = context.productBatchTotalNumberOfPages
            }
            var startingIndex = (productBatchPageNumber - 1) * productBatchPageSize
            var endingIndex = startingIndex + productBatchPageSize
            var paginatedFilteredList = this.filteredList.slice(startingIndex, endingIndex)
            return paginatedFilteredList
        },
        batchListFilteredForTxns(){
            this.filteredList = this.productBatchList
            var pages = this.filteredList.length / this.productBatchPageSize
            this.productBatchTotalNumberOfPages = Number.isInteger(pages) ? pages : (parseInt(pages) + 1)
            var productBatchPageSize = parseInt(context.productBatchPageSize)
            var productBatchPageNumber = parseInt(context.productBatchPageNumber) > parseInt(context.productBatchTotalNumberOfPages) ? parseInt(context.productBatchTotalNumberOfPages) : parseInt(context.productBatchPageNumber)
            if (productBatchPageNumber > context.productBatchTotalNumberOfPages) {
                productBatchPageNumber = context.productBatchTotalNumberOfPages
            }
            var startingIndex = (productBatchPageNumber - 1) * productBatchPageSize
            var endingIndex = startingIndex + productBatchPageSize
            var paginatedFilteredList = this.filteredList.slice(startingIndex, endingIndex)
            return paginatedFilteredList
        }
    },
    methods:{
        setProductBatchPage: function (productBatchPageNumber) {
            context.productBatchPageNumber = productBatchPageNumber
        }
    }

}
