INSERT INTO application_properties
(
  KEY,
  value,
  data_type,
  groupname,
  displayorder,
  valueoptions,
  isconfigurable
)
SELECT 'CE_SEARCH_RNRS_URL',
       'https://uat.zm.elmis-dev.org/rest-api/requisitions/search',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT *
                              FROM application_properties
                              WHERE KEY = 'CE_SEARCH_RNRS_URL');

