INSERT INTO application_properties
(
  KEY,
  value,
  data_type,
  groupname,
  displayorder,
  valueoptions,
  isconfigurable
)
SELECT 'elmis.central.sync.url',
       'http://localhost:8443/rest-api/interfacing/sync/lookup',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT value
                              FROM application_properties
                              WHERE KEY = 'elmis.central.sync.url');

