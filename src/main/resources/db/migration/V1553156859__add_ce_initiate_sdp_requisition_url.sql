INSERT INTO application_properties
(
    KEY,
    value,
    data_type,
    groupname,
    displayorder,
    valueoptions,
    isconfigurable
)
SELECT 'CE_INITIATE_SDP_REQUISITION_URL',
       'https://uat.zm.elmis-dev.org/rest-api/requisitions/initiate',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT *
                              FROM application_properties
                              WHERE KEY = 'CE_INITIATE_SDP_REQUISITION_URL');

