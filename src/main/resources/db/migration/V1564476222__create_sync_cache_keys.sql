INSERT INTO public.sync_response_cache
  (url, response, sync_timestamp)
VALUES
  ('regimens', NULL, NULL),
  ('processing-schedules', NULL, NULL),
  ('geographic-levels', NULL, NULL),
  ('geographic-zones', NULL, NULL),
  ('facility-types', NULL, NULL),
  ('facilities', NULL, NULL),
  ('program-products', NULL, NULL),
  ('facility-approved-products', NULL, NULL),
  ('product-categories', NULL, NULL),
  ('programs', NULL, NULL),
  ('products', NULL, NULL),
  ('processing-periods', NULL, NULL),
  ('regimen-product-combinations', NULL, NULL),
  ('regimen-constituent-dosages', NULL, NULL),
  ('losses-adjustments-types', NULL, NULL),
  ('dosage-units', NULL, NULL),
  ('dosage-frequencies', NULL, NULL),
  ('regimen-categories', NULL, NULL),
  ('regimen-combination-constituents', NULL, NULL)
ON CONFLICT (url) DO UPDATE
  SET response = EXCLUDED.response,
      sync_timestamp = EXCLUDED.sync_timestamp
