INSERT INTO application_properties
(
    KEY,
    value,
    data_type,
    groupname,
    displayorder,
    valueoptions,
    isconfigurable
)
SELECT 'CE_LOGISTICS_PERIODS_URL',
       'https://uat.zm.elmis-dev.org/rest-api/logistics/periods',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT *
                              FROM application_properties
                              WHERE KEY = 'CE_LOGISTICS_PERIODS_URL');

