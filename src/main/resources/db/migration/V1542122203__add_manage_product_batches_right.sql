INSERT INTO rights
(
  name,
  description,
  righttype,
  createddate
)
SELECT 'MANAGE PRODUCT BATCHES',
       'MANAGE PRODUCT BATCHES',
       '',
       NOW() WHERE NOT EXISTS (SELECT * FROM rights WHERE name = 'MANAGE PRODUCT BATCHES');

