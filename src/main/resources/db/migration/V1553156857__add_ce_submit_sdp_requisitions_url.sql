INSERT INTO application_properties
(
  KEY,
  value,
  data_type,
  groupname,
  displayorder,
  valueoptions,
  isconfigurable
)
SELECT 'CE_SUBMIT_SDP_REQUISITIONS_URL',
       'https://uat.zm.elmis-dev.org/rest-api/sdp-requisitions',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT *
                              FROM application_properties
                              WHERE KEY = 'CE_SUBMIT_SDP_REQUISITIONS_URL');

