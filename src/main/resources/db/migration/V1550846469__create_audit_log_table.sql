﻿CREATE TABLE public.data_changes
(
    id bigserial NOT NULL,
    operation character varying(50) NOT NULL,
    object_type character varying(100) NOT NULL,
    record json NOT NULL,
    version_number uuid NOT NULL DEFAULT uuid_generate_v4(),
    ﻿created_date time without time zone NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.data_changes
    OWNER to postgres;