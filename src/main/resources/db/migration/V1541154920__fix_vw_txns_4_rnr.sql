DROP MATERIALIZED VIEW public.vw_transactions_for_rnr;

CREATE MATERIALIZED VIEW public.vw_transactions_for_rnr
TABLESPACE pg_default
AS
 SELECT a.id AS action_id,
    t.id AS txn_id,
    tp.program_product_id,
    tp.quantity,
    tt.name AS txn_type,
        CASE
            WHEN tt.name::text = ANY (ARRAY['SCREENING_TEST'::character varying::text, 'CONFIRMATORY_TEST'::character varying::text, 'ISSUE_OUT'::character varying::text, 'ARV_DISPENSING'::character varying::text, 'DISPENSING'::character varying::text]) THEN 'DISPENSING'::character varying
            ELSE tt.name
        END AS rnr_txn_type,
    t.transaction_timestamp
   FROM transactions t
     JOIN actions a ON t.action_id = a.id
     JOIN transaction_products tp ON tp.transaction_id = t.id
     JOIN transaction_types tt ON tt.id = t.transaction_type
  WHERE NOT (t.id IN ( SELECT reversal_transactions.reversed_transaction_id
           FROM reversal_transactions)) AND NOT (t.id IN ( SELECT reversal_transactions.reversal_transaction_id
           FROM reversal_transactions)) AND (tt.name::text = ANY (ARRAY['RECEIVING'::character varying::text, 'SCREENING_TEST'::character varying::text, 'CONFIRMATORY_TEST'::character varying::text, 'ISSUE_OUT'::character varying::text, 'ARV_DISPENSING'::character varying::text, 'DISPENSING'::character varying::text]))
UNION
 SELECT a.id AS action_id,
    t.id AS txn_id,
    tp.program_product_id,
    tp.quantity,
    tt.name AS txn_type,
    'DISPENSING'::character varying AS rnr_txn_type,
    t.transaction_timestamp
   FROM transactions t
     JOIN actions a ON t.action_id = a.id
     JOIN node_transactions nt ON nt.transaction_id = t.id
     JOIN transaction_products tp ON tp.transaction_id = t.id
     JOIN transaction_types tt ON tt.id = t.transaction_type
     JOIN program_products pp ON pp.id = tp.program_product_id
     JOIN programs p ON pp.programid = p.id
     JOIN node_programs np ON np.program_id = p.id AND nt.node_id = np.node_id
  WHERE t.transaction_type = (( SELECT transaction_types.id
           FROM transaction_types
          WHERE transaction_types.name::text = 'ISSUING'::text)) AND NOT (t.id IN ( SELECT reversal_transactions.reversed_transaction_id
           FROM reversal_transactions)) AND NOT (t.id IN ( SELECT reversal_transactions.reversal_transaction_id
           FROM reversal_transactions)) AND (p.id IN ( SELECT programs.id
           FROM programs
          WHERE (programs.code::text IN ( SELECT programs_1.code
                   FROM programs programs_1)))) AND (nt.node_id IN ( SELECT DISTINCT npg.node_id
           FROM node_programs npg
          WHERE npg.program_id = p.id AND npg.node_id = nt.node_id AND npg.dispensing_point = true))
WITH DATA;

ALTER TABLE public.vw_transactions_for_rnr
    OWNER TO postgres;