CREATE TABLE IF NOT EXISTS fe_update_info
(
  id                   SERIAL NOT NULL,
  facility_code        VARCHAR(50) NOT NULL,
  version_number       VARCHAR(50) NOT NULL,
  alt_version_number   INTEGER,
  TIMESTAMP TIMESTAMP DEFAULT NOW(),
  general_info         TEXT,
  CONSTRAINT fe_update_info_pk PRIMARY KEY (id)
);

COMMIT;
