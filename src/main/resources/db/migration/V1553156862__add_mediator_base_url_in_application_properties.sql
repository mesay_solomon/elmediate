INSERT INTO application_properties
(
    KEY,
    value,
    data_type,
    groupname,
    displayorder,
    valueoptions,
    isconfigurable
)
SELECT 'mediator.base.url',
       'http://localhost:8443',
       'String',
       'General',
       1,
       NULL,
       TRUE WHERE NOT EXISTS (SELECT value
                              FROM application_properties
                              WHERE KEY = 'mediator.base.url');

