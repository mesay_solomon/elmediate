CREATE TABLE public.sync_response_cache
(
    url character varying(100)  NOT NULL,
    response text ,
    sync_timestamp timestamp without time zone ,
    CONSTRAINT pk_sync_response_cache PRIMARY KEY(url)
);