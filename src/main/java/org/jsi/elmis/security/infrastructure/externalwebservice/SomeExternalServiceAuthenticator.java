/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.security.infrastructure.externalwebservice;


import org.jsi.elmis.common.util.Crypto;
import org.jsi.elmis.model.User;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;
import org.jsi.elmis.security.domain.DomainUser;
import org.jsi.elmis.security.infrastructure.AuthenticatedExternalWebService;
import org.jsi.elmis.security.infrastructure.security.ExternalServiceAuthenticator;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SomeExternalServiceAuthenticator implements ExternalServiceAuthenticator {
    @Autowired
    UserManagementService userManagementService;

    @Override
    public AuthenticatedExternalWebService authenticate(String username, String password) throws Exception {
        List<UserNodeRoleRightResult> roleRoleRightList= new  ArrayList<>();
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        ExternalWebServiceStub externalWebService = new ExternalWebServiceStub();
        roleRoleRightList= userManagementService.authenticate(user);
        List<String> rightList= new ArrayList<>();

        if(roleRoleRightList==null || roleRoleRightList.isEmpty()){

            throw new  BadCredentialsException("Unable to authenticate Backend Admin for provided credentials");
        }
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roleRoleRightList.size());


        for(UserNodeRoleRightResult roleRightResult:roleRoleRightList){
            rightList.add(roleRightResult.getRightName());
            authorities.add(new SimpleGrantedAuthority(roleRightResult.getRightName()));
        }

        AuthenticatedExternalWebService authenticatedExternalWebService = new AuthenticatedExternalWebService(new DomainUser(username), null,
                authorities);
        authenticatedExternalWebService.setExternalWebService(externalWebService);

        return authenticatedExternalWebService;
    }
}
