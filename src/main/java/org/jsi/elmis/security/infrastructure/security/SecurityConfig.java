/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.security.infrastructure.security;


import javax.servlet.http.HttpServletResponse;

import org.jsi.elmis.security.api.ApiController;
import org.jsi.elmis.security.infrastructure.externalwebservice.SomeExternalServiceAuthenticator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebMvcSecurity
@EnableScheduling
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${backend.admin.role}")
    private String backendAdminRole;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                csrf().disable().
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                and().
                authorizeRequests().
                antMatchers(actuatorEndpoints()).hasRole(backendAdminRole).
                anyRequest().authenticated().
                and().
                anonymous().disable().
                exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint());


        http.addFilterBefore(new AuthenticationFilter(authenticationManager()), BasicAuthenticationFilter.class).
                addFilterBefore(new ManagementEndpointAuthenticationFilter(authenticationManager()), BasicAuthenticationFilter.class);
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/rest-api/products-in-program/**");
        web.ignoring().antMatchers("/facility-approved-products");
        web.ignoring().antMatchers("/rest-api/lookup/**");
        web.ignoring().antMatchers("/get-all-nodes");
        web.ignoring().antMatchers("/users/authenticate");
        web.ignoring().antMatchers("/rest-api/configuration/property");
        web.ignoring().antMatchers("/rest-api/test-backup");
        web.ignoring().antMatchers("/rest-api/product-batches/**");
        web.ignoring().antMatchers("/rest-api/med-product-batches/**");
        web.ignoring().antMatchers("/v2/api-docs");
        web.ignoring().antMatchers("/swagger-resources/configuration/ui");
        web.ignoring().antMatchers("/swagger-resources");
        web.ignoring().antMatchers("/swagger-resources/configuration/security");
        web.ignoring().antMatchers("/swagger-ui.html");
        web.ignoring().antMatchers("/webjars/**");
        web.ignoring().antMatchers("/validatorUrl");
        web.ignoring().antMatchers("/rest-api/sdp-client");
        web.ignoring().antMatchers("/rest-api/current-version");
        web.ignoring().antMatchers("/rest-api/latest-avaiable-update");
        web.ignoring().antMatchers("/rest-api/execute-update");
        web.ignoring().antMatchers("/rest-api/download-update");
        web.ignoring().antMatchers("/rest-api/system-exit");
        web.ignoring().antMatchers("/rest-api/interfacing/sync");
        web.ignoring().antMatchers("/rest-api/misc/reorder-actions");
        web.ignoring().antMatchers("/rest-api/arv/update-patient-guuid");
        web.ignoring().antMatchers("/local-middleware-notifications/**");
        web.ignoring().antMatchers("/topic/**");
        web.ignoring().antMatchers("/users/interfacing/authenticate");
        web.ignoring().antMatchers("/fonts/**");
        web.ignoring().antMatchers("/report/download/**");
        web.ignoring().antMatchers("/");
        web.ignoring().antMatchers("/messages");
        web.ignoring().antMatchers("//**/locales");
        web.ignoring().antMatchers("/rest-api/interfacing/stock-status-submission");
        web.ignoring().antMatchers("/rest-api/interfacing/sync/lookup");
        web.ignoring().antMatchers("/rest-api/interfacing/syncv2");
        web.ignoring().antMatchers(HttpMethod.POST, "/changeLocale");


    }
    private String[] actuatorEndpoints() {
        return new String[]{ApiController.AUTOCONFIG_ENDPOINT, ApiController.BEANS_ENDPOINT, ApiController.CONFIGPROPS_ENDPOINT,
                ApiController.ENV_ENDPOINT, ApiController.MAPPINGS_ENDPOINT,
                ApiController.METRICS_ENDPOINT, ApiController.SHUTDOWN_ENDPOINT};
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(domainUsernamePasswordAuthenticationProvider()).
                authenticationProvider(backendAdminUsernamePasswordAuthenticationProvider()).
                authenticationProvider(tokenAuthenticationProvider());
    }

    @Bean
    public TokenService tokenService() {
        return new TokenService();
    }

    @Bean
    public ExternalServiceAuthenticator someExternalServiceAuthenticator() {
        return new SomeExternalServiceAuthenticator();
    }

    @Bean
    public AuthenticationProvider domainUsernamePasswordAuthenticationProvider() {
        return new DomainUsernamePasswordAuthenticationProvider(tokenService(), someExternalServiceAuthenticator());
    }

    @Bean
    public AuthenticationProvider backendAdminUsernamePasswordAuthenticationProvider() {
        return new BackendAdminUsernamePasswordAuthenticationProvider();
    }

    @Bean
    public AuthenticationProvider tokenAuthenticationProvider() {
        return new TokenAuthenticationProvider(tokenService());
    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }


}
