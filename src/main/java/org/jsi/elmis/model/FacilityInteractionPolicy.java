/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

/**
 * Created by mesays.taye on 06/09/2017.
 */
public class FacilityInteractionPolicy {

    public static final String REQUEST = "REQUEST";
    public static final String ISSUE = "ISSUE";
    public static final String RETURN = "RETURN";


    private Integer id;

    private Integer fromFacility;

    private Integer toFacility;

    private String actionType;

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromFacility() {
        return fromFacility;
    }

    public void setFromFacility(Integer fromFacility) {
        this.fromFacility = fromFacility;
    }

    public Integer getToFacility() {
        return toFacility;
    }

    public void setToFacility(Integer toFacility) {
        this.toFacility = toFacility;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType == null ? null : actionType.trim();
    }
}