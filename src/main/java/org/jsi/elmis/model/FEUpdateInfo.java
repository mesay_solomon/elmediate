package org.jsi.elmis.model;

import java.util.Date;

public class FEUpdateInfo {
    private Integer id;

    private String facilityCode;

    private String versionNumber;

    private Integer altVersionNumber;

    private Date timestamp;

    private String generalInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode == null ? null : facilityCode.trim();
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber == null ? null : versionNumber.trim();
    }

    public Integer getAltVersionNumber() {
        return altVersionNumber;
    }

    public void setAltVersionNumber(Integer altVersionNumber) {
        this.altVersionNumber = altVersionNumber;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(String generalInfo) {
        this.generalInfo = generalInfo == null ? null : generalInfo.trim();
    }
}