/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model.adapters;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.dao.ProductDAO;
import org.jsi.elmis.dao.ProgramDAO;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.Product;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.RequisitionLineItem;
import org.jsi.elmis.model.RequisitionLineItemLossesAndAdjustment;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.LossesAndAdjustments;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.rnr.RnRLineItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequisitionAdapter {
	
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	ProgramDAO pgmDAO;
	@Autowired
	ProductDAO productDAO;
	public ArrayList<Requisition> convertRnR(List<Rnr> rnrs){
		ArrayList<Requisition> requisitions = new ArrayList<>();
		Requisition requisition;
		if(rnrs != null && !rnrs.isEmpty()){
			for(Rnr rnr : rnrs){
				//TODO: currently approver name is not being pulled?
				requisition = convertRnr(rnr);
				requisitions.add(requisition);
			}
		}
		return requisitions;
	}
	public Requisition convertRnr(Rnr rnr){
		Requisition requisition;
		requisition = this.convertRnR(new RnR(rnr.getFacility().getCode(), rnr.getPeriod().getId(), rnr.getProgram().getCode(),
				"Approver Name", rnr.getSourceApplication(), rnr.getStatus().name(), rnr.isEmergency(), rnr.getAllLineItems(),
				rnr.getRegimenLineItems()));
		requisition.setRequisitionType(rnr.getSourceApplication().equalsIgnoreCase("eLMIS_FE") || rnr.getSourceApplication().equalsIgnoreCase("WEB_UI") ? Requisition.REGULAR_REQUISITION : Requisition.OFFLINE_REQUISITION);
		requisition.setPeriodStart(rnr.getPeriod().getStartdate());
		requisition.setPeriodEnd(rnr.getPeriod().getEnddate());
		return  requisition;
	}
	public Requisition convertRnR(RnR rnr){
		Requisition rqn = new Requisition();
		
		Facility facility = commonDAO.getFacilityByCode(rnr.getAgentCode());
		Program program = pgmDAO.selectByCode(rnr.getProgramCode());
		
		rqn.setFacilityId(facility.getId());
		rqn.setPeriodId(rnr.getPeriodId());
		rqn.setProgramId(program.getId());
		rqn.setEmergency(rnr.getEmergency());
		rqn.setStatus(rnr.getStatus());
		/*
		TODO: approverId, createdDate, modifiedDate;
		 */
		
		for (RnRLineItem r : rnr.getProducts()) {
			Integer amc=0;
			RequisitionLineItem rli = new RequisitionLineItem();
			Product prod = productDAO.selectProductByCode(r.getProductCode());
			System.out.println(" amc is "+ r.getAmc());
			rli.setProductId(prod.getId());
			rli.setProductCode(prod.getCode());
			rli.setProductName(prod.getPrimaryname());
			rli.setBeginningBalance(convertToBigDecimal(r.getBeginningBalance()));
			rli.setQuantityReceived(convertToBigDecimal(r.getQuantityReceived()));
			rli.setQuantityDispensed(convertToBigDecimal(r.getQuantityDispensed()));
			rli.setStockOnHand(convertToBigDecimal(r.getStockInHand()));
			rli.setStockOutDays(r.getStockOutDays());
			rli.setNewPatientCount(r.getNewPatientCount());
			rli.setQuantityRequested(convertToBigDecimal(r.getQuantityRequested()));
			rli.setReasonForRequestedQuantity(r.getReasonForRequestedQuantity());
//			rli.setAmc(convertToBigDecimal(r.getAmc()));
			rli.setNormalizedConsumption(convertToBigDecimal(r.getNormalizedConsumption()));
			rli.setCalculatedOrderQuantity(convertToBigDecimal(r.getCalculatedOrderQuantity()));
			rli.setMaxStockQuantity(convertToBigDecimal(r.getMaxStockQuantity()));
			rli.setDispensingUnit(r.getDispensingUnit());
			rli.setMaxMonthsOfStock(convertToBigDecimal(r.getMaxMonthsOfStock().intValue()));//TODO: take double value
			rli.setPackSize(convertToBigDecimal(r.getPackSize()));
			rli.setDosesPerDispensingUnit(convertToBigDecimal(r.getDosesPerMonth()));
			rli.setDosesPerDispensingUnit(convertToBigDecimal(r.getDosesPerDispensingUnit()));
			rli.setQuantityApproved(convertToBigDecimal(r.getQuantityApproved()));
			rli.setFullSupply(r.getFullSupply());
			rli.setRemarks(r.getRemarks());
			rli.setSkipped(r.getSkipped());
		List<Integer> normalizedConsumptions=	r.getPreviousNormalizedConsumptions();
		if(normalizedConsumptions!=null && !normalizedConsumptions.isEmpty()){
			int size=normalizedConsumptions.size()+1;

			Integer totalConsumption=0;
			for(Integer c: normalizedConsumptions){
				totalConsumption+=c;
				System.out.println(c);
			}
			amc=totalConsumption/size;
		}

rli.setAmc(convertToBigDecimal(amc));
			for (LossesAndAdjustments l : r.getLossesAndAdjustments()) {
				RequisitionLineItemLossesAndAdjustment lna = new RequisitionLineItemLossesAndAdjustment();
				
				lna.setLossesAndAdjustmentType(l.getType().getName());
				lna.setQuantity(BigDecimal.valueOf(l.getQuantity()));
				
				rli.getRliLossesNAdjustments().add(lna);
			}
			rqn.getRequisitionLineItems().add(rli);		
		}
		return rqn;
	}
	
	private BigDecimal convertToBigDecimal(Integer value){
		if(value == null)
			return BigDecimal.ZERO;
		
		return BigDecimal.valueOf(value);
	}
}
