package org.jsi.elmis.model;

import java.util.Date;

public class DataChange {
    private Long id;

    private String operation;

    private String objectType;

    private Object record;

    private Object versionNumber;

    private Date createdDate;

    public DataChange(){}
    public DataChange(Long id, String operation, String objectType, Object record, Object versionNumber, Date createdDate) {
        this.id = id;
        this.operation = operation;
        this.objectType = objectType;
        this.record = record;
        this.versionNumber = versionNumber;
        this.createdDate = createdDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation == null ? null : operation.trim();
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType == null ? null : objectType.trim();
    }

    public Object getRecord() {
        return record;
    }

    public void setRecord(Object record) {
        this.record = record;
    }

    public Object getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Object versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
