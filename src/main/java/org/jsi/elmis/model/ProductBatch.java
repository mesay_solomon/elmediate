/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.CompareToBuilder;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductBatch {
    private Integer id;

    private Integer productId;

    private String name;

    private String description;

    private Integer batchStandardId;

    private BatchStandard batchStandard;

    private Product product;

    private Date expiryDate;


    public boolean compareTo(ProductBatch pb) {
        return new CompareToBuilder().append(this.id, pb.id)
                .append(this.name.trim(), pb.name.trim())
                .append(this.description.trim(), pb.description.trim())
                .append(this.batchStandardId, pb.batchStandardId)
                .append(this.productId, pb.productId)
                .append(this.expiryDate.toString(), pb.expiryDate.toString())
                .toComparison() == 0;


    }
}