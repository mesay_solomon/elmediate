/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(value = { "stockTransfer" })
public class StockTransferStatus extends BaseModel{

	public final static String REQUESTED = "REQUESTED";
	public final static String APPROVED = "APPROVED";
	public final static String ISSUED = "ISSUED";
	public final static String REQUEST_REJECTED = "REQUEST_REJECTED";
	public final static String ISSUE_REJECTED = "ISSUE_REJECTED";
	public final static String COMPLETED = "COMPLETED";
	public final static String REVERSAL_INITIATED = "REVERSAL_INITIATED";
	public final static String REVERSAL_COMPLETED = "REVERSAL_COMPLETED";
	private StockTransfer stockTransfer;

    private String status;

    private Long userId;

	public Long getUserId() {
		return userId;
	}

	public String getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}

	public List<StockTransferStatusLineItem> getMergedLineItems() {
		return mergedLineItems;
	}

	private String userUUID;
    private Date statusChangeTimesamp;

    private List<StockTransferStatusLineItem> lineItems;

    private List<StockTransferStatusLineItem> mergedLineItems;

	private User user;

	private String remarks;
    
	public StockTransferStatus(){
		super();
	}
	
	//id, stockTransfer, status, statusChangeTimesamp, createdDate, createdBy, modifiedDate, modifiedBy, lineItems
	
    public StockTransferStatus(Integer id, StockTransfer stockTransfer, String status, Date statusChangeTimesamp, 
    		Date createdDate, User createdBy, Date modifiedDate, User modifiedBy, List<StockTransferStatusLineItem> lineItems) {
    	
		super(id, createdDate, createdBy, modifiedDate, modifiedBy);
		this.stockTransfer = stockTransfer;
		this.status = status;
		this.statusChangeTimesamp = statusChangeTimesamp;
		this.lineItems = lineItems;
	}

	public StockTransfer getStockTransfer() {
		return stockTransfer;
	}

	public void setStockTransfer(StockTransfer stockTransfer) {
		this.stockTransfer = stockTransfer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getStatusChangeTimesamp() {
		return statusChangeTimesamp.getTime();
	}

	public void setStatusChangeTimesamp(Date statusChangeTimesamp) {
		this.statusChangeTimesamp = statusChangeTimesamp;
	}

	public List<StockTransferStatusLineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<StockTransferStatusLineItem> lineItems) {
		this.lineItems = lineItems;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}