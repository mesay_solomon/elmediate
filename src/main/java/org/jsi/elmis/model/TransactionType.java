/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionType {
	public static final String POSITIVE_ADJUSTMENT = "POSITIVE_ADJUSTMENT";
	public static final String NEGATIVE_ADJUSTMENT = "NEGATIVE_ADJUSTMENT";
	public static final String ARV_DISPENSING = "ARV_DISPENSING";
	public static final String SCREENING_TEST = "SCREENING_TEST";
	public static final String CONFIRMATORY_TEST = "CONFIRMATORY_TEST";
	public static final String RECEIVING = "RECEIVING";
	public static final String ISSUING = "ISSUING";
	public static final String TRANSFER = "TRANSFER";
	public static final String RETURN = "RETURN";
	public static final String ISSUE_OUT = "ISSUE_OUT";
	public static final String EM_DISPENSING = "EM_DISPENSING";
	public static final String LAB_DISPENSING = "LAB_DISPENSING";
	public static final String DISPENSING = "DISPENSING";//general
	public static final String OTHER_POSITIVE = "OTHER_POSITVE";
	public static final String OTHER_NEGATIVE = "OTHER_NEGATIVE";
	public static final String INITIAL_STOCK_SETUP = "INITIAL_STOCK_SETUP";
	public static final String DISPENSING_REVERSAL = "DISPENSING_REVERSAL";
	public static final String RECEIVING_REVERSAL = "RECEIVING_REVERSAL";
	public static final String ISSUING_REVERSAL = "ISSUING_REVERSAL";
	public static final String POSITIVE_ADJUSTMENT_REVERSAL = "POSITIVE_ADJUSTMENT_REVERSAL";
	public static final String NEGATIVE_ADJUSTMENT_REVERSAL = "NEGATIVE_ADJUSTMENT_REVERSAL";
	public static final String ISSUE_OUT_REVERSAL = "ISSUE_OUT_REVERSAL";
	
    private Integer id;
    private String name;
    private String description;
    private Boolean transitive;
    private Integer complements;

    public Boolean isPositive() {
		if(name.equals(ARV_DISPENSING) || name.equals(SCREENING_TEST) || 
				name.equals(CONFIRMATORY_TEST) || name.equals(NEGATIVE_ADJUSTMENT) ||
				name.equals(ISSUING) || //TODO: issuing negative makes sense , since it is two way txn ?
				name.equals(ISSUE_OUT) || name.equals(EM_DISPENSING) || name.equals(DISPENSING) ||
				name.equals(LAB_DISPENSING)  || name.equals(OTHER_NEGATIVE) || name.equals(RECEIVING_REVERSAL) ||
				name.equals(POSITIVE_ADJUSTMENT_REVERSAL))
			return false;
		else if (name.equals(RECEIVING) || name.equals(POSITIVE_ADJUSTMENT) || 
				name.equals(OTHER_POSITIVE) || name.equals(INITIAL_STOCK_SETUP)|| name.equals(DISPENSING_REVERSAL)
				|| name.equals(ISSUING_REVERSAL) || name.equals(NEGATIVE_ADJUSTMENT_REVERSAL) || name.equals(ISSUE_OUT_REVERSAL)) 
			return true;
		else 
			return null;
	}
    
    public static String getReverseTransactionType(String txnType){
    	String reverseTxnType = null;
    	//TODO: accurate reversal types?
    	switch (txnType) {
		case ARV_DISPENSING:
			reverseTxnType = DISPENSING_REVERSAL;
			break;
		case SCREENING_TEST:
			reverseTxnType = DISPENSING_REVERSAL;
			break;
		case CONFIRMATORY_TEST:
			reverseTxnType = DISPENSING_REVERSAL;
			break;
		case ISSUING:
			reverseTxnType = ISSUING_REVERSAL;
			break;
		case ISSUE_OUT:
			reverseTxnType = ISSUE_OUT_REVERSAL;
			break;
		case RECEIVING:
			reverseTxnType = RECEIVING_REVERSAL;
			break;
		case POSITIVE_ADJUSTMENT:
			reverseTxnType = POSITIVE_ADJUSTMENT_REVERSAL;
			break;
		case NEGATIVE_ADJUSTMENT:
			reverseTxnType = NEGATIVE_ADJUSTMENT_REVERSAL;
			break;
		}
    	return reverseTxnType;
    }
}