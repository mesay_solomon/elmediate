/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegimenCombinationProduct {
    private Integer id;

    @SerializedName(value="defaultDosage.id")
    private Integer defaultDosageId;

    @SerializedName(value="productCombination.id")
    private Integer productComboId;

    private Integer productId;
    @SerializedName(value="defaultDosage")
    private DosageUnit defaultDosage;
    @SerializedName(value="productCombination")
    private RegimenProductCombination productCombination;
    @SerializedName(value="product")
    private Product product;

    public boolean compareTo(RegimenCombinationProduct rcp) {
        return new CompareToBuilder().append(this.id, rcp.id)
                .append(this.productCombination.getId(), rcp.productComboId)
                .append(this.product.getId(), rcp.productId)
                .append(this.defaultDosage.getId(), rcp.defaultDosageId)
                .toComparison() == 0;
    }
}