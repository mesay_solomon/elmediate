/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class FacilityClientVital {
    private Integer id;

    private Integer facilityClientId;

    @SerializedName(value="HeightDateCollected")
    private Date heightDateCollected;
    @SerializedName(value="WeightDateCollected")
    private Date weightDateCollected;
    @SerializedName(value="BloodPressureDateCollected")
    private Date bloodPressureDateCollected;
    @SerializedName(value="Height")
    private Double height;
    @SerializedName(value="Weight")
    private Double weight;
    @SerializedName(value="BloodPressure")
    private String bloodPressure;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFacilityClientId() {
        return facilityClientId;
    }

    public void setFacilityClientId(Integer facilityClientId) {
        this.facilityClientId = facilityClientId;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure == null ? null : bloodPressure.trim();
    }

    public Date getHeightDateCollected() {
        return heightDateCollected;
    }

    public void setHeightDateCollected(Date heightDateCollected) {
        this.heightDateCollected = heightDateCollected;
    }

    public Date getWeightDateCollected() {
        return weightDateCollected;
    }

    public void setWeightDateCollected(Date weightDateCollected) {
        this.weightDateCollected = weightDateCollected;
    }

    public Date getBloodPressureDateCollected() {
        return bloodPressureDateCollected;
    }

    public void setBloodPressureDateCollected(Date bloodPressureDateCollected) {
        this.bloodPressureDateCollected = bloodPressureDateCollected;
    }
}