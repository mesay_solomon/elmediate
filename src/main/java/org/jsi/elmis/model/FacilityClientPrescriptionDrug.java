/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

import com.google.gson.annotations.SerializedName;

public class FacilityClientPrescriptionDrug {
    private Integer id;

    private Integer facilityClientPrescriptionId;
    @SerializedName(value = "DrugCode")
    private String productCode;
    @SerializedName(value = "Frequency")
    private String frequency;
    @SerializedName(value = "DosageUnit")
    private String dosageUnit;
    @SerializedName(value = "QuantityPerDose")
    private Double quantity;

    private String dosage;//for display

    private Double dosageQuantityPerDay;

    private Integer dosageUnitId;

    private Integer dosageFrequencyId;
    @SerializedName(value = "Duration")
    private Integer duration;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFacilityClientPrescriptionId() {
        return facilityClientPrescriptionId;
    }

    public void setFacilityClientPrescriptionId(Integer facilityClientPrescriptionId) {
        this.facilityClientPrescriptionId = facilityClientPrescriptionId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Integer getDosageUnitId() {
        return dosageUnitId;
    }

    public void setDosageUnitId(Integer dosageUnitId) {
        this.dosageUnitId = dosageUnitId;
    }

    public Integer getDosageFrequencyId() {
        return dosageFrequencyId;
    }

    public void setDosageFrequencyId(Integer dosageFrequencyId) {
        this.dosageFrequencyId = dosageFrequencyId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(String dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Double getDosageQuantityPerDay() {
        return dosageQuantityPerDay;
    }

    public void setDosageQuantityPerDay(Double dosageQuantityPerDay) {
        this.dosageQuantityPerDay = dosageQuantityPerDay;
    }
}