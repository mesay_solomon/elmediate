package org.jsi.elmis.model;

import java.util.Date;

public class MedProductBatch {
    private Integer id;

    private Integer productId;

    private String name;

    private String description;

    private Integer batchStandardId;

    private Date expiryDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getBatchStandardId() {
        return batchStandardId;
    }

    public void setBatchStandardId(Integer batchStandardId) {
        this.batchStandardId = batchStandardId;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}