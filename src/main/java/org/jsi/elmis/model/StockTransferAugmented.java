/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;

import java.util.List;

/**
 * Created by mesays.taye on 23/08/2017.
 */
public class StockTransferAugmented extends StockTransfer{

    private String requester; // E.g ( Mr.X at Facility Y if we have both pieces of inform
    private String requestedFromFacilityName;
    private String statusString;

    List<StockTransferStatusAugmented> augmentedStatuses;

    public List<StockTransferStatusAugmented> getAugmentedStatuses() {
        return augmentedStatuses;
    }

    public void setAugmentedStatuses(List<StockTransferStatusAugmented> augmentedStatuses) {
        this.augmentedStatuses = augmentedStatuses;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public void setRequestedFromFacilityName(String requestedFromFacilityName) {
        this.requestedFromFacilityName = requestedFromFacilityName;
    }

    public String getRequestedFromFacilityName() {
        return requestedFromFacilityName;
    }

    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }
}
