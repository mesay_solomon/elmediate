/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.model;



import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class StockTransfer extends BaseModel {

	public StockTransfer(){
		super();
	}

	// id, requestPath, stockTransferTimestamp, status, createdDate, createdBy, modifiedDate, modifiedBy
    public StockTransfer(Integer id, RequisitionPath requestPath, Date stockTransferTimestamp, StockTransferStatus status, Date createdDate, User createdBy, Date modifiedDate, User modifiedBy) {
		super(id, createdDate, createdBy, modifiedDate, modifiedBy);
		this.requestPath = requestPath;
		this.stockTransferTimestamp = stockTransferTimestamp;
		this.status = status;
	}
    
	private RequisitionPath requestPath;
    private Date stockTransferTimestamp;
    private StockTransferStatus status;
    
    
	public RequisitionPath getRequestPath() {
		return requestPath;
	}
	public void setRequestPath(RequisitionPath requestPath) {
		this.requestPath = requestPath;
	}
	public Long getStockTransferTimestamp() {
		return stockTransferTimestamp.getTime();
	}
	public void setStockTransferTimestamp(Date stockTransferTimestamp) {
		this.stockTransferTimestamp = stockTransferTimestamp;
	}
	public StockTransferStatus getStatus() {
		return status;
	}
	public void setStatus(StockTransferStatus status) {
		this.status = status;
	}

}