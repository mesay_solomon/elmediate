package org.jsi.elmis.model.criteria;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Boolean.FALSE;
import static lombok.AccessLevel.NONE;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class RequisitionSearchCriteria {

    Long userId;
    Long facilityId;
    Long programId;
    Long periodId;
    boolean withoutLineItems;
    String dateRangeStart;
    String dateRangeEnd;

    @Getter(NONE)
    Boolean emergency = FALSE;

    public Boolean isEmergency() {
        return emergency;
    }

    public Date getRangeStart() {
        Date rangeStartDate;
        try {
            rangeStartDate = new SimpleDateFormat("dd-MM-yyyy").parse(this.dateRangeStart);
        } catch (ParseException e) {
            rangeStartDate = null;
        }
        return rangeStartDate;
    }

    public Date getRangeEnd() {
        Date rangeEndDate;
        try {
            rangeEndDate = new SimpleDateFormat("dd-MM-yyyy").parse(this.dateRangeEnd);
        } catch (ParseException e) {
            rangeEndDate = null;
        }
        return rangeEndDate;
    }
}
