package org.jsi.elmis.model;

public class ElmisSyncObject {

    public ElmisSyncObject(Object fetchedObject, Object storedObject, Boolean changed){
        this.fetchedObject = fetchedObject;
        this.storedObject = storedObject;
        this.changed = changed;
    }

    Object fetchedObject;
    Object storedObject;
    Boolean changed;

    public Object getFetchedObject() {
        return fetchedObject;
    }

    public void setFetchedObject(Object fetchedObject) {
        this.fetchedObject = fetchedObject;
    }

    public Boolean getChanged() {
        return changed;
    }

    public void setChanged(Boolean changed) {
        this.changed = changed;
    }

    public Object getStoredObject() {
        return storedObject;
    }

    public void setStoredObject(Object storedObject) {
        this.storedObject = storedObject;
    }
}
