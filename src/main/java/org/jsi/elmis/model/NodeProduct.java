/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.model;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mesay S. Taye
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NodeProduct {

    //NodeProduct(id, programProductId, programProduct, totalInflow, totalOutflow, totalAdjustments, latestPhysicalCountId, nodeId, node, quantityOnHand)

    public NodeProduct(Integer id){ this.id = id;}

    private Integer id;

    private Integer programProductId;
    
    private ProgramProduct programProduct;

    private BigDecimal totalInflow;

    private BigDecimal totalOutflow;

    private BigDecimal totalAdjustments;

    private Integer latestPhysicalCountId;

    private Integer nodeId;
    
    private Node node;

    private BigDecimal quantityOnHand;

    private List<NodeProgramProductBatch> nodeProgramProductBatches;

    public void setQuantitiesForTxn(TransactionProduct txnProduct, Boolean add){

        if (add){
            this.quantityOnHand = this.quantityOnHand.add(txnProduct.getQuantity());
            for (TransactionProgramProductBatch tPPB:
                    txnProduct.getBatchQuantities()) {
                for (NodeProgramProductBatch nPPB:
                        this.getNodeProgramProductBatches()) {
                    if (tPPB.getProgramProductBatchId() == nPPB.getProgramProductBatchId() ){
                        nPPB.setQuantity(nPPB.getQuantity().add(tPPB.getQuantity()));
                    }
                }

            }
        } else {
            this.quantityOnHand = this.quantityOnHand.subtract(txnProduct.getQuantity());
            for (TransactionProgramProductBatch tPPB:
                    txnProduct.getBatchQuantities()) {
                for (NodeProgramProductBatch nPPB:
                        this.getNodeProgramProductBatches()) {
                    if (tPPB.getProgramProductBatchId() == nPPB.getProgramProductBatchId() ){
                        nPPB.setQuantity(nPPB.getQuantity().subtract(tPPB.getQuantity()));
                    }
                }

            }
        }


    }
}