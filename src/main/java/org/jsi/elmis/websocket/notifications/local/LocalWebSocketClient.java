/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.websocket.notifications.local;

import org.apache.tomcat.websocket.WsWebSocketContainer;
import org.jsi.elmis.websocket.notifications.StompNotificationSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import javax.net.ssl.*;
import javax.validation.executable.ValidateOnExecution;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collections;

/**
 * Created by Mekbib on 6/24/2017.
 */
@Component
public class LocalWebSocketClient {
    LocalWebSocketSessionHandler localWebSocketSessionHandler = new LocalWebSocketSessionHandler();
    final String notifyURL = "/notify-client-application";
    private String localWebSocketURL;
    private String serverPort;
    private String myFacilityCode;

    public void connectToSocket() throws InterruptedException {
        localWebSocketURL = "wss://localhost:" + serverPort + "/local-middleware-notifications";
        StandardWebSocketClient webSocketClient = new StandardWebSocketClient();
        webSocketClient.setUserProperties(Collections.singletonMap(WsWebSocketContainer.SSL_CONTEXT_PROPERTY, buildSSLContext()));
        WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);

        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        ThreadPoolTaskScheduler myTaskScheduler = new ThreadPoolTaskScheduler();
        myTaskScheduler.afterPropertiesSet();
        myTaskScheduler.submit(()->{});

        stompClient.setTaskScheduler(myTaskScheduler);

        localWebSocketSessionHandler.setNotifyURL(notifyURL);
        System.out.println("Connecting to local websocket");
        stompClient.connect(localWebSocketURL, localWebSocketSessionHandler);
    }

    public LocalWebSocketSessionHandler getLocalStompNotificationSessionHandler() throws InterruptedException {
        return this.localWebSocketSessionHandler;
    }


    public void setServerPort(String port){
        this.serverPort = port;
    }
    public void setMyFacilityCode(String myFacilityCode){
        this.myFacilityCode = myFacilityCode;
    }

    public SSLContext buildSSLContext(){
        SSLContext sslContext = null;
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
            public X509Certificate[] getAcceptedIssuers(){return null;}
            public void checkClientTrusted(X509Certificate[] certs, String authType){}
            public void checkServerTrusted(X509Certificate[] certs, String authType){}
        }};

        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) { return true; }
        };

        // Install the all-trusting trust manager
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {e.printStackTrace();}
        return sslContext;
    }
}
