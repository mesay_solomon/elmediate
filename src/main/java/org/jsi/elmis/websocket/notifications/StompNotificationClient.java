/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.websocket.notifications;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

/**
 * Created by Mekbib on 6/22/2017.
 */
@Component
public class StompNotificationClient {
    @Autowired
    StompNotificationSessionHandler stompNotificationSessionHandler;

    @Value("${middleware.websocket.url.base}")
    String notificationWebSocketURL;

    @Value("${middleware.websocket.url.notify}")
    String notifyURL;

    @Autowired
    void connectToSocket() throws InterruptedException {
        WebSocketClient webSocketClient = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(webSocketClient);

        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        ThreadPoolTaskScheduler myTaskScheduler = new ThreadPoolTaskScheduler();

        myTaskScheduler.afterPropertiesSet();
        myTaskScheduler.submit(()->{});

        stompClient.setTaskScheduler(myTaskScheduler);

        stompNotificationSessionHandler.setNotifyURL(notifyURL);
        stompClient.connect(notificationWebSocketURL, stompNotificationSessionHandler);
    }

    public StompNotificationSessionHandler getStompNotificationSessionHandler() throws InterruptedException {
        return this.stompNotificationSessionHandler;
    }
}
