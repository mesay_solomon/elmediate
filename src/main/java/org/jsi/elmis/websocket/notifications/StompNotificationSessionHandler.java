/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.websocket.notifications;


import com.google.gson.*;
import org.jsi.elmis.model.StockTransfer;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.websocket.notifications.local.LocalWebSocketClient;
import org.jsi.elmis.websocket.notifications.local.LocalWebSocketSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created by Mekbib on 6/22/2017.
 */
@Component
public class StompNotificationSessionHandler extends StompSessionHandlerAdapter {

    private String myFacilityCode;

    @Autowired
    PropertiesService propServ;

    private static LocalWebSocketSessionHandler localWebSocketSessionHandler;
    private static StompSession stompSession = null;

    private StompHeaders sendNotificationHeaders = new StompHeaders();
    private String notifyURL;

    private final Gson jSONMarshaller = new GsonBuilder().registerTypeAdapter(Date.class,
            new JsonDeserializer<Date>() {
                @Override
                public Date deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context)
                        throws JsonParseException {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                }
            }).create();

    private static StompNotificationSessionHandler self;

    public StompNotificationSessionHandler(){
        self = this;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {

        this.stompSession = session;
        myFacilityCode = propServ.getProperty("facility.code",false).getValue();
        stompSession.subscribe("/topic/stock-transfer-notification/" + myFacilityCode, new StompFrameHandler() {

            @Override
            public Type getPayloadType(StompHeaders headers) {
                return Object.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                String payloadAsJSON = jSONMarshaller.toJson(payload);
                Notification notificationPayload = jSONMarshaller.fromJson(payloadAsJSON, Notification.class);
                System.out.println("Received Notifications");
                System.out.println(payloadAsJSON);
                localWebSocketSessionHandler.sendNotification(notificationPayload);
            }
        });

        System.out.println("Stomp WebSocket established from Facility Server");
    }

    public Boolean areWeConnected(){
        return stompSession != null ? stompSession.isConnected() : false;
    }

    public void setNotifyURL(String notifyURL){
        this.notifyURL = notifyURL;
    }

    public String getMyFacilityCode(){
        return  propServ.getProperty("facility.code",false).getValue();
    }
    //TODO: improve this implementation
    public static void setLocalWebSocketClient(LocalWebSocketSessionHandler instance){
        localWebSocketSessionHandler = instance;
        localWebSocketSessionHandler.setMiddleWareNotificationWebSocketSessionHandler(stompSession);
        localWebSocketSessionHandler.setMyFacilityCode(self.getMyFacilityCode());
    }

}