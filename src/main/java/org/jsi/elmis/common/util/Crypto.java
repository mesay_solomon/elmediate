/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.common.util;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Crypto {


	private static final String PASSWORD_HASH_ALGORITHM = "SHA-512";
    private static final String FE_PASSWORD_HASH_ALGORITHM = "SHA-256";
	private static final String PASSWORD_HASH_ENCODING = "UTF-8";
	private static final String algorithm = "AES";
	private static final byte[] keyValue  = new byte[] { 'e','L','M','I','S','@','S', 'e', 'c', 'r','e', 't', 'K', 'e', 'y','@' };

	public static String encrypt(String Data) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(algorithm);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		byte[] encryptedValue = Base64.encodeBase64(encVal);
		return encryptedValue.toString();
	}

	public static String decrypt(String encryptedData) {
		try{
			Key key = generateKey();
			Cipher c = Cipher.getInstance(algorithm);
			c.init(Cipher.DECRYPT_MODE, key);
			byte[] decordedValue = Base64.decodeBase64(encryptedData);
			byte[] decValue = c.doFinal(decordedValue);
			String decryptedValue = new String(decValue);
			return decryptedValue;
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return null;
		}
	}
	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, algorithm);
		return key;
	}



	  public static String hash(String plainText) {
	    String hashValue = null;
	    if (plainText == null) return null;
	    try {
	      MessageDigest msgDigest = MessageDigest.getInstance(PASSWORD_HASH_ALGORITHM);
	      msgDigest.update(plainText.getBytes(PASSWORD_HASH_ENCODING));
	      //TODO : use a salt for hashing

	      byte rawByte[] = msgDigest.digest();
	      byte[] encodedBytes = Base64.encodeBase64(rawByte);

	      return base64ToBase62(new String(encodedBytes));
	    } catch (UnsupportedEncodingException e) {
	      //consume this exception-caller does not need to know of the implementation for encoding
	    } catch (NoSuchAlgorithmException e) {
	      //consume this exception-caller does not need to know of this implementation for hashing algorithm
	    }
	    return hashValue;
	  }

	  protected static String base64ToBase62(String base64) {
	    StringBuffer buf = new StringBuffer(base64.length() * 2);

	    for (int i = 0; i < base64.length(); i++) {
	      char ch = base64.charAt(i);
	      switch (ch) {
	        case 'i':
	          buf.append("ii");
	          break;

	        case '+':
	          buf.append("ip");
	          break;

	        case '/':
	          buf.append("is");
	          break;

	        case '=':
	          buf.append("ie");
	          break;

	        case '\n':
	          // Strip out
	          break;

	        default:
	          buf.append(ch);
	      }
	    }


	    return buf.toString();
	  }

    public static String hashToSHA256(String stringToHash) {

        if(null == stringToHash){
            return null;
        }

        MessageDigest encryptPassword;

        byte[] encryptedPasswordBytes;
        StringBuilder encryptedPasswordBuilder = new StringBuilder();
        try {
            encryptPassword = MessageDigest.getInstance(FE_PASSWORD_HASH_ALGORITHM);
            encryptedPasswordBytes = encryptPassword.digest(stringToHash.getBytes("UTF-8"));
            for (int i = 0; i < encryptedPasswordBytes.length; i++) {
                encryptedPasswordBuilder.append(Integer.toString((encryptedPasswordBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Crypto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Crypto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return encryptedPasswordBuilder.toString();
    }

}
