package org.jsi.elmis.rest.result;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Component
public class RestResponse {
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";

    private Map<String, Object> data = new HashMap<>();

    public static org.springframework.http.ResponseEntity<RestResponse> response(String key, Object value, HttpStatus status) {
        return new ResponseEntity<>(new RestResponse(key, value), status);
    }

    public static ResponseEntity<RestResponse> response(String key, Object value) {
        return new ResponseEntity<>(new RestResponse(key, value), HttpStatus.OK);
    }

    public static ResponseEntity<RestResponse> error(String errorMsgCode, HttpStatus statusCode) {
        return new ResponseEntity<>(new RestResponse(ERROR, errorMsgCode), statusCode);
    }

    public RestResponse(String key, Object data) {
        this.data.put(key, data);
    }
}
