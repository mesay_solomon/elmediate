/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.rest.result;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.RegimenCombinationProductDTO;

/**
 * @author Mesay S. Taye
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ARVClientResult {
	private FacilityClient client;
	private List<RegimenProductCombination> regimenCombos;
	private RegimenProductCombination defaultCombo;
	private List<RegimenCombinationProductDTO> products;
	private Date appointmentDate;
	private FacilityClientVital facilityClientVitals;
	private FacilityClientViralLoad facilityClientViralLoad;
	private FacilityClientCD4Count facilityClientCD4Count;
    private ArrayList<FacilityClientPrescription> facilityClientPrescriptions;
}
