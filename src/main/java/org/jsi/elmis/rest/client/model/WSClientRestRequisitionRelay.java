package org.jsi.elmis.rest.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.log4j.Logger;
import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.criteria.RequisitionSearchCriteria;
import org.jsi.elmis.rest.client.ClientCommon;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rest.result.RestResponse;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.jsi.elmis.common.constants.ELMISConstants.RNR;
import static org.jsi.elmis.common.constants.ELMISConstants.RNRS;
import static org.springframework.http.HttpStatus.*;

@Component
public class WSClientRestRequisitionRelay extends ClientCommon {
    private static final Logger logger = Logger.getLogger(WSClientRestRequisitionRelay.class);

    @Autowired
    PropertiesService propertiesService;

    @Autowired
    MiscellaneousService miscellaneousService;

    @Autowired
    CommonDAO commonDAO;

    private Gson jSONMarshaller(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").registerTypeAdapter(Date.class,
                (JsonDeserializer<Date>) (json, typeOfT, context) -> new Date(json.getAsJsonPrimitive().getAsLong()))
                .create();
        return gson;
    }

    public ResponseEntity SendJSON(String json, String url) {

        try{
            logger.debug("calling API on : " + url);
            webResource = getAuthenticatedClient().resource(url);
            response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, json);
        }catch(Exception ex){
            logger.error(ex);
            return null;
        }

        logger.debug("response " + response);

        String payload = response.getEntity(String.class);

        return RestResponse.response(RNR, payload, CREATED);
    }

    public ResponseEntity<RestResponse> initiateRnr(String url, Integer facilityId, Integer programId, Boolean emergency, Integer periodId, String sourceApplication) {
        try{
            logger.debug("calling API on : " + url);

            StringBuilder initiateRnrUrl = new StringBuilder(url);
            initiateRnrUrl.append("?facilityId=")
                    .append(facilityId)
                    .append("&programId=")
                    .append(programId)
                    .append("&emergency=")
                    .append(emergency)
                    .append("&periodId=")
                    .append(periodId)
                    .append("&sourceApplication=")
                    .append(sourceApplication);

            webResource = getAuthenticatedClient().resource(initiateRnrUrl.toString());
            response = webResource.accept("application/json").type("application/json").post(ClientResponse.class);
        }catch(Exception ex){
            logger.error(ex);
            return null;
        }

        logger.debug("response " + response);

        Map<String, Object> payload = response.getEntity(Map.class);

        if(payload.containsKey("error")){
            return RestResponse.error("Error", BAD_REQUEST);
        }

        return RestResponse.response(RNR, payload.get(RNR));
    }

    public ResponseEntity<RestResponse> searchRnrs(String url, RequisitionSearchRequest requisitionSearchRequest) {
        String json = jSONMarshaller().toJson(requisitionSearchRequest);
        try{
            logger.debug("calling API on : " + url);
            webResource = getAuthenticatedClient().resource(url);
            response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, json);
        }catch(Exception ex){
            logger.error(ex);
            return null;
        }

        logger.debug("response " + response);

        String payload = response.getEntity(String.class);

        return RestResponse.response(RNRS, payload);
    }
    public ResponseEntity getLookupData(String lookupKey) {
        String baseURL = propertiesService.getProperty("elmis.central.url", false).getValue();
        String url = baseURL + "/rest-api/lookup/" + lookupKey + "?paging=false";
        if (getResponse(url)) return null;

        logger.debug("response " + response);

        String payload = response.getEntity(String.class);

        return new ResponseEntity<>(payload, OK);
    }

    public ResponseEntity getLookupData(String lookupKey, Boolean cached) {

        String payload = null;
        if( cached){

            payload = commonDAO.getResponse(lookupKey).getResponse();


        } else {
            String baseURL = propertiesService.getProperty("elmis.central.url", false).getValue();
            String url = baseURL + "/rest-api/lookup/" + lookupKey + "?paging=false";
            if (getResponse(url)) return null;

            logger.debug("response " + response);

            payload = response.getEntity(String.class);
        }


        return new ResponseEntity<>(payload, OK);
    }

    private boolean getResponse(String url) {
        try{
            logger.debug("calling API on : " + url);
            webResource = getAuthenticatedClient().resource(url);
            response = webResource.accept("application/json").type("application/json").get(ClientResponse.class);
        }catch(Exception ex){
            logger.error(ex);
            return true;
        }
        return false;
    }

    public ResponseEntity getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(String url, RequisitionSearchCriteria criteria) {
        String json = jSONMarshaller().toJson(criteria);
        Map<String, Object> responseMap = null;
        try{
            logger.debug("calling API on : " + url);
            webResource = getAuthenticatedClient().resource(url);
            responseMap = webResource.accept("application/json").type("application/json").post(Map.class, json);
        }catch(Exception ex){
            logger.error(ex);
            return null;
        }

        if(responseMap.containsKey("error")){
            return RestResponse.error( jSONMarshaller().toJson(responseMap), BAD_REQUEST);
        }


        List<Rnr> requisitions = null;
        List<ProcessingPeriod> processingPeriods = null;
        String responsePeriodPayload = jSONMarshaller().toJson(responseMap.get("periods"));
        processingPeriods = jSONMarshaller().fromJson(responsePeriodPayload, new TypeToken<ArrayList<ProcessingPeriod>>() {
        }.getType());


        ProcessingPeriod fePeriod;
        for(ProcessingPeriod pp : processingPeriods){
            fePeriod = miscellaneousService.getByCeEquivalentPeriodId(pp.getId());
            if(fePeriod != null) {
                pp.setId(fePeriod.getId());
            }else{
                return RestResponse.error("invalid.period.detected", HttpStatus.BAD_REQUEST);
            }
        }


        return new ResponseEntity<>(new LogisticsPeriodsSuccessResponse(requisitions, processingPeriods, null), OK);
    }


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class LogisticsPeriodsSuccessResponse{
        private List<Rnr> requisitions;
        private List<ProcessingPeriod> periods;
        private Boolean isEmergency;

    }

    @Override
    protected Client getAuthenticatedClient(){
        String middleWareUser = propertiesService.getProperty("elmis.central.approver.name", false).getValue();
        String middleWarePassword = propertiesService.getProperty("elmis.central.password", false).getValue();

        HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(middleWareUser, middleWarePassword);
        Client authenticatedClient = getWSClient();
        authenticatedClient.addFilter(authFilter);
        return authenticatedClient;
    }
}
