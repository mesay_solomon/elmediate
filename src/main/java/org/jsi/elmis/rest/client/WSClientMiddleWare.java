/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.jsi.elmis.model.*;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.UserManagementService;
import org.jsi.elmis.websocket.notifications.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Mekbib on 6/23/2017.
 */
@Component
public class WSClientMiddleWare extends ClientCommon {
    @Autowired
    PropertiesService propertiesService;
    @Autowired
    UserManagementService userManagementService;
    private final Logger log = Logger.getLogger(getClass());

    private final Gson jSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();

    public Integer requestStockTransfer(StockTransfer stockTransfer) {
        Map<String, Object> middleWareResponse = makeWebServiceCall(propertiesService.getProperty("integration.middleware.url.stocktransfer", false).getValue(), stockTransfer, RequestMethod.POST);
        return Integer.valueOf(middleWareResponse.get("stockTransferId").toString());
    }

    public Integer updateStockTransfer(StockTransfer stockTransfer) {
        Map<String, Object> middleWareResponse = makeWebServiceCall(propertiesService.getProperty("integration.middleware.url.stocktransfer", false).getValue(), stockTransfer, RequestMethod.PUT);
        return Integer.valueOf(middleWareResponse.get("stockTransferId").toString());
    }

    public Integer updateNotification(Notification notification) {
        Map<String, Object> middleWareResponse = makeWebServiceCall(propertiesService.getProperty("integration.middleware.url.notification", false).getValue(), notification, RequestMethod.PUT);
        return Integer.valueOf(middleWareResponse.get("notificationId").toString());
    }

    public StockTransfer getStockTransferById(Integer stockTransferId) {
        Map<String, Object> middleWareResponse = null;
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();

        try{
            StringBuilder getStockTransferById = new StringBuilder();
            getStockTransferById
                    .append(integrationMiddleWareBaseURL).append(propertiesService.getProperty("integration.middleware.url.stocktransfer", false).getValue())
                    .append("/")
                    .append(stockTransferId);
            log.debug("posting to : " + getStockTransferById.toString());
            webResource = getAuthenticatedClient().resource(getStockTransferById.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(), Map.class);
            if(middleWareResponse.containsKey("error")){
                log.debug(middleWareResponse.get("error").toString());
                return null;
            }
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return null;
        }


        String responsePayload = getGson().toJson(middleWareResponse.get("stockTransfer"));

        StockTransfer stockTransferResponse = getGson().fromJson(responsePayload,StockTransfer.class);

        return stockTransferResponse;
    }

    Map<String, Object> makeWebServiceCall(String url, Object payload, RequestMethod method){
        String jsonPayLoad = jSONMarshaller.toJson(payload);
        Map<String, Object> middleWareResponse = null;
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();

        try{
            StringBuilder requestStockRestUriSB = new StringBuilder();
            requestStockRestUriSB.append(integrationMiddleWareBaseURL).append(url);
            log.debug("posting to : " + requestStockRestUriSB.toString());
            log.debug("PayLoad : " + jsonPayLoad);
            webResource = getAuthenticatedClient().resource(requestStockRestUriSB.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(method.name(), Map.class, jsonPayLoad);
            if(middleWareResponse.containsKey("error")){
                log.debug(middleWareResponse.get("error").toString());
                return null;
            }
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return null;
        }

        return middleWareResponse;
    }

    //TODO: implement this better?
    protected Client getAuthenticatedClient(){
        String middleWareUser = propertiesService.getProperty("integration.middleware.user", false).getValue();
        String middleWarePassword = propertiesService.getProperty("integration.middleware.password", false).getValue();

        HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(middleWareUser, middleWarePassword);
        Client authenticatedClient = getWSClient();
        authenticatedClient.addFilter(authFilter);
        return authenticatedClient;
    }

	public List<Notification> getStockTransferNotifications(String userUUID, String status) {

		String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
		String integrationMiddleWareNotificationURL = propertiesService.getProperty("integration.middleware.url.notification", false).getValue();
		String myFacilityCode = propertiesService.getProperty("facility.code", false).getValue();
		List<Notification> notifications = null;
        Map<String, Object> middleWareResponse = null;
		try{
			StringBuilder requestStockRestUriSB = new StringBuilder();
			requestStockRestUriSB.append(integrationMiddleWareBaseURL)
					.append(integrationMiddleWareNotificationURL)
					.append("?userUUID=")
                    .append(userUUID)
                    .append("&facilityCode=")
                    .append(myFacilityCode)
                    .append("&status=")
                    .append(status);

			log.debug("requesting from : " + requestStockRestUriSB.toString());
			webResource = getAuthenticatedClient().resource(requestStockRestUriSB.toString());

			middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(),Map.class);

			String output = getGson().toJson(middleWareResponse.get("stockTransferNotifications"));

			notifications = getGson().fromJson(output,
					new TypeToken<ArrayList<Notification>>() {
					}.getType());

		}catch(Exception ex){
			log.error("FAILED : " + ex.getMessage());
			return Collections.emptyList();
		}


		return notifications;
	}


	public List<StockTransferAugmented> getFacilityStockTransfers(){
        String myFacilityCode = propertiesService.getProperty("facility.code", false).getValue();
        List<StockTransferAugmented> stockTransferAugmentedList;
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String integrationMiddleWareFacilityStockTransferAugmentedURL = propertiesService.getProperty("integration.middleware.url.facilitystocktransfersaugmented", false).getValue();
        Map<String, Object> middleWareResponse = null;
        try{
            StringBuilder requestStockRestUriSB = new StringBuilder();
            requestStockRestUriSB.append(integrationMiddleWareBaseURL)
                    .append(integrationMiddleWareFacilityStockTransferAugmentedURL)
                    .append("/")
                    .append(myFacilityCode);
            log.debug("requesting from : " + requestStockRestUriSB.toString());
            webResource = getAuthenticatedClient().resource(requestStockRestUriSB.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(),Map.class);
            String output = getGson().toJson(middleWareResponse.get("stockTransferAugmented"));
            stockTransferAugmentedList = getGson().fromJson(output,
                    new TypeToken<ArrayList<StockTransferAugmented>>() {
                    }.getType());
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return Collections.emptyList();
        }
        return stockTransferAugmentedList;
    }

    public StockTransferAugmented getStockTransferAugmented(Integer stockTransferId){
        StockTransferAugmented stockTransferAugmented = null;
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String integrationMiddleWareStockTransferAugmentedURL = propertiesService.getProperty("integration.middleware.url.stocktransferaugmented", false).getValue();
        Map<String, Object> middleWareResponse;
        try{
            StringBuilder requestStockRestUriSB = new StringBuilder();
            requestStockRestUriSB.append(integrationMiddleWareBaseURL)
                    .append(integrationMiddleWareStockTransferAugmentedURL)
                    .append("/")
                    .append(stockTransferId);

            log.debug("requesting from : " + requestStockRestUriSB.toString());
            webResource = getAuthenticatedClient().resource(requestStockRestUriSB.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(),Map.class);
            String output = getGson().toJson(middleWareResponse.get("stockTransferAugmented"));
            stockTransferAugmented = getGson().fromJson(output, StockTransferAugmented.class);
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
        }
        return stockTransferAugmented;
    }

    public List<StockTransferAugmented> getStockTransfers(String requesterCode, String supplerCode, Date initiationFrom, Date initiationTo, String status, Date lastUpdateFrom, Date lastUpdateTo) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy");
        List<StockTransferAugmented> stockTransferAugmentedList;
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String integrationMiddleWareFacilityStockTransferAugmentedURL = propertiesService.getProperty("integration.middleware.url.stocktransferaugmented", false).getValue();
        Map<String, Object> middleWareResponse;
        try{
            StringBuilder requestStockRestUriSB = new StringBuilder();
            requestStockRestUriSB.append(integrationMiddleWareBaseURL)
                    .append(integrationMiddleWareFacilityStockTransferAugmentedURL)
                    .append("?");
            if(status != null){
                requestStockRestUriSB.append("status="+status+"&");
            }
            if(requesterCode != null){
                requestStockRestUriSB.append("requester-code="+requesterCode+"&");
            }
            if(supplerCode != null){
                requestStockRestUriSB.append("supplier-code="+supplerCode+"&");
            }
            if(initiationFrom != null){
                requestStockRestUriSB.append("initiation-from="+sdf.format(initiationFrom)+"&");
            }
            if(initiationTo != null){
                requestStockRestUriSB.append("initiation-to="+sdf.format(initiationTo)+"&");
            }
            if(lastUpdateFrom != null){
                requestStockRestUriSB.append("last-update-from="+sdf.format(lastUpdateFrom)+"&");
            }
            if(lastUpdateTo != null){
                requestStockRestUriSB.append("last-update-to="+sdf.format(lastUpdateTo)+"&");
            }

            log.debug("requesting from : " + requestStockRestUriSB.toString());
            webResource = getAuthenticatedClient().resource(requestStockRestUriSB.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(),Map.class);
            String output = getGson().toJson(middleWareResponse.get("stockTransferAugmented"));
            stockTransferAugmentedList = getGson().fromJson(output,
                    new TypeToken<ArrayList<StockTransferAugmented>>() {
                    }.getType());
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return Collections.emptyList();
        }
        return stockTransferAugmentedList;
    }

    public int createInteractionPolicies(List<FacilityInteractionPolicy> interactionPolicies) {
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String createInteractionPoliciesURL = propertiesService.getProperty("integration.middleware.url.createiniteractionpolicy", false).getValue();
        Integer createResponse;
        try{
            StringBuilder sbCreateInteractionPoliciesURL = new StringBuilder(integrationMiddleWareBaseURL);
            sbCreateInteractionPoliciesURL.append(createInteractionPoliciesURL);
            log.debug("requesting from : " + sbCreateInteractionPoliciesURL.toString());
            webResource = getAuthenticatedClient().resource(sbCreateInteractionPoliciesURL.toString());
            createResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.POST.name(), Integer.class, interactionPolicies);
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return -1;
        }
        return createResponse;
    }

    public int deleteInterationPolicies(List<FacilityInteractionPolicy> interactionPolicies) {
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String deleteInteractionPoliciesURL = propertiesService.getProperty("integration.middleware.url.deleteiniteractionpolicy", false).getValue();
        Integer deleteResponse;
        try{
            StringBuilder sbDeleteInteractionPoliciesURL = new StringBuilder(integrationMiddleWareBaseURL);
            sbDeleteInteractionPoliciesURL.append(deleteInteractionPoliciesURL);
            log.debug("requesting from : " + sbDeleteInteractionPoliciesURL.toString());
            webResource = getAuthenticatedClient().resource(sbDeleteInteractionPoliciesURL.toString());
            deleteResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.DELETE.name(), Integer.class, interactionPolicies);
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return -1;
        }
        return deleteResponse;
    }

    public List<Facility> getFacilitiesInteractingWithFacility(String facilityCode, String actionType) {
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        String getInteractionPoliciesURL = propertiesService.getProperty("integration.middleware.url.getiniteractionpolicies", false).getValue();
        List<Facility> facilityInteractionPolicies;
        try{
            StringBuilder sbGetInteractionPoliciesURL = new StringBuilder(integrationMiddleWareBaseURL);
            sbGetInteractionPoliciesURL.append(getInteractionPoliciesURL).append("/").append(facilityCode).append("/").append(actionType);
            log.debug("requesting from : " + sbGetInteractionPoliciesURL.toString());
            webResource = getAuthenticatedClient().resource(sbGetInteractionPoliciesURL.toString());
            ClientResponse response = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(), ClientResponse.class);
            String output = response.getEntity(String.class);
            facilityInteractionPolicies = getGson().fromJson(output,
                    new TypeToken<ArrayList<Facility>>() {
                    }.getType());
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return Collections.emptyList();
        }

        return facilityInteractionPolicies;
    }

    public List<Facility> getAllFacilities(){
        String integrationMiddleWareBaseURL = propertiesService.getProperty("integration.middleware.url.base", false).getValue();
        List<Facility> facilities;
        Map<String, Object> middleWareResponse;
        try{
            StringBuilder sbGetAllMiddlewareFacilities = new StringBuilder(integrationMiddleWareBaseURL);
            sbGetAllMiddlewareFacilities.append("/rest-api/lookup/facilities?paging=false");
            log.debug("requesting from : " + sbGetAllMiddlewareFacilities.toString());
            webResource = getAuthenticatedClient().resource(sbGetAllMiddlewareFacilities.toString());
            middleWareResponse = webResource.accept("application/json").type("application/json").method(RequestMethod.GET.name(), Map.class);
            String output = getGson().toJson(middleWareResponse.get("facilities"));
            facilities = getGson().fromJson(output,
                    new TypeToken<ArrayList<Facility>>() {
                    }.getType());
        }catch(Exception ex){
            log.error("FAILED : " + ex.getMessage());
            return Collections.emptyList();
        }
        return facilities;
    }
}
