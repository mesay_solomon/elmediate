/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client;

import org.apache.log4j.Logger;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.RnRLineItem;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Component
public class RnRInterfacing extends ClientCommon {
    @Autowired
    PropertiesService propertiesService;
    @Autowired
    RnRService rnRService;


    private final Logger log = Logger.getLogger(getClass());
    public Rnr initiateRnr(Long facilityId, Long programId,  Long periodId, String sourceApplication,  Boolean emergency, String sourceApp) {
        Rnr rnr;
        Map<String, Object> responseMap;
        String centralELMISURL = propertiesService.getProperty("elmis.central.url", false).getValue();
        try {
            StringBuilder interfacingUrl = new StringBuilder();
            String baseUrl="/rest-api/requisitions/initiate?";
            interfacingUrl
                    .append(centralELMISURL).append(baseUrl)
                    .append("facilityId=" + facilityId + "&")
                    .append("programId=" + programId + "&")
                    .append("periodId=" + periodId + "&")
                    .append("sourceApplication=" + sourceApp + "&")
                    .append("emergency=" + emergency);
            log.debug("posting to : " + baseUrl.toString());
            webResource = getAuthenticatedClient().resource(interfacingUrl.toString());
            responseMap = webResource.accept("application/json").header("X-Auth-Token", getAuthToken()).type("application/json").post(Map.class);

            if (responseMap.containsKey("error")) {
                log.debug(responseMap.get("error").toString());
                return null;
            }
        } catch (Exception ex) {
            return null;
        }

        rnr=convertResponse(responseMap);
        return rnr;
    }
    private Rnr convertResponse(  Map<String, Object> responseMap){
        Rnr rnr=null;
        String responsePayload = getGson().toJson(responseMap.get("requisitionId"));
        log.debug("response " + responsePayload);
        rnr = getGson().fromJson(responsePayload, Rnr.class);
        List<RnRLineItem> allLineItems = new ArrayList<>();
        allLineItems.addAll(rnr.getFullSupplyLineItems());
        allLineItems.addAll(rnr.getNonFullSupplyLineItems());
        rnr.setAllLineItems(allLineItems);
        return rnr;
    }

}
