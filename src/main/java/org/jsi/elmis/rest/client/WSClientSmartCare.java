/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.log4j.Logger;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mekbib on 12/28/2016.
 */
@Component
public class WSClientSmartCare extends ClientCommon {
	private final Logger log = Logger.getLogger(getClass());
	
	@Value("${smartcare.integration.baseurl}")
	private String smartcareURL;
	@Value("${smartcare.integration.registration}")
	private String smartcareRegistrationAPIURL;
	@Value("${smartcare.integration.registration.byuuid}")
	private String smartcareRegistrationByUUIDAPIURL;
	@Value("${smartcare.integration.registration.all}")
	private String smartcareAllRegistrationAPIURL;
	@Value("${smartcare.integration.auth}")
	private String smartcareAuthURL;
	@Value("${smartcare.integration.user}")
	private String smartcareUser;
	@Value("${smartcare.integration.password}")
	private String smartcarePassword;
	@Value("${smartcare.integration.numberOfDaysToPullRegistrationsFor}")
	private String numberOfDaysToPullRegistrationsFor;

    @Value("${quartz.integration.enabled}")
    private Boolean integrationEnabled;
	
	private SimpleDateFormat sdfSmartCareDateFormat = new SimpleDateFormat("MMM-dd-yyyy");
	
	public ArrayList<SmartCarePatientRegistration> pullClientRegistrationFromSmartCare(Integer newOrUpdated, Date startDate, Date endDate) {
		if(getToken() == null){
			return null;
		}
		ArrayList<SmartCarePatientRegistration> patientRegResolution = new ArrayList<>();
		ClientResponse response;
		try {

			StringBuilder sbClientRegistrationURI = new StringBuilder();
			sbClientRegistrationURI.append(smartcareURL)
					.append(smartcareRegistrationAPIURL)
					.append("/")
					.append(newOrUpdated)
					.append("/")
					.append(sdfSmartCareDateFormat.format(startDate))
					.append("/")
					.append(sdfSmartCareDateFormat.format(endDate));

			
			String _token = getToken();
			
			log.debug("pulling registration from " + sbClientRegistrationURI.toString());
			response = getWSClient().resource(sbClientRegistrationURI.toString())
					.type("application/json")
					.header("X-Auth-Token", _token)
					.accept("application/json").get(ClientResponse.class);
			
		} catch (Exception ex) {
			log.debug("FAILED : " + ex.getMessage());
			return null;
		}
		if (response.getStatus() != 200) {
			return null;
		}
		log.debug("Response" + response);
		String output = response.getEntity(String.class);

		log.debug("Response" + output);
		Gson smartCareJSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
		try {
			patientRegResolution = smartCareJSONMarshaller.fromJson(output,
					new TypeToken<ArrayList<SmartCarePatientRegistration>>() {
					}.getType());
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}
		return patientRegResolution;
	}

	public ArrayList<SmartCarePatientRegistration> allPatientRegistrations() {
		if(getToken()==null){
			return null;
		}
		ArrayList<SmartCarePatientRegistration> patientRegResolution;
		ClientResponse response;
		try {

			StringBuilder sbClientAllRegistrationsURI = new StringBuilder();
			sbClientAllRegistrationsURI.append(smartcareURL)
					.append(smartcareAllRegistrationAPIURL);

			String _token = getToken();

			log.debug("getting all registrations from SmartCare " + sbClientAllRegistrationsURI.toString());
			response = getWSClient().resource(sbClientAllRegistrationsURI.toString())
					.type("application/json")
					.header("X-Auth-Token", _token)
					.accept("application/json").get(ClientResponse.class);

		} catch (Exception ex) {
			log.debug("FAILED : " + ex.getMessage());
			return new ArrayList<>();
		}
		if (response.getStatus() != 200) {
			return new ArrayList<>();
		}
		log.debug("Response" + response);
		String output = response.getEntity(String.class);

		log.debug("Response" + output);
		Gson smartCareJSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
		try {
			patientRegResolution = smartCareJSONMarshaller.fromJson(output,
					new TypeToken<ArrayList<SmartCarePatientRegistration>>() {
					}.getType());
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			return null;
		}
		return patientRegResolution;
	}

	public SmartCarePatientRegistration getPatientRegistrationByUUID(String uuid){
		if(getToken() == null){
			return null;
		}
		ClientResponse response;
		SmartCarePatientRegistration smartCarePatientRegistration = null;
		try {
			//TODO: SmartCare should use the same format
			uuid = uuid.replace("-","");
			StringBuilder sbClientRegistrationByUUIDURI = new StringBuilder();
			sbClientRegistrationByUUIDURI.append(smartcareURL)
					.append(smartcareRegistrationByUUIDAPIURL)
					.append("/")
					.append(uuid);

			String _token = getToken();

			log.debug("getting registration from SmartCare " + sbClientRegistrationByUUIDURI.toString());
			response = getWSClient().resource(sbClientRegistrationByUUIDURI.toString())
					.type("application/json")
					.header("X-Auth-Token", _token)
					.accept("application/json").get(ClientResponse.class);

		} catch (Exception ex) {
			log.debug("FAILED : " + ex.getMessage());
			return  null;
		}
		if (response.getStatus() != 200) {
			return  null;
		}

		String output = response.getEntity(String.class);
		log.debug("Response " + output);
		Gson smartCareJSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
		smartCarePatientRegistration = smartCareJSONMarshaller.fromJson(output,SmartCarePatientRegistration.class );
		return smartCarePatientRegistration;
	}

	private String getToken() {

	    if(! integrationEnabled ){
	        return null;
        }

		ClientResponse response;
		try {
			Gson gson = new Gson();
			StringBuilder sbSmartCareAuthURI = new StringBuilder();
			sbSmartCareAuthURI.append(smartcareURL)
					.append(smartcareAuthURL);

			Map<String, String> userCred = new HashMap<>();
			userCred.put("UserName", smartcareUser);
			userCred.put("Password", smartcarePassword);
			
			log.debug("authenticating user on " + sbSmartCareAuthURI.toString() +
					" PayLoad : "+gson.toJson(userCred));
			response = getWSClient().resource(sbSmartCareAuthURI.toString()).accept("application/json")
					.type("application/json").post(ClientResponse.class, gson.toJson(userCred));
		} catch (Exception ex) {
			log.error("FAILED : " + ex.getMessage());
			log.debug("\n\n------------------------\n\n SMARTCARE Server @ " + smartcareURL + " Unreachable!! \n\n------------------------");
			return null;
		}
		if (response.getStatus() != 200) {
			log.debug("\n\n------------------------\n\n SMARTCARE Server @ " + smartcareURL + " Unreachable!! \n\n------------------------");
			return null;
		}
		String output = response.getEntity(String.class);
		log.debug("Token " + output);
		return output.replace("\"", "");
	}
}
