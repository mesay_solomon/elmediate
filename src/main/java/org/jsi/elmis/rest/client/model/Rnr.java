/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jsi.elmis.model.BaseModel;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.rnr.RegimenLineItem;
import org.jsi.elmis.rnr.RnRLineItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion.NON_NULL;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = NON_NULL)
@EqualsAndHashCode(callSuper = false)
public class Rnr extends BaseModel {

    public static final String RNR_VALIDATION_ERROR = "error.rnr.validation";
    private boolean emergency;
    private Facility facility;
    private Program program;
    private ProcessingPeriod period;
    private RnrStatus status;
//    private Money fullSupplyItemsSubmittedCost = new Money("0");
//    private Money nonFullSupplyItemsSubmittedCost = new Money("0");
    private List<RnRLineItem> fullSupplyLineItems = new ArrayList<>();
    private List<RnRLineItem> nonFullSupplyLineItems = new ArrayList<>();
    private List<RegimenLineItem> regimenLineItems = new ArrayList<>();
//    private List<EquipmentLineItem> equipmentLineItems = new ArrayList<>();
//    private List<PatientQuantificationLineItem> patientQuantifications = new ArrayList<>();
    private BigDecimal allocatedBudget;
//    @Transient
//    @JsonIgnore
    private List<RnRLineItem> allLineItems = new ArrayList<>();

    private Facility supplyingDepot;
    private Long supplyingDepotId;
    private Long supervisoryNodeId;
    private Date submittedDate;
    private Date clientSubmittedTime;
    private String clientSubmittedNotes;
    private String sourceApplication = "WEB_UI";
//    private List<Comment> comments = new ArrayList<>();

//    private List<Signature> rnrSignatures;
//    private List<ManualTestesLineItem> manualTestLineItems = new ArrayList<>();

    public Rnr(Facility facility, Program program, ProcessingPeriod period, Boolean emergency, Long modifiedBy, Long createdBy) {
        this.facility = facility;
        this.program = program;
        this.period = period;
        this.emergency = emergency;
        this.getModifiedBy().setId(modifiedBy.intValue());
        this.getCreatedBy().setId(createdBy.intValue());
    }

}


