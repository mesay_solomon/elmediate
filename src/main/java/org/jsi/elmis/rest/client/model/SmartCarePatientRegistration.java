/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.rest.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.DateDeserializers.DateDeserializer;
import com.google.gson.annotations.SerializedName;
import org.jsi.elmis.model.FacilityClientCD4Count;
import org.jsi.elmis.model.FacilityClientPrescription;
import org.jsi.elmis.model.FacilityClientViralLoad;
import org.jsi.elmis.model.FacilityClientVital;

/**
 * Created by Mekbib on 12/28/2016.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SmartCarePatientRegistration{
    @JsonDeserialize(using=DateDeserializer.class)
    @SerializedName(value="RegistrationDate")
    private Date registrationDate;
    @SerializedName(value="DateOfBirth")
    private Date dateOfBirth;
    @SerializedName(value="ArtNumber")
    private String artNumber;
    @SerializedName(value="NrcNumber")
    private String nrcNumber;
    @SerializedName(value="RegimenCode")
    private String regimenCode;
    @SerializedName(value="RegimenId")
    private String regimenId;
    @SerializedName(value="FirstName")
    private String firstName;
    @SerializedName(value="LastName")
    private String lastName;
    @SerializedName(value="PatientGUID")
    private String patientGUID;
    @SerializedName(value="PatientID")
    private String patientID;
    @SerializedName(value="Sex")
    private String sex;
    @SerializedName(value="BirthDay")
    private String birthDay;
    @SerializedName(value="BirthMonth")
    private String birthMonth;
    @SerializedName(value="BirthYear")
    private String birthYear;
    @SerializedName(value="Sequence")
    private Long sequence;
    @SerializedName(value="Vitals")
    private FacilityClientVital vitals;
    @SerializedName(value="CD4")
    private FacilityClientCD4Count cd4;
    @SerializedName(value="ViralLoad")
    private FacilityClientViralLoad viralLoad;
    @SerializedName(value = "Prescription")
    private FacilityClientPrescription prescription;
}
