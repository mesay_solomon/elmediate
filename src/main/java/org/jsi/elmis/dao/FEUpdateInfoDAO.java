package org.jsi.elmis.dao;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FEUpdateInfoMapper;
import org.jsi.elmis.model.FEUpdateInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FEUpdateInfoDAO extends DAO {

    public void insert(FEUpdateInfo feUpdateInfo){
        SqlSession session = getSqlMapper().openSession();
        FEUpdateInfoMapper mapper = session.getMapper(FEUpdateInfoMapper.class);
        try {
            mapper.insert(feUpdateInfo);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            session.commit();
            session.close();
        }
    }

    public List<FEUpdateInfo> selectAll(){
        List<FEUpdateInfo> result = null;
        SqlSession session = getSqlMapper().openSession();
        FEUpdateInfoMapper mapper = session.getMapper(FEUpdateInfoMapper.class);
        try {
            result = mapper.selectAll();
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            session.close();
        }
        return result;
    }


    public List<FEUpdateInfo> selectByFacilityCode(String facilityCode){
        List<FEUpdateInfo> result = null;
        SqlSession session = getSqlMapper().openSession();
        FEUpdateInfoMapper mapper = session.getMapper(FEUpdateInfoMapper.class);
        try {
            result = mapper.selectByFacilityCode(facilityCode);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            session.close();
        }
        return result;
    }
}
