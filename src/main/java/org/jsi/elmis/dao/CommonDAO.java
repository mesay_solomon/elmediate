/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.*;
import org.jsi.elmis.model.*;
import org.jsi.elmis.reporting.report.IssueVoucherReport;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class CommonDAO extends DAO {
	public List<Program> getAllPrograms( ){

		SqlSession session = getSqlMapper().openSession();
		ProgramMapper mapper = session.getMapper(ProgramMapper.class);
		List<Program> programs = null;

		try {
			programs = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return programs;
	}

	public List<Facility> getAllFacilities(Integer geoZoneId ){

		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		List<Facility> facilities = null;

		try {
			facilities = mapper.selectAllByGeoZoneId(geoZoneId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilities;
	}

    public SyncResponseCache getResponse(String url){

        SqlSession session = getSqlMapper().openSession();
        SyncResponseCacheMapper mapper = session.getMapper(SyncResponseCacheMapper.class);
        SyncResponseCache response = null;

        try {
            response = mapper.selectByPrimaryKey(url);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return response;
    }

    public void saveResponse(SyncResponseCache response){

        SqlSession session = getSqlMapper().openSession();
        SyncResponseCacheMapper mapper = session.getMapper(SyncResponseCacheMapper.class);

        try {
            mapper.updateByPrimaryKey(response);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
    }


	public ProcessingPeriod getCurrentPeriod(String scheduleCode ){

		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;

		try {
			period = mapper.selectCurrentPeriod(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}

	public ProcessingPeriod getPreviousPeriod(String scheduleCode ){

		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		ProcessingPeriod period = null;

		try {
			period = mapper.selectPreviousPeriod(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}

	public FacilityDistrictAndProvinceResult selectDistrictAndProvince(String facilityCode){
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		FacilityDistrictAndProvinceResult facilityDistrictAndProvince = null;

		try {
			facilityDistrictAndProvince = mapper.selectDistrictAndProvince(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityDistrictAndProvince;
	}

	public List<ProcessingPeriod> getAllProcessingPeriods(String scheduleCode){

		SqlSession session = getSqlMapper().openSession();
		ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
		List<ProcessingPeriod> result = null;

		try {
			result = mapper.getAllProcessingPeriods(scheduleCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public Facility getFacilityById(Integer facilityId) {
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		Facility facility = null;

		try {
			facility = mapper.selectByPrimaryKey(facilityId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facility;
	}

	public Facility getFacilityByCode(String facilityCode) {
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		Facility facility = null;

		try {
			facility = mapper.selectByCode(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facility;
	}

	public List<TransactionType> getTransactionTypes() {
		SqlSession session = getSqlMapper().openSession();
		TransactionTypeMapper mapper = session.getMapper(TransactionTypeMapper.class);
		List<TransactionType> result = null;

		try {
			result = mapper.selectAll();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

    public Program getProgramById( Integer id){

        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        Program program = null;
        try {
            program = mapper.selectByPrimaryKey(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return program;
    }


    public List<IssueVoucherReport> getIssueVoucherItems(Integer txnId){
        SqlSession session = getSqlMapper().openSession();
        TransactionMapper mapper = session.getMapper(TransactionMapper.class);
        List<IssueVoucherReport> issueVoucherItems = null;
        try {
            issueVoucherItems = mapper.selectIssueVoucherItems(txnId);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return issueVoucherItems;
    }


    public Integer insertGeographicLevel(GeographicLevel geographicLevel) {
        SqlSession session = getSqlMapper().openSession();
        GeographicLevelMapper mapper = session.getMapper(GeographicLevelMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(geographicLevel);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateGeographicLevelByCode(GeographicLevel geographicLevel) {
        SqlSession session = getSqlMapper().openSession();
        GeographicLevelMapper mapper = session.getMapper(GeographicLevelMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(geographicLevel);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertGeographicZone(GeographicZone geographicZone) {
        SqlSession session = getSqlMapper().openSession();
        GeographicZoneMapper mapper = session.getMapper(GeographicZoneMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(geographicZone);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertGeographicZoneSelective(GeographicZone geographicZone) {
        SqlSession session = getSqlMapper().openSession();
        GeographicZoneMapper mapper = session.getMapper(GeographicZoneMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(geographicZone);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateGeographicZoneByCode(GeographicZone geographicZone) {
        SqlSession session = getSqlMapper().openSession();
        GeographicZoneMapper mapper = session.getMapper(GeographicZoneMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(geographicZone);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertFacilityType(FacilityType facilityType) {
        SqlSession session = getSqlMapper().openSession();
        FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(facilityType);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertFacilityTypeSelective(FacilityType facilityType) {
        SqlSession session = getSqlMapper().openSession();
        FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(facilityType);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateFacilityTypeByCode(FacilityType facilityType) {
        SqlSession session = getSqlMapper().openSession();
        FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(facilityType);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertProgram(Program program) {
        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(program);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertProgramSelective(Program program) {
        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(program);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateProgramByCode(Program program) {
        SqlSession session = getSqlMapper().openSession();
        ProgramMapper mapper = session.getMapper(ProgramMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(program);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertProcessingSchedule(ProcessingSchedule processingSchedule) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingScheduleMapper mapper = session.getMapper(ProcessingScheduleMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(processingSchedule);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertProcessingScheduleSelective(ProcessingSchedule processingSchedule) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingScheduleMapper mapper = session.getMapper(ProcessingScheduleMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(processingSchedule);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateProcessingScheduleByCode(ProcessingSchedule processingSchedule) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingScheduleMapper mapper = session.getMapper(ProcessingScheduleMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(processingSchedule);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }


    public Integer insertProcessingPeriod(ProcessingPeriod processingPeriod) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(processingPeriod);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertProcessingPeriodSelective(ProcessingPeriod processingPeriod) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(processingPeriod);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateProcessingPeriodByDateRange(ProcessingPeriod processingPeriod) {
        SqlSession session = getSqlMapper().openSession();
        ProcessingPeriodMapper mapper = session.getMapper(ProcessingPeriodMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByDateRange(processingPeriod);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertLossAndAdjustmentType(LossAdjustmentType lossAdjustmentType) {
        SqlSession session = getSqlMapper().openSession();
        LossAdjustmentTypeMapper mapper = session.getMapper(LossAdjustmentTypeMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(lossAdjustmentType);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateLossAndAdjustmentTypeByName(LossAdjustmentType lossAdjustmentType) {
        SqlSession session = getSqlMapper().openSession();
        LossAdjustmentTypeMapper mapper = session.getMapper(LossAdjustmentTypeMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByName(lossAdjustmentType);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertDosageUnit(DosageUnit dosageUnit) {
        SqlSession session = getSqlMapper().openSession();
        DosageUnitMapper mapper = session.getMapper(DosageUnitMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(dosageUnit);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertDosageUnitSelective(DosageUnit dosageUnit) {
        SqlSession session = getSqlMapper().openSession();
        DosageUnitMapper mapper = session.getMapper(DosageUnitMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(dosageUnit);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateDosageUnitByName(DosageUnit dosageUnit) {
        SqlSession session = getSqlMapper().openSession();
        DosageUnitMapper mapper = session.getMapper(DosageUnitMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByName(dosageUnit);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertDosageFrequency(DosageFrequency dosageFrequency) {
        SqlSession session = getSqlMapper().openSession();
        DosageFrequencyMapper mapper = session.getMapper(DosageFrequencyMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(dosageFrequency);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertDosageFrequencySelective(DosageFrequency dosageFrequency) {
        SqlSession session = getSqlMapper().openSession();
        DosageFrequencyMapper mapper = session.getMapper(DosageFrequencyMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(dosageFrequency);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateDosageFrequencyByName(DosageFrequency dosageFrequency) {
        SqlSession session = getSqlMapper().openSession();
        DosageFrequencyMapper mapper = session.getMapper(DosageFrequencyMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByName(dosageFrequency);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenLine(RegimenLine regimenLine) {
        SqlSession session = getSqlMapper().openSession();
        RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(regimenLine);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenLineSelective(RegimenLine regimenLine) {
        SqlSession session = getSqlMapper().openSession();
        RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(regimenLine);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateRegimenLineByCode(RegimenLine regimenLine) {
        SqlSession session = getSqlMapper().openSession();
        RegimenLineMapper mapper = session.getMapper(RegimenLineMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(regimenLine);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimen(Regimen regimen) {
        SqlSession session = getSqlMapper().openSession();
        RegimenMapper mapper = session.getMapper(RegimenMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(regimen);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenSelective(Regimen regimen) {
        SqlSession session = getSqlMapper().openSession();
        RegimenMapper mapper = session.getMapper(RegimenMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(regimen);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateRegimenByCode(Regimen regimen) {
        SqlSession session = getSqlMapper().openSession();
        RegimenMapper mapper = session.getMapper(RegimenMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(regimen);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenProductCombination(RegimenProductCombination regimenProductCombination) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(regimenProductCombination);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenProductCombinationSelective(RegimenProductCombination regimenProductCombination) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(regimenProductCombination);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer updateRegimenProductCombinationByName(RegimenProductCombination regimenProductCombination) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductCombinationMapper mapper = session.getMapper(RegimenProductCombinationMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByName(regimenProductCombination);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenCombinationProduct(RegimenCombinationProduct regimenCombinationProduct) {
        SqlSession session = getSqlMapper().openSession();
        RegimenCombinationProductMapper mapper = session.getMapper(RegimenCombinationProductMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(regimenCombinationProduct);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenCombinationProductSelective(RegimenCombinationProduct regimenCombinationProduct) {
        SqlSession session = getSqlMapper().openSession();
        RegimenCombinationProductMapper mapper = session.getMapper(RegimenCombinationProductMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(regimenCombinationProduct);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }



    public Integer updateRegimenCombinationProductByComboId(RegimenCombinationProduct regimenCombinationProduct) {
        SqlSession session = getSqlMapper().openSession();
        RegimenCombinationProductMapper mapper = session.getMapper(RegimenCombinationProductMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByProductCombinationId(regimenCombinationProduct);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenProductDosage(RegimenProductDosage regimenProductDosage) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);
        Integer result = null;
        try {
            result = mapper.insert(regimenProductDosage);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertRegimenProductDosageSelective(RegimenProductDosage regimenProductDosage) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(regimenProductDosage);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }




    public Integer updateRegimenProductDosageByCombinationId(RegimenProductDosage regimenProductDosage) {
        SqlSession session = getSqlMapper().openSession();
        RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCombinationProductId(regimenProductDosage);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

    public Integer insertGeographicLevelSelective(GeographicLevel geographicLevel) {
        SqlSession session = getSqlMapper().openSession();
        GeographicLevelMapper mapper = session.getMapper(GeographicLevelMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(geographicLevel);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
}
