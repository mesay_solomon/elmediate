package org.jsi.elmis.dao;/*
 * This program was produced for the U.S. Agency for International Development. It was prepared by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed under MPL v2 or later.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the Mozilla Public License as published by the Mozilla Foundation, either version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 *
 * You should have received a copy of the Mozilla Public License along with this program. If not, see http://www.mozilla.org/MPL/
 */

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ApprovalHierarchyDiagramNodeMapper;
import org.jsi.elmis.dao.mappers.RequestApproveHeirarchiMapper;
import org.jsi.elmis.dao.mappers.RequestPathGroupMapper;
import org.jsi.elmis.model.ApprovalHierarchyDiagramNode;
import org.jsi.elmis.model.RequestApproveHeirarchi;
import org.jsi.elmis.model.StockRequestPath;
import org.springframework.stereotype.Component;
import org.jsi.elmis.model.RequestPathGroup;

@Component
public class RequestApproveHeirarchiDAO extends  DAO{

    public List<StockRequestPath> loadRequestPathList;

    public Integer insert(RequestApproveHeirarchi approveHeirarchi){
        SqlSession session = getSqlMapper().openSession();
        RequestApproveHeirarchiMapper mapper = session.getMapper(RequestApproveHeirarchiMapper.class);
        int result = 0;

        try {
            mapper.insert(approveHeirarchi);
            result = approveHeirarchi.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    public RequestApproveHeirarchi loadUserRequestApprovalHeirarchi(Integer user, Integer path){
        SqlSession session = getSqlMapper().openSession();
        RequestApproveHeirarchiMapper mapper = session.getMapper(RequestApproveHeirarchiMapper.class);
        RequestApproveHeirarchi result = null;

        try {
            result=  mapper.loadUserRequestApprovalHeirarchi(user, path);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    public Integer saveHierarchyDiagramNode(List<ApprovalHierarchyDiagramNode> hierarchyDiagramNodes){
        SqlSession session = getSqlMapper().openSession();
        ApprovalHierarchyDiagramNodeMapper mapper = session.getMapper(ApprovalHierarchyDiagramNodeMapper.class);
        int result = 0;

        try {
        	for (ApprovalHierarchyDiagramNode approvalHierarchyDiagramNode : hierarchyDiagramNodes) {
        		mapper.insert(approvalHierarchyDiagramNode);
			}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }
    
    public Integer savePathGroup(RequestPathGroup requestPathGroup){
        SqlSession session = getSqlMapper().openSession();
        RequestPathGroupMapper mapper = session.getMapper(RequestPathGroupMapper.class);

        try {
        	
        		mapper.insert(requestPathGroup);
        		
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return requestPathGroup.getId();
    }
    
    public Integer deleteApprovalHierachy(Integer pathGroupId){
        SqlSession session = getSqlMapper().openSession();
		RequestApproveHeirarchiMapper mapper = session.getMapper(RequestApproveHeirarchiMapper.class);
        Integer result = null;
        try {
    		result = mapper.deleteApprovalHierarchy(pathGroupId);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

}
