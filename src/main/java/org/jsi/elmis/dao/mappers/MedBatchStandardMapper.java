package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.MedBatchStandard;

public interface MedBatchStandardMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MedBatchStandard record);

    int insertSelective(MedBatchStandard record);

    MedBatchStandard selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MedBatchStandard record);

    int updateByPrimaryKey(MedBatchStandard record);
}