/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao.mappers;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;

/**
 * @author Mesay S. Taye
 *
 */
public interface UserMapper {
	List<User> getUsers();
	
	List<Role> getRoles();
	
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);
    
    User selectByEmail(String email);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    ArrayList<UserNodeRoleRightResult> authenticate(User userToAuthenticate);
    
    ArrayList<UserNodeRoleRightResult> getUserNodeRoleRights(Integer userId);
    
    Boolean revokeRightsFromRole(RoleRight roleRight);
	
	Boolean revokeRolesFromUser(UserNodeRole userNodeRole);
	
	Boolean revokeNodesFromUser(UserNodeRole userNodeRole);

	ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr);

    int upsert(User record);

    int updateUserPassword(User record);

}