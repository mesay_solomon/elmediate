/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers.V2;


import org.apache.ibatis.annotations.*;
import org.jsi.elmis.model.Right;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleMapper {

    @Select("select distinct r.* from role_rights rr left outer join rights r on rr.rightname = r.name where rr.roleid = #{roleId}")
    List<Right> getRightsByRoleId(Integer roleId);

    @Select("select * from roles")
    @Results(
            value = {
            @Result(column = "id", property = "id"),
            @Result(column = "id", property = "rights", javaType = List.class,
                    many = @Many(select = "getRightsByRoleId"))
            }
    )
    List<Role> getAllRoles();

    @Insert("INSERT INTO roles(\n" +
            "             name, description, type, createddate,  modifieddate)\n" +
            "    VALUES (#{name}, #{description}, ' - ', now(), now())")
    @Options(useGeneratedKeys=true)
    void insertRole(Role role);

    @Insert("insert into role_rights (roleid, rightname) values (#{roleId}, #{rightName})")
    void insertRoleRights(@Param("roleId") Integer roleId, @Param("rightName") String roleName);

    @Update("update roles set name = #{name}, description = #{description} , modifieddate = now() where id = #{id}")
    void updateRole(Role role);

    @Select("select * from roles where id = #{roleId}")
    @Results(
            value = {
                    @Result(column = "id", property = "id"),
                    @Result(column = "id", property = "rights", javaType = List.class,
                            many = @Many(select = "getRightsByRoleId"))
            }
    )
    Role getRoleById(Integer roleId);

    @Delete("delete from role_rights where roleid = #{id}")
    void resetRolesRightByRoleId(Integer id);
}
