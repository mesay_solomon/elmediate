package org.jsi.elmis.dao.mappers;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.FEUpdateInfo;

import java.util.List;

public interface FEUpdateInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FEUpdateInfo record);

    int insertSelective(FEUpdateInfo record);

    FEUpdateInfo selectByPrimaryKey(Integer id);

    List<FEUpdateInfo> selectByFacilityCode(@Param("facilityCode") String facilityCode);

    List<FEUpdateInfo> selectAll();

    int updateByPrimaryKeySelective(FEUpdateInfo record);

    int updateByPrimaryKey(FEUpdateInfo record);
}
