package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.MedProductBatch;

import java.util.List;

public interface MedProductBatchMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(MedProductBatch record);

    int insertSelective(MedProductBatch record);

    MedProductBatch selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MedProductBatch record);

    int updateByPrimaryKey(MedProductBatch record);

    List<MedProductBatch> selectByProductCode(String productCode);
}