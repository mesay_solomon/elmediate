/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao.mappers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.NodeProduct;

/**
 * @author Mesay S. Taye
 *
 */
public interface NodeProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(NodeProduct record);

    int insertSelective(NodeProduct record);
    
    NodeProduct selectByProductIdandNodeID(@Param("nodeId")Integer nodeId , @Param("programProductId")Integer programProductId);

    NodeProduct selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NodeProduct record);

    int updateByPrimaryKey(NodeProduct record);
    
    int upsert(NodeProduct nodeProduct);
    
    List<NodeProduct> selectByNodeIdAndProgramId(@Param("nodeId")Integer nodeId,@Param("programId")Integer programId);
    
    List<NodeProduct> selectByNodeId(@Param("nodeId")Integer nodeId);
    
    List<NodeProduct> selectByProgramProductId(@Param("ppId")Integer ppbId);
    
    BigDecimal selectBeginningQtyFromTransactionHistory(@Param("nodeId")Integer nodeId  , @Param("productId")Integer productId );
    
    List<NodeProduct> selectByNodeIdAndProgram(@Param("nodeId")Integer nodeId , @Param("programCode")String programCode , @Param("facilityCode")String facilityCode);

	List<NodeProduct> selectAll();

	List<NodeProduct> selectNodeProductsInProgram(@Param("programCode")String programCode,@Param("isExternal") Boolean isExternal, @Param("isVirtual")Boolean isVirtual, @Param("includecps") Boolean includeConsumptionPoints);
	
	int updateFacilityPhysicalCountForProduct(@Param("periodId") Integer periodId , @Param("productCode") String productCode);
	
	int selectTxnCountForNodeProductAfterDate(@Param("nodeId") Integer nodeId,@Param("ppId") Integer productId,@Param("txnDate") Date txnDate);
	
	List<Map<String , Object>> selectFacilityStock(@Param("programCode") String programCode);

	int selectActionCountAfterDate(@Param("nodeId")Integer nodeId, @Param("ppId")Integer ppId, @Param("txnDate")Date txnDate);
	
	int selectTransactionCountAfterDate(@Param("nodeId")Integer nodeId, @Param("ppId")Integer ppId, @Param("txnDate")Date txnDate);
    

}