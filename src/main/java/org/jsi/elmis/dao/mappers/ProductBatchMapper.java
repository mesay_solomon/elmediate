/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.ProductBatch;
import org.jsi.elmis.model.ProductCategory;

import java.util.List;

public interface ProductBatchMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(ProductBatch record);

    int insertSelective(ProductBatch record);

    ProductBatch selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ProductBatch record);

    int updateByPrimaryKey(ProductBatch record);

    List<ProductBatch> selectByProductCode(@Param("productCode") String productCode);

    ProductCategory getCategoryByProductCode(@Param("program") Integer program,
                                             @Param("product") Integer product);

    int upsert(ProductBatch productBatch);

    ProductBatch selectByName(String name);

    int updateByName(ProductBatch record);
}
