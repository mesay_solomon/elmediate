package org.jsi.elmis.dao.mappers;

import org.apache.ibatis.annotations.Param;
import org.jsi.elmis.model.DataChange;

import java.util.List;
import java.util.UUID;

public interface DataChangeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DataChange record);

    int insertSelective(DataChange record);

    DataChange selectByPrimaryKey(Long id);

    List<DataChange> selectAfterVersion(@Param("uuid") Object uuid);

    int updateByPrimaryKeySelective(DataChange record);

    int updateByPrimaryKey(DataChange record);
}
