package org.jsi.elmis.dao.mappers;

import org.jsi.elmis.model.SyncResponseCache;

public interface SyncResponseCacheMapper {
    int deleteByPrimaryKey(String url);

    int insert(SyncResponseCache record);

    int insertSelective(SyncResponseCache record);

    SyncResponseCache selectByPrimaryKey(String url);

    int updateByPrimaryKeySelective(SyncResponseCache record);

    int updateByPrimaryKey(SyncResponseCache record);
}