/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao.mappers.V2;


import org.apache.ibatis.annotations.*;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNode;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface UserAccountMapper {

    @Select("SELECT id, username, firstname, lastname, employeeid, jobtitle, primarynotificationmethod, \n" +
            "       officephone, cellphone, email, supervisorid, facilityid, active, \n" +
            "       vendorid, createdby, createddate, modifiedby, modifieddate, verified, \n" +
            "       deleted, uuid\n" +
            "  FROM users")
   List<User> getAllUsers();

   @Select("SELECT id, username, firstname, lastname, employeeid, jobtitle, primarynotificationmethod, \n" +
           "       officephone, cellphone, email, supervisorid, facilityid, active, \n" +
           "       vendorid, createdby, createddate, modifiedby, modifieddate, verified, \n" +
           "       deleted, uuid\n" +
           "  FROM users where id = #{userId} " )
   @Results(value = {
           @Result(column = "id", property = "id"),
           @Result(column = "id", property = "userNodes", javaType=List.class,
                   many=@Many(select="getUserNodesByUserId")),

   })
   User getUserById(Long userId);

   @Select("select distinct node.*, #{userId} as userId " +
           "from nodes node\n" +
           "left outer join user_node_roles unr on node.id = unr.node_id and unr.user_id = #{userId}\n" +
           "where visible_for_login  = true")
   @Results(value = {
           @Result(column = "id", property = "id"),
           @Result(column = "{nodeId=id, userId=userId}", property = "roles", javaType=List.class,
                   many=@Many(select="getUserRolesByNodeId")),

   })
   List<UserNode> getUserNodesByUserId(Long userId);

    @Select("select distinct roles.* " +
            "from roles roles " +
            "join user_node_roles unr ON unr.role_id = roles.id\n" +
            "where unr.user_id =  #{userId} and unr.node_id = #{nodeId}")
   List<Role> getUserRolesByNodeId(Map map);

   void updateUserRoles(User user);

   @Delete("delete from user_node_roles where user_id = #{userId}")
   void resetUserRole(Integer userId);

   @Insert("INSERT INTO user_node_roles(\n" +
           "            role_id, node_id, user_id)\n" +
           "    VALUES (#{roleId}, #{nodeId}, #{userId})")
   void insertUserRole(@Param("roleId") Integer roleId, @Param("nodeId") Integer nodeId, @Param("userId") Integer userId);

   @Select("SELECT id  FROM users where username = #{userName} limit 1" )
   Integer getUserIdByUserName(String userName);
}

