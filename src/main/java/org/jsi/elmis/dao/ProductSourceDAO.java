/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.AdjustmentMapper;
import org.jsi.elmis.dao.mappers.LossAdjustmentTypeMapper;
import org.jsi.elmis.dao.mappers.NodeProductMapper;
import org.jsi.elmis.dao.mappers.ProductSourceMapper;
import org.jsi.elmis.model.ProductSource;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.NodeProduct;
import org.springframework.stereotype.Component;

@Component
public class ProductSourceDAO extends DAO {
	
	private ProductSourceDAO(){
		super();
	};
	
	private static ProductSourceDAO prodsrcDAO = null;
	
	public static ProductSourceDAO getInstance(){
		if(prodsrcDAO == null){
			prodsrcDAO = new ProductSourceDAO();
		} 
		return prodsrcDAO;
	}

	public int insertProductSource(ProductSource prodSource){
		
		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		int result = 0;
		
		try {
			result = mapper.insert(prodSource);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}
	
	
public int editProductSource(ProductSource prodSource){
		
		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		int result = 0;
		
		try {
			result = mapper.updateByPrimaryKey(prodSource);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
		
	}



public int deleteProductSource(ProductSource prodSource){
	
	SqlSession session = getSqlMapper().openSession();
	ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
	int result = 0;
	
	try {
		result = mapper.deleteByPrimaryKey(prodSource);

	} catch (Exception ex) {
		ex.printStackTrace();
	} finally {
		session.commit();
		session.close();
	}
	return result;
	
}


	
	public List<ProductSource> getAllProductSources(){

		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		List<ProductSource> prodsources = null;

		try {
			prodsources = mapper.selectAllProductsources();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodsources;

	}
	
	public ProductSource getSourceForReciept(Integer txnId){

		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		ProductSource prodSource = null;

		try {
			prodSource = mapper.selectForTxn(txnId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodSource;

	}

	public ProductSource selectByNodeId(Integer nodeId){

		SqlSession session = getSqlMapper().openSession();
		ProductSourceMapper mapper = session.getMapper(ProductSourceMapper.class);
		ProductSource prodSource = null;

		try {
			prodSource = mapper.selectByNodeId(nodeId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodSource;

	}

}
