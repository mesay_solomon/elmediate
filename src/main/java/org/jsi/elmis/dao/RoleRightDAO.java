/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.RoleRightMapper;
import org.jsi.elmis.model.RoleRight;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Michael Mwebaze
 *
 */
@Component
public class RoleRightDAO extends DAO{

	public int saveRoleRight(RoleRight roleRight){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		int result = 0;

		try {
			result = mapper.insert(roleRight);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public List<RoleRight> getRoleRights(){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		List<RoleRight> roleRightList = null;

		try {
			roleRightList = mapper.getRoleRights();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return roleRightList; 
	}
	
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		List<RoleRight> roleRightList = null;

		try {
			roleRightList = mapper.getRoleRightsByRoleId(roleId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return roleRightList; 
	}
	
	public int updateRoleRights(RoleRight roleRight){
		SqlSession session = getSqlMapper().openSession();
		RoleRightMapper mapper = session.getMapper(RoleRightMapper.class);
		int result = 0;

		try {
			result = mapper.update(roleRight);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}