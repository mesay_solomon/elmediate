/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.ActionMapper;
import org.jsi.elmis.model.Action;
import org.jsi.elmis.model.ActionOrder;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ActionDAO extends DAO {

	public int saveAction(Action action){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		
		try {
			 mapper.insert(action);

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (action != null)?action.getId():0;
	}
	
	public Action getAction(Integer id){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		Action action = null;
		
		try {
			 action =  mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.close();
		}
		return action;
	}
	
	public int saveAction(Action action , ActionOrder actionOrder){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		
		if(actionOrder != null){
			if(actionOrder.getPrecedence().equals(ActionOrder.BEFORE )){
				action.setOrdinalNumber(actionOrder.getNeignboringOrder());
				updateActionsIncreaseOrder(actionOrder.getNeignboringOrder());
			} else if (actionOrder.getPrecedence().equals(ActionOrder.AFTER)){
				action.setOrdinalNumber(actionOrder.getNeignboringOrder() + 1);
				updateActionsIncreaseOrder(actionOrder.getNeignboringOrder() + 1);
			}
		} else {
			action.setOrdinalNumber(getNextOrdinalNumber());
		}
		
		try {
			 mapper.insert(action);
		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return (action != null)?action.getId():0;
	}
	
	public Integer getNextOrdinalNumber(){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		
		try {
			return mapper.selectNextOrdinalNumber();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	public List<Integer> getActionsAfterAction(Integer nodeId, Integer ppId, Integer order){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		
		try {
			return mapper.selectActionOrderAfterAction(nodeId, ppId, order);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}
	
	public int updateActionsIncreaseOrder(Integer order){
		SqlSession session = getSqlMapper().openSession();
		ActionMapper mapper = session.getMapper(ActionMapper.class);
		Integer maxCurrentOrder = getNextOrdinalNumber() - 1;
		
		try {
			for(int i = maxCurrentOrder ; i >= order ; i--){
				mapper.updateActionIncreaseOrder(i);
			}
		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			session.commit();
			session.close();
		}
		return 0;
	}
}
