/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FacilityMapper;
import org.jsi.elmis.dao.mappers.FacilityTypeMapper;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.FacilityType;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class FacilityDAO extends DAO {

	/**
	 * @param facilityTypes
	 */
	public void upsertFacilityTypes(ArrayList<FacilityType> facilityTypes) {
		SqlSession session = getSqlMapper().openSession();
		FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);
		try {
			for (FacilityType facilityType : facilityTypes) {
				System.out.println(facilityType.getCode());
				mapper.upsertByCode(facilityType);
			}
			session.commit();
		} finally {
			//session.close();
		}
	}

	/**
	 * @param facilities
	 */

	public void upsertFacilities(ArrayList<Facility> facilities) {
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		try {
			for (Facility facility : facilities) {
				System.out.println(facility.getCode());
				mapper.upsertByCode(facility);
			}

			session.commit();
		} finally {
			//session.close();
		}

	}

	public Facility getFacilityByCode(String facilityCode) {
		SqlSession session = getSqlMapper().openSession();
		FacilityMapper mapper = session.getMapper(FacilityMapper.class);
		Facility facility = null;

		try {
			facility = mapper.selectByCode(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facility;
	}

    public FacilityType getFacilityTyepByCode(String fTypeCode) {
        SqlSession session = getSqlMapper().openSession();
        FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);

        FacilityType facilityType = null;

        try {
            facilityType = mapper.selectByCode(fTypeCode);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return facilityType;
    }

	public  FacilityType loadFacilityTypebyId(Integer id){
		SqlSession session = getSqlMapper().openSession();
		FacilityTypeMapper mapper = session.getMapper(FacilityTypeMapper.class);
		FacilityType facilityType = null;

		try {
			facilityType = mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilityType;
	}

    public Integer insertFacilitySelective(Facility facility) {
        SqlSession session = getSqlMapper().openSession();
        FacilityMapper mapper = session.getMapper(FacilityMapper.class);
        Integer result = null;
        try {
            result = mapper.insertSelective(facility);
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            session.commit();
            session.close();
        }
        return result;
    }
    public Integer updateFacilityByCode(Facility facility) {
        SqlSession session = getSqlMapper().openSession();
        FacilityMapper mapper = session.getMapper(FacilityMapper.class);
        Integer result = null;
        try {
            result = mapper.updateByCode(facility);
        } finally {
            session.commit();
            session.close();
        }
        return result;
    }

}
