/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.RoleMapper;
import org.jsi.elmis.model.Role;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Michael Mwebaze
 *
 */
@Component
public class RoleDAO extends DAO{

	public int saveRole(Role role){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.insert(role);
			result = role.getId();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public List<Role> getRoles(){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		List<Role> rolesList = null;

		try {
			rolesList = mapper.getRoles();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rolesList;
	}
	
	public int updateRole(Role role){
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.updateByPrimaryKey(role);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
	public Integer deleteRole(int roleId) {
		SqlSession session = getSqlMapper().openSession();
		RoleMapper mapper = session.getMapper(RoleMapper.class);
		int result = 0;

		try {
			result = mapper.deleteByPrimaryKey(roleId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
