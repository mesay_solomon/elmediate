/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.FacilityApprovedProductMapper;
import org.jsi.elmis.dao.mappers.ProgramProductMapper;
import org.jsi.elmis.model.FacilityApprovedProduct;
import org.jsi.elmis.model.ProgramProduct;
import org.springframework.stereotype.Component;


/**
 * @author Mesay S. Taye
 *
 */
@Component
public class ProgramProductDAO extends DAO {

	public ProgramProduct getProgramProduct(Integer id){
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		ProgramProduct pp = null;

		try {
			pp =  mapper.selectByPrimaryKey(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return pp;
	}

	public ProgramProduct getProgramProduct(Integer productId, String programCode){
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
		ProgramProduct pp = null;

		try {
			pp =  mapper.selectByProgramAndProduct(programCode, productId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return pp;
	}

    public ProgramProduct getProgramProduct(Integer productId, Integer programId){
        SqlSession session = getSqlMapper().openSession();
        ProgramProductMapper mapper = session.getMapper(ProgramProductMapper.class);
        ProgramProduct pp = null;

        try {
            pp =  mapper.selectByProgramIdAndProductId(programId, productId);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return pp;
    }

	public void deleteProgramProducts() {
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session
				.getMapper(ProgramProductMapper.class);
		try {

				mapper.deleteProgramProducts();

			session.commit();
		} finally {
			//session.close();
		}

	}

	public void insertFacilityApprovedProduct(
			ArrayList<FacilityApprovedProduct> faciltyApprovedProducts) {
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session
				.getMapper(FacilityApprovedProductMapper.class);

		try {
			for (FacilityApprovedProduct facilityApprovedProduct : faciltyApprovedProducts) {
				System.out.println(facilityApprovedProduct.getId());
				mapper.insert(facilityApprovedProduct);
			}
			session.commit();
		} finally {
			session.close();
		}

	}

	public void deleteFacilityApprovedProduct() {
		SqlSession session = getSqlMapper().openSession();
		FacilityApprovedProductMapper mapper = session
				.getMapper(FacilityApprovedProductMapper.class);

		try {

				mapper.deleteApprovedProducts();
			session.commit();
		} finally {
			//session.close();
		}

	}


	public void insertProgramProducts(ArrayList<ProgramProduct> programProducts) {
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session
				.getMapper(ProgramProductMapper.class);
		try {
			for (int i = 0; i < programProducts.size(); i++) {
				// System.out.println(programProducts.get(i).getId());
				// mapper.updateByPrimaryKey(products.get(i));
				mapper.insert(programProducts.get(i));

			}
			session.commit();
		} finally {
			//session.close();
		}

	}

	public void upsertProgramProducts(ArrayList<ProgramProduct> programProducts) {
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session
				.getMapper(ProgramProductMapper.class);
		try {
			for (int i = 0; i < programProducts.size(); i++) {
				// System.out.println(programProducts.get(i).getId());
				// mapper.updateByPrimaryKey(products.get(i));
				ProgramProduct pp = programProducts.get(i);
				mapper.upsertByProgramCodeAndProductCode(pp);

			}
			session.commit();
		} finally {
			//session.close();
		}

	}

	public Integer updateProgramProduct(ProgramProduct programProduct) {
		Integer result = -1;
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session
				.getMapper(ProgramProductMapper.class);
		try{
			result = mapper.updateByPrimaryKeySelective(programProduct);
		}catch(Exception ex){
			ex.printStackTrace();
		}finally {
			session.commit();
			session.close();
		}
		return result;
	}

	public Integer updateProgramProducts(List<ProgramProduct> programProducts) {
		Integer result = -1;
		SqlSession session = getSqlMapper().openSession();
		ProgramProductMapper mapper = session
				.getMapper(ProgramProductMapper.class);
		try{
			for(ProgramProduct pp: programProducts){
				result = mapper.updateByPrimaryKeySelective(pp);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally {
			session.commit();
			session.close();
		}
		return result;
	}


    public FacilityApprovedProduct getFacilityApprovedProduct(Integer programProductId, Integer facilityTypeId) {
        SqlSession session = getSqlMapper().openSession();
        FacilityApprovedProductMapper mapper = session.getMapper(FacilityApprovedProductMapper.class);
        FacilityApprovedProduct fap = null;

        try {
            fap =  mapper.selectFacilityApprovedProgramProductByPPIDandFTID(programProductId, facilityTypeId);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return fap;
    }
}
