/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.jsi.elmis.common.util.Crypto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ELMISJDBC {

	@Value("${dbdriver}")
	private String DRIVER;
	@Value("${dbuser}")
	private String USER_NAME;
	@Value("${dbpassword}")
	private String PASSOWRD;
	@Value("${dburl}")
	private String URL;

	public void refreshMaterializedView(String matView) throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;	
		String sql = "refresh materialized view "+ matView + ";";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	public Boolean reorderActions() throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;	
		String sql = "SELECT reorder_actions();";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		return true;
	}
	
	public Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(this.DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			dbConnection = DriverManager.getConnection(this.URL, this.USER_NAME, Crypto.decrypt(this.PASSOWRD)); 
			return dbConnection;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dbConnection;
	}
}