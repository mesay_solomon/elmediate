/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.jsi.elmis.dao.mappers.HubRnRMapper;
import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.hub.rnr.HubRnR;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
@Component
public class HubRnRDAO extends DAO{
	
	public Integer insertRnR(HubRnR hubRnR) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = 0;
		try {
			result = mapper.insertRnR(hubRnR);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
		return result;
	}
	
	public List<Facility> getFacilities(){
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		List<Facility> facilities = null;

		try {
			facilities = mapper.selectFacilities();

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return facilities;
	}

	public void updateRnR(HubRnR hubRnR) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		try {
			mapper.updateRnR(hubRnR);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public Integer updateUser(User user) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = null;

		try {
			result = mapper.updateUser(user);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
			return result;
		}
	}
	
	public Integer insertUser(User user) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = null;

		try {
			result = mapper.insertUser(user);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
			return result;
		}
	}

	public ArrayList<ProcessingPeriod> fetchPeriodsToSubmitRnRFor() {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectPeriodsToSubmitRnRFor();

			for (ProcessingPeriod processingPeriod : periods) {
				System.out.println(processingPeriod.getStartdate()+ ":" + processingPeriod.getEnddate());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}
	
	public ProcessingPeriod getProcessingPeriod(Integer id) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ProcessingPeriod period = null;
		try {

			period = mapper.selectPerocessingPeriod(id);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}
	

	public List<HubRnR> selectMontlyRnRsForPeriod(Date startDate, Date endDate) {

		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		List<HubRnR> rnrs = null;
		try {

			rnrs = mapper.selectMontlyRnRsForPeriod(startDate, endDate);

			for (HubRnR hubRnR : rnrs) {
				System.out.println(hubRnR.getFacilityCode());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rnrs;
	}
	
	public Integer findScheduleIdByPeriodId(Integer periodId) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		Integer scheduleId = null;
		try {

			scheduleId = mapper.findScheduleIdByPeriodId(periodId);

			if(scheduleId !=  null)
			System.out.println(scheduleId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return scheduleId;
	}
	
	public Boolean insertWeeklyPeriod(ProcessingPeriod weeklyPeriod) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		Boolean status = false;
		try {
			mapper.insertWeeklyProcessingPeriod(weeklyPeriod);
			status = weeklyPeriod.getId() != 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			status = false;
		} finally {
			session.commit();
			session.close();
		}
		return status;
	}
	
	public ArrayList<ProcessingPeriod> fetchWeeklyPeriods() {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectWeeklyPeriods();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}
	public List<HubRnR> selectAllRnRs(String programCode , String facilityCode, String type) {

		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		List<HubRnR> rnrs = null;
		try {

			rnrs = mapper.selectAllRnRs(programCode , facilityCode , type);

			for (HubRnR hubRnR : rnrs) {
				System.out.println(hubRnR.getFacilityCode());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return rnrs;
	}

	public ProcessingPeriod getProcessingPeriodByRange(Date startDate,
			Date endDate) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ProcessingPeriod period = null;
		try {

			period = mapper.selectPerocessingPeriodByRange(startDate , endDate);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return period;
	}

	public List<ProcessingPeriod> fetchAllPeriods() {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		ArrayList<ProcessingPeriod> periods = null;
		try {

			periods = mapper.selectAllPeriods();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return periods;
	}

	public List<HubRnR> selectMontlyRnRsForPeriod(Integer periodId) {
			SqlSession session = getSqlMapper().openSession();
			HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

			List<HubRnR> rnrs = null;
			try {

				rnrs = mapper.selectMontlyRnRsForPeriodByPeriodId(periodId);

				for (HubRnR hubRnR : rnrs) {
					org.jsi.elmis.model.hub.rnr.Report report = new Gson().fromJson(hubRnR.getRnrJSON(), org.jsi.elmis.model.hub.rnr.Report.class);
					hubRnR.setReport(report);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				session.close();
			}
			return rnrs;
		}
	public int updateRnRStatus(List<HubRnR> hubRnRList, String status) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);

		try {
			for(HubRnR rnr : hubRnRList){
				rnr.setStatus(status);
				mapper.updateRnRStatus(rnr);	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return -1;
		} finally {
			session.commit();
			session.close();
		}
		return 1;
	}

	public List<HubRnR> fetchRnRByStatus(String status) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		List<HubRnR> result = null;
		try {
			result = mapper.selectRnRByStatus(status);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return result;
	}

	public Integer deleteById(Integer id) {
		SqlSession session = getSqlMapper().openSession();
		HubRnRMapper mapper = session.getMapper(HubRnRMapper.class);
		Integer result = null;
		try {
			result = mapper.deleteById(id);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		return result;
	}
}
