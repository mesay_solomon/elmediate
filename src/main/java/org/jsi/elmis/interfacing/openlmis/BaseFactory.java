/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.interfacing.openlmis;

import com.google.gson.*;
import org.apache.http.HttpStatus;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.SyncResponseCache;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class BaseFactory {
    @Value("${elmis.rnr.send.to.hub}")
    private Boolean sendRnRToHub;
    @Autowired
    PropertiesService propertyService;

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    JerseyHttpClient client;

    public static Long FacilityID = 1L;

    private void parseHostAndPort() {
        // this assumes that the base URL contains the protocol and all
        HttpClient.PROTOCOL = propertyService.getProperty("elmis.central.url", false).getValue().substring(0, propertyService.getProperty("elmis.central.url", false).getValue().indexOf(':'));

        // set the port
        if (propertyService.getProperty("elmis.central.url", false).getValue().lastIndexOf(':') != propertyService.getProperty("elmis.central.url", false).getValue().indexOf(':')) {
            String port = propertyService.getProperty("elmis.central.url", false).getValue().substring(propertyService.getProperty("elmis.central.url", false).getValue().lastIndexOf(':') + 1);
            HttpClient.PORT = Integer.parseInt(port);
        } else {
            // default to http port 80
            HttpClient.PORT = 80;
        }

        String host = propertyService.getProperty("elmis.central.url", false).getValue().substring(propertyService.getProperty("elmis.central.url", false).getValue().lastIndexOf("//") + 2);
        if (host.indexOf(':') > 0) {
            host = host.substring(0, host.indexOf(':'));
        }

        HttpClient.HOST = host;

    }
    
    
    private void parseUATHostAndPort(String uatURL) {
        // this assumes that the base URL contains the protocol and all
        HttpClient.PROTOCOL = uatURL.substring(0, uatURL.indexOf(':'));

        // set the port
        if (uatURL.lastIndexOf(':') != uatURL.indexOf(':')) {
            String port = uatURL.substring(uatURL.lastIndexOf(':') + 1);
            HttpClient.PORT = Integer.parseInt(port);
        } else {
            // default to http port 80
            HttpClient.PORT = 80;
        }

        String host = uatURL.substring(uatURL.lastIndexOf("//") + 2);
        if (host.indexOf(':') > 0) {
            host = host.substring(0, host.indexOf(':'));
        }

        HttpClient.HOST = host;

    }

    public Object loadJSONLookup(String service, Class<?> type, Boolean cache, Boolean useAuthToken) throws Exception {

        parseHostAndPort();
//        HttpClient client = new HttpClient();
//        client.createContext();
        JSONObject array = null;
        JSONArray innerArray = null;

        ResponseEntity response;
        if (sendRnRToHub && type == ProcessingPeriod.class) {
            response = client.SendJSON("{}", propertyService.getProperty(
                    "elmis.hub.url", false).getValue() + "/rest-api/lookup/" + service, HttpClient.GET,
                    propertyService.getProperty("elmis.central.approver.name", false).getValue(),
                    propertyService.getProperty("elmis.central.password", false).getValue());
            innerArray = new JSONArray(response.getResponse());
        } else {
            if (useAuthToken) {
                response = client.SendJSON(
                        "{}", propertyService.getProperty("elmis.central.url", false).getValue() + "/rest-api/lookup/" + service,
                        HttpClient.GET, propertyService.getProperty("elmis.central.approver.name", false).getValue(),
                        propertyService.getProperty("elmis.central.password", false).getValue());
            } else {
                response = client.sendJSONNoToken(
                        "{}", propertyService.getProperty("elmis.central.url", false).getValue() + "/rest-api/lookup/" + service,
                        HttpClient.GET, propertyService.getProperty("elmis.central.approver.name", false).getValue(),
                        propertyService.getProperty("elmis.central.password", false).getValue());
            }
            array = new JSONObject(response.getResponse());
            innerArray = array.getJSONArray(service);

            if(cache) commonDAO.saveResponse(new SyncResponseCache(service, response.getResponse(), DateCustomUtil.getCurrentTime()));
        }
        try {


            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                }
            }).create();

            List<Object> list = new ArrayList<>();
            for (int i = 0; i < innerArray.length(); i++) {

                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(), type));
            }
            return list;
        } catch (Exception ex) {

            if (ex instanceof NullPointerException) {
                throw new Exception("Network Error");
            }
            throw new Exception("Authentication Error");
        }
    }

    public Object loadJSONLookup(String service, Class<?> type, Map<String, String> parameters, Boolean cache, Boolean useAuthToken) throws Exception {

        parseHostAndPort();
//        HttpClient client = new HttpClient();
//        client.createContext();
        ResponseEntity response;
        if(useAuthToken){
            response = client.SendJSON(
                    "{}",
                    propertyService.getProperty("elmis.central.url", false).getValue() + "/rest-api/lookup/" + service + (buildParametersString(parameters)),
                    HttpClient.GET, propertyService.getProperty("elmis.central.approver.name", false).getValue(),
                    propertyService.getProperty("elmis.central.password", false).getValue());
        }else {
            response = client.sendJSONNoToken(
                    "{}",
                    propertyService.getProperty("elmis.central.url", false).getValue() + "/rest-api/lookup/" + service + (buildParametersString(parameters)),
                    HttpClient.GET, propertyService.getProperty("elmis.central.approver.name", false).getValue(),
                    propertyService.getProperty("elmis.central.password", false).getValue());
        }


        if(cache) commonDAO.saveResponse(new SyncResponseCache(service, response.getResponse(), DateCustomUtil.getCurrentTime()));
        System.out.println(response.getResponse());
        try {
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                }
            }).create();

            List<Object> list = new ArrayList<>();
            for (int i = 0; i < innerArray.length(); i++) {

                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(), type));
            }
            return list;
        } catch (Exception ex) {

            if (ex instanceof NullPointerException) {
                throw new Exception("Network Error");
            }
            throw new Exception("Authentication Error");
        }
    }

    public Object loadJSONLookup(Class<?> type, String BaseURL, Boolean cache, Boolean useAuthToken) throws Exception {

        parseHostAndPort();
//        HttpClient client = new HttpClient();
//        client.createContext();


        ResponseEntity response;

        if(useAuthToken) {
            response    =client.SendJSON("{}",
                            BaseURL + "/rest-api/med-product-batches", HttpClient.GET, "admin", "admin123");
        } else {
            response    =client.sendJSONNoToken("{}",
                    BaseURL + "/rest-api/med-product-batches", HttpClient.GET, "admin", "admin123");
                }
        if (cache) commonDAO.saveResponse(new SyncResponseCache("med-product-batches", response.getResponse(), DateCustomUtil.getCurrentTime()));
        System.out.println(response.getResponse());
        try {
            JSONArray array = new JSONArray(response.getResponse());
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                    return new Date(json.getAsJsonPrimitive().getAsLong());
                }
            }).create();

            List<Object> list = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {

                list.add(gson.fromJson(array.getJSONObject(i).toString(), type));
            }
            return list;
        } catch (Exception ex) {

            if (ex instanceof NullPointerException) {
                throw new Exception("Network Error");
            }
            throw new Exception("Authentication Error");
        }
    }

    @SuppressWarnings("rawtypes")
    public static String buildParametersString(Map<String, String> parameters) {
        String parametersString = "";

        Iterator it = parameters.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
//                System.out.println(pair.getKey() + " = " + pair.getValue());
            parametersString = parametersString.concat("?".concat(pair.getKey().toString().concat("=").concat(pair.getValue().toString())));
            it.remove(); // avoids a ConcurrentModificationException
        }
        return parametersString;
    }


    public Object uploadJSON(String url, String service, String result, String json, Class<?> type, String programCode, Boolean useAuthToken) throws JSONException, Exception {

        parseHostAndPort();
//        HttpClient client = new HttpClient();
//        client.createContext();

        ResponseEntity response = null;


        if (useAuthToken) {
            response = client.SendJSON(json, url + "/rest-api/" + service, HttpClient.POST, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());
        } else {
            response = client.sendJSONNoToken(json, url + "/rest-api/" + service, HttpClient.POST, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());
        }

        JSONObject array = new JSONObject(response.getResponse());


        String errorObject = null;
        try {
            errorObject = array.getString("error");
        } catch (Exception ex) {

        }
        if (errorObject != null) {
            throw new Exception(errorObject);
        }
        try {
            /*JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);*/

        } catch (Exception ex) {

            if (ex instanceof NullPointerException) {
                throw new Exception("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }
        return true;

    }

    public JSONObject uploadDailyStockStatusJSON(String url, String service, String result, String json, Class<?> type, String programCode, Boolean useAuthToken) throws JSONException {

      parseUATHostAndPort(url);
//      HttpClient client = new HttpClient();
//      client.createContext();

      ResponseEntity response = null;

      if (useAuthToken){
          response = client.SendJSON(json, url + "/rest-api/" + service, HttpClient.POST, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());
      } else {
          response = client.sendJSONNoToken(json, url + "/rest-api/" + service, HttpClient.POST, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());
      }


      return new JSONObject(response.getResponse());
  }


    public Boolean isUserAuthenticated(String service, String json) throws JSONException {

        String hostURL = propertyService.getProperty("elmis.central.url", false).getValue();

        parseUATHostAndPort(hostURL);
//        HttpClient client = new HttpClient();
//        client.createContext();

        ResponseEntity response = null;


        response = client.SendJSON(json, hostURL + "/rest-api/" + service, HttpClient.POST, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());

        return response.getStatus() == HttpStatus.SC_OK;
    }

/////
public Object uploadFile(String url, String service, String result, String json, Class<?> type, String programCode, InputStream inputStream, String fileName) throws JSONException, Exception {

    parseHostAndPort();
//    HttpClient client = new HttpClient();
//    client.createContext();

    ResponseEntity response = null;


    response = client.uploadFileAndJSON( url + "/rest-api/" + service,json, inputStream,fileName, propertyService.getProperty("elmis.central.approver.name", false).getValue(), propertyService.getProperty("elmis.central.password", false).getValue());

    JSONObject array = new JSONObject(response.getResponse());


    String errorObject = null;
    try {
        errorObject = array.getString("error");
    } catch (Exception ex) {

    }
    if (errorObject != null) {
        throw new Exception(errorObject);
    }
    try {
            /*JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);*/

    } catch (Exception ex) {

        if (ex instanceof NullPointerException) {
            throw new Exception("Network Error");
        }
        // JSON was not being parsed,
        // this indicates that the authentication has failed.
        throw new Exception("Authentication Error");
    }
    return true;

}


}

class TimestampDeserializer implements JsonDeserializer<Timestamp> {
    @Override
    public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        long time = Long.parseLong(json.getAsString());
        return new Timestamp(time);
    }
}

class DateDeserializer implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        String date = element.getAsString();

        SimpleDateFormat formatter = new SimpleDateFormat();
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            System.err.println("Failed to parse Date due to:");
            return null;
        }
    }
}
