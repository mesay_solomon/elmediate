package org.jsi.elmis.interfacing.openlmis;


import us.monoid.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

public interface IHttpClient {
    ResponseEntity uploadFileAndJSON(String url, String json, InputStream inputStream, String fileName, String username, String password)  throws JSONException, Exception;
    ResponseEntity SendJSON(String json, String url, String commMethod, String username, String password);
    ResponseEntity sendJSONNoToken(String json, String url, String commMethod, String username, String password);
    ResponseEntity handleRequest(String commMethod, String json, String url) throws IOException;
    void createContext();
}
