package org.jsi.elmis.interfacing.openlmis;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.jsi.elmis.rest.client.ClientCommon;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.monoid.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

@Component
public class JerseyHttpClient extends ClientCommon implements IHttpClient{

    @Autowired
    PropertiesService propertiesService;

    private final Logger log = Logger.getLogger(getClass());

    @Override
    public ResponseEntity uploadFileAndJSON(String url, String json, InputStream inputStream, String fileName, String username, String password) throws JSONException, Exception {
        ResponseEntity responseEntity;
        String responseBody;
        try{
            log.debug("calling API on : " + url);
            webResource = getAuthenticatedClient(username, password).resource(url);
            response = webResource.accept("application/json").header("X-Auth-Token", getAuthToken()).type("application/json").post(ClientResponse.class, json);
        }catch(Exception ex){
            return null;
        }


        responseBody = response.getEntity(String.class);
        log.debug("response " + responseBody);

        responseEntity = new ResponseEntity();

        responseEntity.setResponse(responseBody);
        responseEntity.setStatus(response.getStatus());

        return responseEntity;
    }

    @Override
    public ResponseEntity SendJSON(String json, String url, String commMethod, String username, String password) {
        ResponseEntity responseEntity;
        String responseBody;
        try{
            log.debug("calling API on : " + url);
            webResource = getAuthenticatedClient(username, password).resource(url);
            if(commMethod.equalsIgnoreCase("get")){
                response = webResource.accept("application/json").header("X-Auth-Token", getAuthToken()).type("application/json").method(commMethod, ClientResponse.class);
            }else{
                response = webResource.accept("application/json").header("X-Auth-Token", getAuthToken()).type("application/json").method(commMethod, ClientResponse.class, json);
            }

        }catch(Exception ex){
            return null;
        }



        responseBody = response.getEntity(String.class);
        log.debug("response " + responseBody);

        responseEntity = new ResponseEntity();

        responseEntity.setResponse(responseBody);
        responseEntity.setStatus(response.getStatus());

        return responseEntity;
    }

    @Override
    public ResponseEntity sendJSONNoToken(String json, String url, String commMethod, String username, String password) {
        ResponseEntity responseEntity;
        String responseBody;
        try{
            log.debug("calling API on : " + url);
            webResource = getAuthenticatedClient(username, password).resource(url);
            if(commMethod.equalsIgnoreCase("get")){
                response = webResource.accept("application/json").type("application/json").method(commMethod, ClientResponse.class);
            }else{
                response = webResource.accept("application/json").type("application/json").method(commMethod, ClientResponse.class, json);
            }

        }catch(Exception ex){
            return null;
        }

        responseBody = response.getEntity(String.class);
        log.debug("response " + responseBody);

        responseEntity = new ResponseEntity();

        responseEntity.setResponse(responseBody);
        responseEntity.setStatus(response.getStatus());

        return responseEntity;
    }

    @Override
    public ResponseEntity handleRequest(String commMethod, String json, String url) throws IOException {
        String username = propertiesService.getProperty("elmis.central.approver.name", false).getValue();
        String password = propertiesService.getProperty("elmis.central.password", false).getValue();
        return this.SendJSON(json, url, commMethod, username, password);
    }

    @Override
    public void createContext() {

    }

    private Client getAuthenticatedClient(String username, String password){
        HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(username, password);
        Client authenticatedClient = getWSClient();
        authenticatedClient.addFilter(authFilter);
        return authenticatedClient;
    }
}
