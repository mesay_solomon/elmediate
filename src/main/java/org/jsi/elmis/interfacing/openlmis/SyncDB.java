  /*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.interfacing.openlmis;

import java.util.*;

//import com.sun.org.apache.xpath.internal.operations.Bool;
import com.google.gson.*;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.Connectivity;
import org.jsi.elmis.dao.*;
import org.jsi.elmis.model.*;
import org.jsi.elmis.rest.client.WSClientInterfacing;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.groupingBy;

  @Component
public class SyncDB  implements Job{

	@Autowired
	ProgramDAO programDAO;
	@Autowired
	ProductDAO productDAO;
	@Autowired
    AdjustmentDAO adjustmentDAO;
	@Autowired
    ProgramProductDAO programProductDAO;
	@Autowired
	GeographicLevelDAO geoLevelDAO;
	@Autowired
	GeographicZoneDAO geoZoneDAO;
	@Autowired
	FacilityDAO facilityDAO;
	@Autowired
	ProcessingPeriodDAO periodDAO;
	@Autowired
	ProgramProductDAO progProdDAO;
	@Autowired
	RegimenDAO regimenDAO;
    @Autowired
    UserDAO userDAO;
    @Autowired
    CommonDAO commonDAO;
    @Autowired PropertiesService propertyService;

    @Autowired
    AuditDAO auditDAO;

    @Autowired
    RnRService rnrService;

	@Value("${sync.user.data}")
	Boolean syncUserData;

	@Autowired
    WSClientInterfacing wsClientInterfacing;

	private static final String UPDATE = "UPDATE";
	private static final String INSERT = "INSERT";

	private ArrayList<GeographicZone> geographicZones = null;
	private ArrayList<GeographicLevel> geographicLevels = null;
	private ArrayList<FacilityType> facilityTypes = null;
	private ArrayList<Facility> facilities = null;
	private ArrayList<ProcessingSchedule> schedules = null;
	private ArrayList<Product> products = null;
    private ArrayList<ProductBatch> productBatches = null;
	private ArrayList<ProgramProduct> programProducts = null;

	private ArrayList<FacilityApprovedProduct> faciltyApprovedProducts = null;
	private ArrayList<ProductCategory> categories = null;
	private ArrayList<Program> programs = null;
	private ArrayList<ProcessingPeriod> processingPeriods = null;

	private ArrayList<RegimenLine> regimenLines = null;
	private ArrayList<Regimen> regimens = null;
	private ArrayList<RegimenProductCombination> regimenProductCombinations = null;
	private ArrayList<RegimenCombinationProduct> regimenCombinationProducts = null;
	private ArrayList<RegimenProductDosage> regimenProductDosages = null;
	private ArrayList<DosageUnit> dosageUnits = null;
	private ArrayList<DosageFrequency> dosageFrequencies = null;
	private ArrayList<User> users = null;
	private ArrayList<Requisition> requisitions = null;
    private ArrayList<Requisition> emergencyRequisitions = null;

	@SuppressWarnings("unused")
	private ArrayList<LossAdjustmentType> lossesAndAdjustmentTypes = null;

	@Autowired
	LookupFactory lookupFactory;

    private Gson gson = new GsonBuilder()
              .setDateFormat("MMM dd, yyyy hh:mm:ss a") //Jan 1, 2017 2:00:00 AM
              .create();

	public SyncRequestResult syncFull() {
		// new ExampleApplication().insertProductCategory();

		// new ExampleApplication().testInsertAllProducts();


		if (Connectivity.isReachable(propertyService.getProperty("elmis.central.url", false).getValue())) {

			this.syncGeoInfo();
			this.syncFacilityInfo();
			this.fetchSyncData();

			this.upsertPrograms();
			this.upsertProductCategory();
			this.upsertProducts();
			this.upsertProductBatches();
			this.upsertFacilityTypes();
			this.deleteFacilityApprovedProducts();
			this.upsertProgramProducts();

			this.insertFacilityApprovedProducts();

			this.upsertProcessingSchedule();
			this.upsertPeriodsByDateRange();

			/*
			 * sync.deleteLossesAndAdjustmentTypes();
			 * sync.insertLossesAndAdjustmentTypes();
			 */

			this.upsertDosageUnits();
			this.upsertDosageFrequencies();
			this.upsertRegimenLines();
			this.upsertRegimens();
			this.upsertRegimenProductCombinations();
			this.upsertRegimenCombinationProducts();
			this.upsertRegimenProductDosages();

			if (syncUserData) this.upsertUsers();

			this.upsertRequisitions();

			System.out.println("Sync completed!");
			return new SyncRequestResult(true, "Synchronization completed!");
		} else {
			System.out.println("Sync not completed!");
			return new SyncRequestResult(false,
					"Unable to reach eLMIS server! Please check your connection and try again!");
		}
	}
      public SyncRequestResult syncFullTV() {
          // new ExampleApplication().insertProductCategory();

          // new ExampleApplication().testInsertAllProducts();


          if (Connectivity.isReachable(propertyService.getProperty("elmis.central.url", false).getValue())) {

              this.fetchSyncDataTest();
              extractDiffs();

              syncDelta(null);

//            this.syncGeoInfo();


              System.out.println("Sync completed!");
              return new SyncRequestResult(true, "Synchronization completed!");
          } else {
              System.out.println("Sync not completed!");
              return new SyncRequestResult(false,
                      "Unable to reach eLMIS server! Please check your connection and try again!");
          }
      }

	private void syncGeoInfo() {
		geographicLevels = lookupFactory.getGeographicLevels();
		geographicZones = lookupFactory.getGeographicZones();

		this.upsertGeographicLevels();
		this.upsertGeographicZones();
	}

	public SyncRequestResult syncFacilityInfo(){

		if(Connectivity.isReachable(propertyService.getProperty("elmis.central.url", false).getValue())){

			this.fetchFacilityInfo();

            this.upsertFacilityTypes();

			this.upsertFacilities();

			return new SyncRequestResult(true, "Synchronization completed!");

		} else {

			System.out.println("Sync not completed!");
			return new SyncRequestResult(false,
					"Unable to reach eLMIS server! Please check your connection and try again!");
		}

	}
      public Boolean fetchSyncDataTest() {
          Boolean fetchIsSuccessful = false;

          schedules = lookupFactory.getProcessingSchedules();

          geographicLevels = lookupFactory.getGeographicLevels();
          geographicZones = lookupFactory.getGeographicZones();

          facilityTypes = lookupFactory.getFacilityTypes();
          facilities = lookupFactory.getFacilities();
          productBatches = lookupFactory.getProductBatches();
          programProducts = lookupFactory.getProgramProducts();
          faciltyApprovedProducts = lookupFactory.getFacilityApprovedProducts();
          categories = lookupFactory.getProductCategories();
          programs = lookupFactory.getPrograms();
          products = lookupFactory.getProducts();
          processingPeriods = lookupFactory.getProcessingPeriods();

          lossesAndAdjustmentTypes = lookupFactory.getLossesAndAdjustmentTypes();

          dosageUnits = lookupFactory.getDosageUnits();
          dosageFrequencies = lookupFactory.getDosageFrequencies();
          regimenLines = lookupFactory.getRegimenLines();
          regimens = lookupFactory.getRegimens();
          regimenProductCombinations = lookupFactory.getRegimenProductCombinations();
          regimenCombinationProducts = lookupFactory.getRegimenCombinationProducts();
          regimenProductDosages = lookupFactory.getRegimenProductDosages();
          Facility f = commonDAO.getFacilityByCode(propertyService.getFacilityCode());
          requisitions = rnrService.getMissingRequisitions(f, null, null, false);
          emergencyRequisitions = rnrService.getMissingRequisitions(f, null, null, true);
          if(syncUserData)
              users = lookupFactory.getUsers();

          fetchIsSuccessful = true;

          return fetchIsSuccessful;
      }
	public Boolean fetchSyncData() {
		Boolean fetchIsSuccessful = false;

		schedules = lookupFactory.getProcessingSchedules();
		products = lookupFactory.getProducts();
		productBatches = lookupFactory.getProductBatches();
		programProducts = lookupFactory.getProgramProducts();
		faciltyApprovedProducts = lookupFactory.getFacilityApprovedProducts();
		categories = lookupFactory.getProductCategories();
		programs = lookupFactory.getPrograms();
	    processingPeriods = lookupFactory.getProcessingPeriods();

		lossesAndAdjustmentTypes = lookupFactory.getLossesAndAdjustmentTypes();

		dosageUnits = lookupFactory.getDosageUnits();
		dosageFrequencies = lookupFactory.getDosageFrequencies();
		regimenLines = lookupFactory.getRegimenLines();
		regimens = lookupFactory.getRegimens();
		regimenProductCombinations = lookupFactory.getRegimenProductCombinations();
		regimenCombinationProducts = lookupFactory.getRegimenCombinationProducts();
		regimenProductDosages = lookupFactory.getRegimenProductDosages();
		Facility f = commonDAO.getFacilityByCode(propertyService.getFacilityCode());
		requisitions = rnrService.getMissingRequisitions(f, null, null, false);
        emergencyRequisitions = rnrService.getMissingRequisitions(f, null, null, true);
		if(syncUserData)
			users = lookupFactory.getUsers();

		fetchIsSuccessful = true;

		return fetchIsSuccessful;
	}


    public boolean fetchFacilityInfo(){
		facilityTypes = lookupFactory.getFacilityTypes();
		facilities = lookupFactory.getFacilities();
		return true;
	}

	private void upsertPeriodsByDateRange() {
		periodDAO.upsertPeriodByDateRange(processingPeriods);
	}

	public void insertProgramProducts() {

		progProdDAO.insertProgramProducts(programProducts);
	}

	public void upsertGeographicZones() {

		geoZoneDAO.upsertGeographicZones(geographicZones);
	}

	public void upsertGeographicLevels() {

		geoLevelDAO.upsertGeographicLevels(geographicLevels);
	}

	public void upsertFacilityTypes() {

		facilityDAO.upsertFacilityTypes(facilityTypes);
	}

	public void upsertFacilities() {

		facilityDAO.upsertFacilities(facilities);

	}

	public void upsertProcessingSchedule() {

		periodDAO.upsertProcessingSchedules(schedules);

	}

	public void deleteProcessingSchedules() {
		periodDAO.deleteProcessingSchedules();
	}

	public void deleteProgramProducts() {
		progProdDAO.deleteProgramProducts();
	}

	public void deleteFacilityApprovedProducts() {
		progProdDAO.deleteFacilityApprovedProduct();
	}

	public void insertFacilityApprovedProducts() {

		progProdDAO.insertFacilityApprovedProduct(faciltyApprovedProducts);

	}

	public void insertPeriods() {
	}

	public void deletePeriods() {
	}

	private void upsertProducts() {
		productDAO.upsertProducts(products);
	}

    private void upsertProductBatches() {
	    if (productBatches != null) {
            productDAO.upsertProductBatches(productBatches);
        }
    }


	/**
	 *
	 */

	private void upsertProductCategory() {

		productDAO.upsertProductCategories(categories);

	}

	public void upsertPrograms() {
		programDAO.upsertPrograms(programs);
	}

	public void upsertUsers() {
		userDAO.upsertUsers(users);
	}

	public void upsertRequisitions(){
        rnrService.upsertRnrs(requisitions);
        rnrService.upsertRnrs(emergencyRequisitions);
    }

	private void upsertRegimenLines() {
		regimenDAO.upsertRegimenLines(regimenLines);
	}

	private void upsertRegimens() {
		regimenDAO.upsertRegimens(regimens);
	}

	private void upsertRegimenProductCombinations() {
		regimenDAO.upsertRegimenProductCombinations(regimenProductCombinations);
	}

	private void upsertRegimenCombinationProducts() {
		regimenDAO.upsertRegimenCombinationProducts(regimenCombinationProducts);
	}

	private void upsertRegimenProductDosages() {
		regimenDAO.upsertRegimenProductDosages(regimenProductDosages);

	}

	private void upsertDosageUnits() {
		regimenDAO.upsertDosageUnits(dosageUnits);
	}

	private void upsertDosageFrequencies() {
		regimenDAO.upsertDosageFrequencies(dosageFrequencies);
	}

	private void upsertProgramProducts() {
		progProdDAO.upsertProgramProducts(programProducts);
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub

	}

	public ElmisSyncObject programExists(Program program){


        Program storedProgram = programDAO.selectByCode(program.getCode());
        if (storedProgram == null) return new ElmisSyncObject(program , null, null);

        Boolean changed = !program.compareTo(storedProgram);

        ElmisSyncObject elmisSyncObject = new ElmisSyncObject(program, storedProgram,  changed );
        return elmisSyncObject;
    }

      public ElmisSyncObject productExists(Product product){


          Product storedProduct = productDAO.selectProductByCode(product.getCode());
          if (storedProduct == null) return new ElmisSyncObject(product, null, null);

          Boolean changed = !product.compareTo(storedProduct);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(product, storedProduct,  changed );
          return elmisSyncObject;
      }


	public ElmisSyncObject objectExists(Object object){
/*      if (object instanceof Facility) {

            Facility fetchedFacility = (Facility)object;
            Facility storedFacility = facilityDAO.getFacilityByCode(fetchedFacility.getCode());
            Boolean noDiff = null;

            ElmisSyncObject elmisSyncObject = new ElmisSyncObject(fetchedFacility, storedFacility, Facility.class, noDiff );
            return elmisSyncObject;

        } else if (object instanceof Program) {

            Program fetchedProgram = (Program)object;
            Program storedProgram = programDAO.selectByCode(fetchedProgram.getCode());
            Boolean noDiff = null;

            ElmisSyncObject elmisSyncObject = new ElmisSyncObject(fetchedProgram, storedProgram, Program.class, noDiff );
            return elmisSyncObject;
        } */
	    return null;
    }

    public Boolean extractDiffs(){


	    Boolean extractionComplete = false;

        scanGeographicLevels(geographicLevels);
        scanGeographicZones(geographicZones);
        scanFacilityTypes(facilityTypes);

        scanProcessingSchedules(schedules);
        scanProcessingPeriods(processingPeriods);
        scanLossesAndAdjustmentTypes(lossesAndAdjustmentTypes);
        scanPrograms(programs);

        scanProductCatogoreis(categories);
        scanProducts(products);
        scanProductBatches(productBatches);

        scanDosageUnits(dosageUnits);
        scanDosageFrequencies(dosageFrequencies);
        scanRegimenLines(regimenLines);
        scanRegimens(regimens);
        scanRegimenProductCombinations(regimenProductCombinations);
        scanRegimenCombinationConstituents(regimenCombinationProducts);
        scanRegimenProductDosages(regimenProductDosages);


        scanFacilities(facilities);
        scanProgramProducts(programProducts);
        scanFacilityApprovedProducts(faciltyApprovedProducts);


        extractionComplete = true;
        return extractionComplete;
    }

      private void scanGeographicLevels(ArrayList<GeographicLevel> geographicLevels) {

          for (GeographicLevel geoLevel: geographicLevels) {
              ElmisSyncObject elmisSyncObject = geographicLevelExists(geoLevel);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject geographicLevelExists(GeographicLevel geoLevel) {
          GeographicLevel storedGeoLevel = geoLevelDAO.selectByCode(geoLevel.getCode());

          if( storedGeoLevel == null) return new ElmisSyncObject(geoLevel, null, null);
          Boolean changed = !geoLevel.compareTo(storedGeoLevel);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(geoLevel, storedGeoLevel,  changed );
          return elmisSyncObject;
      }

      private void scanGeographicZones(ArrayList<GeographicZone> geographicZones) {

          for (GeographicZone geographicZone: geographicZones) {
              ElmisSyncObject elmisSyncObject = geographicZoneExists(geographicZone);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject geographicZoneExists(GeographicZone geographicZone) {

          GeographicZone storedGeographicZone = geoZoneDAO.getGeographicZoneByCode(geographicZone.getCode());

          if( storedGeographicZone == null) return new ElmisSyncObject(geographicZone, null, null);

          Boolean changed = !geographicZone.compareTo(storedGeographicZone);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(geographicZone, storedGeographicZone,  changed );
          return elmisSyncObject;
      }

      private ElmisSyncObject regimenCombinationProductExists(RegimenCombinationProduct rcp) {
          RegimenCombinationProduct storedRCP = regimenDAO.getRegimenComboProductByProductIdandProductComboId(rcp.getProductCombination().getId(), rcp.getProduct().getId());
          if(storedRCP == null ) return  new ElmisSyncObject(rcp, null, null);

          Boolean changed = !rcp.compareTo(storedRCP);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(rcp, storedRCP,  changed );
          return  elmisSyncObject;
      }

    private void scanRegimenCombinationConstituents(ArrayList<RegimenCombinationProduct> regimenCombinationProducts){

        for (RegimenCombinationProduct rcp: regimenCombinationProducts) {
            ElmisSyncObject elmisSyncObject = regimenCombinationProductExists(rcp);
            if(elmisSyncObject.getStoredObject() != null) {
                if(elmisSyncObject.getChanged()) {
                    createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                }
            } else {
                createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
            }
        }
    }



      private void scanRegimenProductCombinations(ArrayList<RegimenProductCombination> regimenProductCombinations) {

          for (RegimenProductCombination rpc: regimenProductCombinations) {
              ElmisSyncObject elmisSyncObject = regimenProductCombinationExists(rpc);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject regimenProductCombinationExists(RegimenProductCombination rpc) {
          RegimenProductCombination storedRPC = regimenDAO.getRegimenProductComboByName(rpc.getName());
          if(storedRPC ==  null) return new ElmisSyncObject(rpc, null, null);
          Boolean changed = !rpc.compareTo(storedRPC);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(rpc, storedRPC,  changed );
          return  elmisSyncObject;
      }

      private void scanRegimens(ArrayList<Regimen> regimens){
          for (Regimen regimen: regimens) {
              ElmisSyncObject elmisSyncObject = regimenExists(regimen);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
    }

      private ElmisSyncObject regimenExists(Regimen regimen) {
          Regimen storedRegimen = regimenDAO.getRegimenByCode(regimen.getCode());
          if(storedRegimen == null) return new ElmisSyncObject(regimen, null, null);
          Boolean changed = !regimen.compareTo(storedRegimen);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(regimen, storedRegimen,  changed );
          return  elmisSyncObject;
      }

      private void scanRegimenLines(ArrayList<RegimenLine> regimenLines) {

          for (RegimenLine regimenLine: regimenLines) {
              ElmisSyncObject elmisSyncObject = regimenLineExists(regimenLine);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject regimenLineExists(RegimenLine regimenLine) {

          RegimenLine storedRegLine = regimenDAO.getRegimenLineByCode(regimenLine.getCode());
          if (storedRegLine == null) return new ElmisSyncObject(regimenLine, null, null);
          Boolean changed = !regimenLine.compareTo(storedRegLine);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(regimenLine, storedRegLine,  changed );
          return  elmisSyncObject;
      }

      private void scanDosageFrequencies(ArrayList<DosageFrequency> dosageFrequencies){

          for (DosageFrequency dFreq: dosageFrequencies) {
              ElmisSyncObject elmisSyncObject = dosageFrequencyExists(dFreq);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }

    }

      private ElmisSyncObject dosageFrequencyExists(DosageFrequency dFreq) {

          DosageFrequency storedDFreq = regimenDAO.getDosageFrequecyByName(dFreq.getName());
          if( storedDFreq == null) return new ElmisSyncObject(dFreq, null, null);
          Boolean changed = !dFreq.compareTo(storedDFreq);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(dFreq, storedDFreq,  changed );
          return  elmisSyncObject;
      }

      private void scanDosageUnits(ArrayList<DosageUnit> dosageUnits) {

          for (DosageUnit dUnit: dosageUnits) {
              ElmisSyncObject elmisSyncObject = dosageUnitExits(dUnit);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject dosageUnitExits(DosageUnit dUnit) {
          DosageUnit storedDUnit = regimenDAO.getDosageUnitByName(dUnit.getName());

          if(storedDUnit == null) return new ElmisSyncObject(dUnit, null, null);
          Boolean changed = !dUnit.compareTo(storedDUnit);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(dUnit, storedDUnit,  changed );
          return  elmisSyncObject;
      }

      private void scanRegimenProductDosages(ArrayList<RegimenProductDosage> regimenProductDosages) {


          for (RegimenProductDosage rpd: regimenProductDosages) {
              ElmisSyncObject elmisSyncObject = regProdDosageExists(rpd);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject regProdDosageExists(RegimenProductDosage rpd) {
          RegimenProductDosage storedRPD = regimenDAO.getRegimenProductDosage(rpd);

          if(storedRPD == null) return new ElmisSyncObject(rpd, null, null);
          Boolean changed = !rpd.compareTo(storedRPD);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(rpd,storedRPD,  changed );
          return elmisSyncObject;
      }

      private void scanFacilityApprovedProducts(List<FacilityApprovedProduct> facilityApprovedProducts){
          for (FacilityApprovedProduct fap: facilityApprovedProducts) {
              ElmisSyncObject elmisSyncObject = facilityApprovedProductExists(fap);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
    }

      private ElmisSyncObject facilityApprovedProductExists(FacilityApprovedProduct fap) {
          FacilityApprovedProduct storedFAP = programProductDAO.getFacilityApprovedProduct(fap.getProgramproductid(), fap.getFacilitytypeid());

          if(storedFAP == null) return new ElmisSyncObject(fap, null, null);
          Boolean changed = !fap.compareTo(storedFAP);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(fap,storedFAP,  changed );
          return elmisSyncObject;
      }

      private void scanProgramProducts(List<ProgramProduct> programProducts){
        for (ProgramProduct pProd: programProducts) {
            ElmisSyncObject elmisSyncObject = pProdExists(pProd);
            if(elmisSyncObject.getStoredObject() != null) {
                if(elmisSyncObject.getChanged()) {
                    createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                }
            } else {
                createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
            }
        }
    }

      private ElmisSyncObject pProdExists(ProgramProduct pProd) {

          ProgramProduct sPProd = programProductDAO.getProgramProduct(pProd.getProductid(), pProd.getProgramid());

          if(sPProd == null) return new ElmisSyncObject(pProd, null, null);

          Boolean changed = !pProd.compareTo(sPProd);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(pProd, sPProd,  changed );
          return elmisSyncObject;
      }

      private void scanFacilities(List<Facility> facilities){

        for (Facility facility: facilities) {
            ElmisSyncObject elmisSyncObject = facilityExists(facility);
            if(elmisSyncObject.getStoredObject() != null) {
                if(elmisSyncObject.getChanged()) {
                    createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                }
            } else {
                createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
            }
        }

    }

      private ElmisSyncObject facilityExists(Facility facility) {
          Facility storeFacility = facilityDAO.getFacilityByCode(facility.getCode());

          if( storeFacility == null) return new ElmisSyncObject(facility, null, null);
          Boolean changed = !facility.compareTo(storeFacility);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(facility, storeFacility,  changed );
          return elmisSyncObject;


      }

      private void scanFacilityTypes(ArrayList<FacilityType> facilityTypes) {

          for (FacilityType fType: facilityTypes) {
              ElmisSyncObject elmisSyncObject = faciltyTypeExists(fType);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject faciltyTypeExists(FacilityType fType) {
          FacilityType storedFacilityType = facilityDAO.getFacilityTyepByCode(fType.getCode());

          if (storedFacilityType == null) return new ElmisSyncObject(fType, null, null);
          Boolean changed = !fType.compareTo(storedFacilityType);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(fType, storedFacilityType,  changed );
          return elmisSyncObject;
      }

      private void scanLossesAndAdjustmentTypes(List<LossAdjustmentType> laTypes) {

          for (LossAdjustmentType laType: lossesAndAdjustmentTypes) {
              ElmisSyncObject elmisSyncObject = laExists(laType);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject laExists(LossAdjustmentType laType) {
          LossAdjustmentType storedLAType = adjustmentDAO.findAdjustmentByName(laType.getName());

          if (storedLAType == null) return new ElmisSyncObject(laType, null, null);
          Boolean changed = !laType.compareTo(laType);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(laType, storedLAType,  changed );
          return elmisSyncObject;


      }

      private void scanProcessingPeriods(List<ProcessingPeriod> processingPeriods){
          for (ProcessingPeriod period: processingPeriods) {
              ElmisSyncObject elmisSyncObject = periodExists(period);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
    }

      private ElmisSyncObject periodExists(ProcessingPeriod period) {
          ProcessingPeriod storedPeriod = periodDAO.getProcessingPeriod(period.getStartdate(), period.getEnddate());
          Boolean changed = !period.compareTo(period);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(period, storedPeriod,  changed );
          return elmisSyncObject;
      }

      private void scanProcessingSchedules(ArrayList<ProcessingSchedule> schedules) {

          for (ProcessingSchedule schedule: schedules) {
              ElmisSyncObject elmisSyncObject = scheduleExists(schedule);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private ElmisSyncObject scheduleExists(ProcessingSchedule schedule) {

          ProcessingSchedule storedSchedule = periodDAO.selectScheduleByCode(schedule.getCode());
          if (storedSchedule == null ) return new ElmisSyncObject(schedule, null, null);

          Boolean changed = !schedule.compareTo(storedSchedule);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(schedule, storedSchedule,  changed );
          return elmisSyncObject;
      }

      private void scanPrograms(ArrayList<Program> programs) {

          for (Program program: programs) {
              ElmisSyncObject elmisSyncObject = programExists(program);
              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
              }
          }
      }

      private void scanProductBatches(ArrayList<ProductBatch> productBatches) {
	    if(productBatches != null){
            int insert_count = 0;
            int update_count = 0;
            for (ProductBatch productBatch: productBatches) {
                ElmisSyncObject elmisSyncObject = productBatchExists(productBatch);

                if(elmisSyncObject.getStoredObject() != null) {
                    if(elmisSyncObject.getChanged()) {
                        createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                        update_count++;
                    }
                } else {
                    createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
                    insert_count++;
                }
            }
            System.out.println(insert_count + " product batches to insert");
            System.out.println(update_count + " product batches to update") ;
        }

      }

      private ElmisSyncObject productBatchExists(ProductBatch productBatch) {
          ProductBatch storedProductBatch = productDAO.selectProductBatchByName(productBatch.getName());
          if (storedProductBatch == null) return new ElmisSyncObject(productBatch, null, null);
          Boolean changed = !productBatch.compareTo(storedProductBatch);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(productBatch, storedProductBatch,  changed );
          return elmisSyncObject;
      }

      private void scanProductCatogoreis(ArrayList<ProductCategory> categories) {
          int insert_count = 0;
          int update_count = 0;
          for (ProductCategory productCategory: categories) {
              ElmisSyncObject elmisSyncObject = productCategoryExists(productCategory);

              if(elmisSyncObject.getStoredObject() != null) {
                  if(elmisSyncObject.getChanged()) {
                      createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                      update_count++;
                  }
              } else {
                  createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
                  insert_count++;
              }
          }
          System.out.println(insert_count + " products to insert");
          System.out.println(update_count + " products to update") ;

      }

      private ElmisSyncObject productCategoryExists(ProductCategory productCategory) {
          ProductCategory storedCategory = productDAO.selectCategoryByCode(productCategory.getCode());
          if( storedCategory == null)  return new ElmisSyncObject(productCategory, null, null);
          Boolean changed = !productCategory.compareTo(storedCategory);

          ElmisSyncObject elmisSyncObject = new ElmisSyncObject(productCategory, storedCategory,  changed );
          return elmisSyncObject;
      }

      private void scanProducts(List<Product> lproducts){
        int insert_count = 0;
        int update_count = 0;
        for (Product product: products) {
            ElmisSyncObject elmisSyncObject = productExists(product);

            if(elmisSyncObject.getStoredObject() != null) {
                if(elmisSyncObject.getChanged()) {
                    createAuditEntry(UPDATE, elmisSyncObject.getFetchedObject());
                    update_count++;
                }
            } else {
                createAuditEntry(INSERT, elmisSyncObject.getFetchedObject());
                insert_count++;
            }
        }
        System.out.println(insert_count + " products to insert");
        System.out.println(update_count + " products to update") ;
    }
      public void createAuditEntry(String operation, Object object) {
          Gson gson = new Gson();
          String objectAsJSON = gson.toJson(object);
	    System.out.println("Operation: " + operation + "\nData: " + objectAsJSON);
        auditDAO.insertDataChange(new DataChange(null, operation, object.getClass().getSimpleName(), objectAsJSON, null, null));
	}

	public List<DataChange> lookUpSyncData(String currentVersionNumber){
        UUID vNumberUUID = currentVersionNumber != null ? UUID.fromString(currentVersionNumber) : null;
        return auditDAO.dataChangesAfterVersion(vNumberUUID);
    }

    public SyncRequestResult syncDelta(String currentVersion){

        String installationType = propertyService.getProperty("installation.type", false).getValue();
        String syncBaseURL;

        if(installationType.equalsIgnoreCase("MEDIATOR")){
            syncBaseURL = propertyService.getProperty("mediator.base.url", false).getValue();
        }else{
            syncBaseURL = propertyService.getProperty("elmis.central.url", false).getValue();
        }

        if (Connectivity.isReachable(syncBaseURL)) {

            if(currentVersion == null){
                ApplicationProperty dbVersionNumber = propertyService.getProperty(ELMISConstants.CURRENT_DB_VERSION_NUMBER.getValue(), true);

                currentVersion = dbVersionNumber != null ? dbVersionNumber.getValue() : null;
            }

            List<DataChange> dataChanges = wsClientInterfacing.downloadSyncDataAfterVersion(syncBaseURL, currentVersion);
            if(dataChanges.size() > 0){
                for(DataChange dataChange : dataChanges){
                    System.out.println("Operation: " + dataChange.getOperation() + " DataType: " + dataChange.getObjectType());
                    executeOperation(dataChange.getOperation(), dataChange.getObjectType(), dataChange.getRecord());
                }

                DataChange latestChange = dataChanges.get(dataChanges.size() - 1);

                ApplicationProperty applicationProperty =
                        new ApplicationProperty(ELMISConstants.CURRENT_DB_VERSION_NUMBER.getValue(),
                                latestChange.getVersionNumber().toString(),
                                "String",
                                null,
                                null,
                                null,
                                false);

                propertyService.saveApplicationProperties(Arrays.asList(applicationProperty));
            }


            System.out.println("completed!");
            return new SyncRequestResult(true, "Synchronization completed!");
        } else {
            System.out.println("Sync not completed!");
            return new SyncRequestResult(false,
                    "Unable to reach eLMIS server! Please check your connection and try again!");
        }
    }

    private void executeOperation(String operation, String objectType, Object object){

          switch (objectType){
              case "GeographicLevel":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertGeographicLevelSelective(gson.fromJson(object.toString(), GeographicLevel.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateGeographicLevelByCode(gson.fromJson(object.toString(), GeographicLevel.class));
                  }
                  break;

              case "GeographicZone":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertGeographicZoneSelective(gson.fromJson(object.toString(), GeographicZone.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateGeographicZoneByCode(gson.fromJson(object.toString(), GeographicZone.class));
                  }
                  break;

              case "FacilityType":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertFacilityTypeSelective(gson.fromJson(object.toString(), FacilityType.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateFacilityTypeByCode(gson.fromJson(object.toString(), FacilityType.class));
                  }
                  break;

              case "Facility":
                  if(operation.equalsIgnoreCase(INSERT)){
                      facilityDAO.insertFacilitySelective(gson.fromJson(object.toString(), Facility.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      facilityDAO.updateFacilityByCode(gson.fromJson(object.toString(), Facility.class));
                  }
                  break;

              case "Product":
                  if(operation.equalsIgnoreCase(INSERT)){
                      productDAO.insertProductSelective(gson.fromJson(object.toString(), Product.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      productDAO.updateProductByCode(gson.fromJson(object.toString(), Product.class));
                  }
                  break;

              case "ProductCategory":
                  if(operation.equalsIgnoreCase(INSERT)){
                      productDAO.insertProductCategorySelective(gson.fromJson(object.toString(), ProductCategory.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      productDAO.updateProductCategoryByCode(gson.fromJson(object.toString(), ProductCategory.class));
                  }
                  break;

              case "ProductBatch":
                  if(operation.equalsIgnoreCase(INSERT)){
                      productDAO.insertProductBatchSelective(gson.fromJson(object.toString(), ProductBatch.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      productDAO.updateProductBatchByName(gson.fromJson(object.toString(), ProductBatch.class));
                  }
                  break;

              case "Program":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertProgramSelective(gson.fromJson(object.toString(), Program.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateProgramByCode(gson.fromJson(object.toString(), Program.class));
                  }
                  break;

              case "ProgramProduct":
                  if(operation.equalsIgnoreCase(INSERT)){
                      productDAO.insertProgramProductSelective(gson.fromJson(object.toString(), ProgramProduct.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      productDAO.updateProgramProductByProgramIdAndProductId(gson.fromJson(object.toString(), ProgramProduct.class));
                  }
                  break;

              case "FacilityApprovedProduct":
                  if(operation.equalsIgnoreCase(INSERT)){
                      productDAO.insertFacilityApprovedProductSelective(gson.fromJson(object.toString(), FacilityApprovedProduct.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      productDAO.updateByFacilityTypeIdAndProgramProductId(gson.fromJson(object.toString(), FacilityApprovedProduct.class));
                  }
                  break;

              case "ProcessingSchedule":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertProcessingScheduleSelective(gson.fromJson(object.toString(), ProcessingSchedule.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateProcessingScheduleByCode(gson.fromJson(object.toString(), ProcessingSchedule.class));
                  }
                  break;

              case "ProcessingPeriod":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertProcessingPeriodSelective(gson.fromJson(object.toString(), ProcessingPeriod.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateProcessingPeriodByDateRange(gson.fromJson(object.toString(), ProcessingPeriod.class));
                  }
                  break;

              case "LossAdjustmentType":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertLossAndAdjustmentType(gson.fromJson(object.toString(), LossAdjustmentType.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateLossAndAdjustmentTypeByName(gson.fromJson(object.toString(), LossAdjustmentType.class));
                  }
                  break;

              case "DosageUnit":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertDosageUnitSelective(gson.fromJson(object.toString(), DosageUnit.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateDosageUnitByName(gson.fromJson(object.toString(), DosageUnit.class));
                  }
                  break;

              case "DosageFrequency":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertDosageFrequencySelective(gson.fromJson(object.toString(), DosageFrequency.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateDosageFrequencyByName(gson.fromJson(object.toString(), DosageFrequency.class));
                  }
                  break;

              case "RegimenLine":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertRegimenLineSelective(gson.fromJson(object.toString(), RegimenLine.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateRegimenLineByCode(gson.fromJson(object.toString(), RegimenLine.class));
                  }
                  break;

              case "Regimen":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertRegimenSelective(gson.fromJson(object.toString(), Regimen.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateRegimenByCode(gson.fromJson(object.toString(), Regimen.class));
                  }
                  break;

              case "RegimenProductCombination":
                  RegimenProductCombination rpc = gson.fromJson(object.toString(), RegimenProductCombination.class);
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertRegimenProductCombination(rpc);
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateRegimenProductCombinationByName(rpc);
                  }
                  break;

              case "RegimenCombinationProduct":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertRegimenCombinationProduct(gson.fromJson(object.toString(), RegimenCombinationProduct.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateRegimenCombinationProductByComboId(gson.fromJson(object.toString(), RegimenCombinationProduct.class));
                  }
                  break;

              case "RegimenProductDosage":
                  if(operation.equalsIgnoreCase(INSERT)){
                      commonDAO.insertRegimenProductDosage(gson.fromJson(object.toString(), RegimenProductDosage.class));
                  }else if(operation.equalsIgnoreCase(UPDATE)){
                      commonDAO.updateRegimenProductDosageByCombinationId(gson.fromJson(object.toString(), RegimenProductDosage.class));
                  }
                  break;
          }
    }

}
