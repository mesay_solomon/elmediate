/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.quartz.job.facade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.RequisitionLineItem;
import org.jsi.elmis.model.RequisitionLineItemLossesAndAdjustment;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.LossesAndAdjustments;
import org.jsi.elmis.rnr.OfflineRnRDTO;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.rnr.RnRLineItem;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OfflineRequisitionFacade {
	@Autowired
	OfflineRnRService offlineRnRService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	ProductService productService;
	@Autowired
	AdjustmentService adjService;
	@Autowired
    PropertiesService propertyService;
	
	List<ProgramProduct> programProducts;
	
	public List<OfflineRnRDTO> getOfflineRequistionsForSubmission(){
		List<OfflineRnRDTO> rnrsForSubmission = new ArrayList<>();
		List<Requisition> offlineRequisitions = offlineRnRService.getOfflineRequisitionsByStatus(ELMISConstants.READY_FOR_SUBMISSION.getValue());
		OfflineRnRDTO offlineRnRDTO ;
		RnR rnr = null;
		for(Requisition requisition : offlineRequisitions){

			offlineRnRDTO=getOfflineRequisition(requisition);
			rnrsForSubmission.add(offlineRnRDTO);
		}
		return rnrsForSubmission;
	}

	public OfflineRnRDTO getOfflineRequisition( Requisition requisition) {
		OfflineRnRDTO offlineRnRDTO = new OfflineRnRDTO();
		RnR rnr;
		rnr = new RnR();
		rnr.setAgentCode(miscService.getFacilityById(requisition.getFacilityId()).getCode());
		rnr.setPeriodId(requisition.getPeriodId());
		rnr.setEmergency(requisition.getEmergency());

		Optional<Program> programMatch  = miscService.getAllPrograms().stream().filter(prg -> prg.getId().equals(requisition.getProgramId())).findFirst();

		rnr.setProgramCode(programMatch.get().getCode());

		programProducts = productService.getProgramProductsByProgramCode(rnr.getProgramCode());

		rnr.setProducts(toRnRLineItems(requisition.getRequisitionLineItems()));
		rnr.setRegimens(new ArrayList<>());

		rnr.setApproverName(propertyService.getProperty("elmis.central.approver.name", false).getValue());

		offlineRnRDTO.setRnr(rnr);
		offlineRnRDTO.setOfflineRequisition(requisition);
		return offlineRnRDTO;
	}

	private List<RnRLineItem> toRnRLineItems(List<RequisitionLineItem> requisitionLineItems) {
		List<RnRLineItem> rnrLineItems = new ArrayList<>();
		List<LossAdjustmentType>lossesAndAdjustmentTypes = adjService.getAllAdjustments();
		
		RnRLineItem rnrLineItem = null;
		for(RequisitionLineItem requisitionLineItem : requisitionLineItems){
			
			rnrLineItem = new RnRLineItem();			
						
			Optional<ProgramProduct> matchProgramProduct = programProducts.stream().filter(programProduct -> programProduct.getProductid().equals(requisitionLineItem.getProductId())).findFirst();
			ProgramProduct lineItemProgramProduct = matchProgramProduct.get();
			rnrLineItem.setProductCode(lineItemProgramProduct.getProduct().getCode());
			rnrLineItem.setBeginningBalance(getIntValue(requisitionLineItem.getBeginningBalance()));
			rnrLineItem.setQuantityReceived(getIntValue(requisitionLineItem.getQuantityReceived()));
			rnrLineItem.setQuantityDispensed(getIntValue(requisitionLineItem.getQuantityDispensed()));
			rnrLineItem.setStockInHand(getIntValue(requisitionLineItem.getStockOnHand()));
			rnrLineItem.setStockOutDays(requisitionLineItem.getStockOutDays());
			rnrLineItem.setNewPatientCount(requisitionLineItem.getNewPatientCount());
			rnrLineItem.setQuantityRequested(getIntValue(requisitionLineItem.getQuantityRequested()));
			rnrLineItem.setAmc(getIntValue(requisitionLineItem.getAmc()));
			rnrLineItem.setNormalizedConsumption(getIntValue(requisitionLineItem.getNormalizedConsumption()));
			rnrLineItem.setCalculatedOrderQuantity(getIntValue(requisitionLineItem.getCalculatedOrderQuantity()));
			rnrLineItem.setMaxStockQuantity(getIntValue(requisitionLineItem.getMaxStockQuantity()));
			rnrLineItem.setDispensingUnit(requisitionLineItem.getDispensingUnit());
			rnrLineItem.setMaxMonthsOfStock(getDoubleValue(requisitionLineItem.getMaxMonthsOfStock()));
			rnrLineItem.setPackSize(getIntValue(requisitionLineItem.getPackSize()));
			rnrLineItem.setDosesPerMonth(getIntValue(requisitionLineItem.getDosesPerMonth()));
			rnrLineItem.setDosesPerDispensingUnit(getIntValue(requisitionLineItem.getDosesPerDispensingUnit()));
			rnrLineItem.setQuantityApproved(getIntValue(requisitionLineItem.getQuantityApproved()));
			rnrLineItem.setFullSupply(requisitionLineItem.getFullSupply());
			rnrLineItem.setRemarks(requisitionLineItem.getRemarks());
			rnrLineItem.setSkipped(requisitionLineItem.getSkipped());
			rnrLineItem.setReasonForRequestedQuantity(requisitionLineItem.getReasonForRequestedQuantity());
			
			if(requisitionLineItem.getRliLossesNAdjustments().size() > 0){
				
				List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<>();
				LossesAndAdjustments rnrLineItemLossesAndAdjustments = null;
				
				for(RequisitionLineItemLossesAndAdjustment rlilaa : requisitionLineItem.getRliLossesNAdjustments()){
					rnrLineItemLossesAndAdjustments = new LossesAndAdjustments();
					Optional<LossAdjustmentType> matchLossAdjustmentType = lossesAndAdjustmentTypes.stream().filter(adjType -> adjType.getName().equalsIgnoreCase(rlilaa.getLossesAndAdjustmentType())).findFirst();
					rnrLineItemLossesAndAdjustments.setType(matchLossAdjustmentType.get());
					rnrLineItemLossesAndAdjustments.setQuantity(rlilaa.getQuantity().intValue());
					
					lossesAndAdjustments.add(rnrLineItemLossesAndAdjustments);
				}
				
				rnrLineItem.setLossesAndAdjustments(lossesAndAdjustments);
			}
			
			rnrLineItems.add(rnrLineItem);
		}
		
		
		return rnrLineItems;
	}

	public Requisition convert(Rnr rnr){

	    Requisition requisition = new Requisition();



	    return new Requisition();
    }

    private Integer getIntValue(BigDecimal value){
	    if(value == null){
	        return 0;
        }
        return value.intValue();
    }

    private Double getDoubleValue(BigDecimal value){
        if(value == null){
            return 0d;
        }
        return value.doubleValue();
    }
}
