/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.quartz;

import org.apache.log4j.Logger;
import org.jsi.elmis.quartz.job.StockStatusJob;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Component
@ConditionalOnProperty(value = { "quartz.dailystockstatus.enabled" })
public class DailyStockStatusQuartzScheduler extends ELMISQuartzScheduler{

    private final Logger logger = Logger.getLogger(getClass());

    @Autowired
    PropertiesService propertyService;

    @PostConstruct
    public void init() throws IOException, SchedulerException {
        logger.info("starting daily stock status quartz...");

        String dailyStockStatusCronExpression = propertyService.getProperty("daily_stock_status.cron", false).getValue();
        JobDetail jbSendDailyStockStatus = newJob().ofType(StockStatusJob.class).storeDurably()
                .withIdentity(JobKey.jobKey("Qrtz_Job_Send_Daily_Stock_Status"))
                .withDescription("Invoke Send Daily Stock Status Service").build();

        Trigger trgSendDailyStockStatus = newTrigger().forJob(JobKey.jobKey("Qrtz_Job_Send_Daily_Stock_Status"))
                .withIdentity(TriggerKey.triggerKey("Qrtz_Trigger_Send_Daily_Stock_Status"))
                .withDescription(dailyStockStatusCronExpression)
                .withSchedule(cronSchedule(dailyStockStatusCronExpression)).build();

        scheduler().scheduleJob(jbSendDailyStockStatus, trgSendDailyStockStatus);
    }
}
