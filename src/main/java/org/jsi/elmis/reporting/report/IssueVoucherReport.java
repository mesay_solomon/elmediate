package org.jsi.elmis.reporting.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsi.elmis.reporting.ResultRow;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueVoucherReport implements ResultRow {
    private Integer productId;
    private String productCode;
    private String productName;
    private String generalStrength;
    private Double qtyToIssue;
    private String issuedBy;
    private String receivedBy;
    private String issuedTo;
    private Date txnDate;
}
