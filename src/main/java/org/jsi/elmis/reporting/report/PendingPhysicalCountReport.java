package org.jsi.elmis.reporting.report;

import lombok.Getter;
import lombok.Setter;
import org.jsi.elmis.reporting.ResultRow;

@Getter
@Setter
public class PendingPhysicalCountReport implements ResultRow {
    private String nodeName;
    private String productCode;
    private String productName;
}
