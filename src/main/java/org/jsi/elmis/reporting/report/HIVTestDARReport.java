package org.jsi.elmis.reporting.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsi.elmis.reporting.ResultRow;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HIVTestDARReport implements ResultRow {
    private String id;
    private Integer facility_id;
    private String client_id;
    private String purpose;
    private String screeningResult;
    private String confirmatoryResult;
    private String finalResult;
    private String negativeResult;
    private String positiveResult;
    private String indeterminateResult;
    private Date testDate;
    /////////dummy
    private String testId;
}
