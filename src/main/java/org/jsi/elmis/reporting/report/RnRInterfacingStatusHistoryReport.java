package org.jsi.elmis.reporting.report;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RnRInterfacingStatusHistoryReport {
    private Integer id;
    private Integer rnrId;
    private String status;
    private String statusRemark;
    private Date sentDate;
}
