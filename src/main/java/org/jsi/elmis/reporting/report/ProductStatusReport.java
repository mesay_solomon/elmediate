package org.jsi.elmis.reporting.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsi.elmis.reporting.ResultRow;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductStatusReport implements ResultRow {
    private String productCode;
    private String productName;
    private Double balance;
    private Double usableBalance;
}
