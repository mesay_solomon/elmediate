package org.jsi.elmis.reporting.report;

import lombok.Getter;
import lombok.Setter;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
@Getter
@Setter
public class StockControlCardReport implements ResultRow{




    private String id;
    private String refno;
    private String issueto_receivedfrom;
    private Double qty_received;
    private Double qty_isssued;
    private Double adjustments;
    private double balance;
    private String remark;
    private String program_area;
    private String createdby;
    private Timestamp createdtimestamp;
    private Date createddate;
    private Integer productid;
    private String productcode;
    private Integer dpid;

    public StockControlCardReport(Date date, String refNo,String fromOrTo,BigDecimal qtyReceived, BigDecimal qtyIssued,
                                  BigDecimal lossesNAdjustments, BigDecimal balance, String remark, String user){
        setCreateddate(date);
        setRefno(refNo);
        setIssueto_receivedfrom(fromOrTo);
        if(qtyReceived!=null) {
            setQty_received(qtyReceived.doubleValue());
        }
        if(qtyIssued!=null){
            setQty_isssued(qtyIssued.doubleValue());
        }
        if(lossesNAdjustments!=null){
            setAdjustments(lossesNAdjustments.doubleValue());
        }
        setBalance(balance.doubleValue());
        setRemark(remark);
        setCreatedby(user);
    }


}
