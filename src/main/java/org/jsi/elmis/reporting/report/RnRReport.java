package org.jsi.elmis.reporting.report;

import lombok.Getter;
import lombok.Setter;
import org.jsi.elmis.reporting.ResultRow;

@Getter
@Setter
public class RnRReport implements ResultRow {
    private String productCode;
    private Integer productId;
    private Integer beginningBal;
    private Integer daysStockedOut;
    private String drugProduct;
    private Integer lossAdjustment;
    private String explnLossAdj;
    private Integer maxQty;
    private Integer orderQty;
    private Integer phyCount;
    private Integer qtyDispensed;
    private Integer totalReceived;
    private String unit;
    private String typeTest;
    private String productCategory;
    private Double currentPrice;
    private Double amc;
    private Boolean skipped;
}
