package org.jsi.elmis.reporting.report;

import lombok.Getter;
import lombok.Setter;
import org.jsi.elmis.reporting.ResultRow;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class RnRInterfacingReport implements ResultRow {
    private Integer id;
    private Integer facilityId;
    private String facility;
    private Integer programId;
    private String program;
    private String periodId;
    private String period;
    private String status;
    private List<RnRInterfacingStatusHistoryReport> statusHistoryReportList;



}
