package org.jsi.elmis.reporting;

import net.sf.jasperreports.engine.JRParameter;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportManager {

    private IReportExporter reportExporter;

    private Map<String,Report> reportsByKey;

    public ReportManager(IReportExporter reportExporter, List<Report> reports) {
        this(reports);
        this.reportExporter = reportExporter;
    }

    private ReportManager(List<Report> reports){
        if(reports != null){
            reportsByKey = new HashMap<>();
            for (Report report: reports){
                reportsByKey.put(report.getReportKey(),report);
            }
        }
    }


    /**
     *
     * @param reportKey
     * @param params
     * @param outputOption
     * @param response
     */
    public void showReport(Integer userId, String reportKey, Map<String, String[]> params, ReportOutputOption outputOption, HttpServletResponse response){
        showReport(userId, getReportByKey(reportKey), params, outputOption, response);
    }

    /**
     *
     * @param report
     * @param params
     * @param outputOption
     * @param response
     */
    public void showReport(Integer userId, Report report, Map<String, String[]> params, ReportOutputOption outputOption, HttpServletResponse response){

        if (report == null){
            //TODO: handle this properly throw new Exception("invalid report");
        }

        List<? extends ResultRow> dataSource = report.getReportDataProvider().getResultSet(params);
        Map<String, Object> extraParams = report.getReportDataProvider().getExtraReportParams();//TODO: handle this properly ??? getReportExtraDataSourceParams(userId, param, outputOption, dataSource, report);
        //TODO: workaround better implement
        InputStream reportInputStream =  this.getClass().getClassLoader().getResourceAsStream(report.getTemplate()) ;
        reportExporter.exportReport(reportInputStream, extraParams, dataSource, outputOption, response);
    }

    public Report getReportByKey(String reportKey){
        return reportsByKey.get(reportKey);
    }
}
