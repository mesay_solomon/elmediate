package org.jsi.elmis.reporting.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DispensationDARReportParam {
    private Long nodeId;
    private String fromDate;
    private String toDate;
    SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy");

    public Date getFromDate(){

        try {
            return sdf.parse(fromDate);
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    public Date getToDate(){
        try {
            return sdf.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
