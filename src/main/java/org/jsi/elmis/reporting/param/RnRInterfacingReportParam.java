package org.jsi.elmis.reporting.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RnRInterfacingReportParam {
    private Integer rnrId;
    private Integer programId;
    private Integer facilityId;
    private Integer periodId;
    private String fromDate;
    private String toDate;
    private Boolean emergency;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    public Date getFromDate() {
        try {
            if (fromDate != null) {
                return sdf.parse(fromDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Date getToDate() {
        try {
            if (toDate != null) {
                return sdf.parse(toDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }
}
