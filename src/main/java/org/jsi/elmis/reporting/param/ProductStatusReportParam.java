package org.jsi.elmis.reporting.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductStatusReportParam {
    private Long nodeId;
    private String date;
    private Long programId;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    public Date getDate(){
        try {
            if(date != null)
            {
                return sdf.parse(date);
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }
}
