package org.jsi.elmis.reporting.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PendingPhysicalCountParam {
    private Integer periodId;
    private String scheduleCode;
    private String programCode;
}
