package org.jsi.elmis.reporting.param;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IssueVoucherParam {
    private Integer txnId;
    private String receivedBy;
}
