package org.jsi.elmis.reporting.param;


import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class HIVDARReportParam {
    private String fromDate;
    private String toDate;
    private Integer nodeId;

    SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy");

    public Date getFromDate(){
        try {
            if(fromDate != null)
            {
                return sdf.parse(fromDate);
            }
        } catch (ParseException e){
            e.printStackTrace();
        }
        return null;
    }

    public Date getToDate(){
        try {
            if(toDate != null)
            {
                return sdf.parse(toDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }
}
