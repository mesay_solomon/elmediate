package org.jsi.elmis.reporting;

import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRParameter;
import org.apache.ibatis.session.RowBounds;
import org.jsi.elmis.model.ApplicationProperty;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ReportDataProvider {
    @Getter
    @Setter
    private Long userId;

    @Autowired
    PropertiesService propertiesService;

    protected Map<String, Object> parameters = new HashMap<>();

    public List<? extends ResultRow> getResultSet(Map<String, String[]> params){
        return getReportBody(params, null, RowBounds.NO_ROW_OFFSET, RowBounds.NO_ROW_LIMIT);
    }

    public abstract List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize);

    public Map<String,String> getExtendedHeader(Map params){
        return Collections.emptyMap();
    }

    public String getFilterSummary(Map<String, String[]> params){
        return "";
    }
    public abstract Map<String, Object> getExtraReportParams();

    public void appendGeneralParams(Map<String, Object> parameters){
        ApplicationProperty logoProperty = propertiesService.getProperty("report.logo.filename", false);
        String logoImagePath = logoProperty != null ? "Reports/images/" + logoProperty.getValue() : "Reports/images/zambia.jpg";
        InputStream logoInputStream = this.getClass().getClassLoader().getResourceAsStream(logoImagePath);
        parameters.put("logo", logoInputStream);
        parameters.put("title",propertiesService.getProperty("report.title", false).getValue());
        parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.FALSE);
    }

}
