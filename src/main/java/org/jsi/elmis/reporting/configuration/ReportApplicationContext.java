package org.jsi.elmis.reporting.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath*:applicationContext-report.xml"})
public class ReportApplicationContext {
}
