package org.jsi.elmis.reporting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Report {
    private String reportKey;
    private String template;
    private ReportDataProvider reportDataProvider;
    private ResultRow filterOption;


    // report properties used by report design
    private String title;
    private String subTitle;
    private String subBranchTitle;
    private String name;
    private String id;
    private String version;
}
