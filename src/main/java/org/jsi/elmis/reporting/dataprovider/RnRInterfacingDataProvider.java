package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.RnRInterfacingReportParam;
import org.jsi.elmis.reporting.param.StockControlCardReportParam;
import org.jsi.elmis.reporting.report.RnRInterfacingReport;
import org.jsi.elmis.service.interfaces.OfflineRnRService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class RnRInterfacingDataProvider extends ReportDataProvider {
    @Autowired
    private OfflineRnRService rnRService;
    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        RnRInterfacingReportParam rnRInterfacingReportParam=this.getReportFilterData(filter);
        List<RnRInterfacingReport> reportList= rnRService.loadRnRInterfacingStatus(rnRInterfacingReportParam);
        return null;
    }

    public RnRInterfacingReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, RnRInterfacingReportParam.class);
    }
    @Override
    public Map<String, Object> getExtraReportParams() {
        return null;
    }
}
