package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.model.NodeProductDateBalance;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.ProductStatusReportParam;
import org.jsi.elmis.reporting.report.ProductStatusReport;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class ProductStatusDataProvider extends ReportDataProvider {

    @Autowired
    TransactionService txnService;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        ProductStatusReportParam param = getReportFilterData(filter);
        setExtraReportParameters(param);
        Map<String , NodeProductDateBalance> npdbMap = txnService.getNodeProductBalancesOnDate(param.getDate(), param.getNodeId().intValue(), param.getProgramId().intValue());
        if(npdbMap != null){
            return convertToProductStatusReportItems(npdbMap);
        }
        return Collections.emptyList();
    }

    private void setExtraReportParameters(ProductStatusReportParam param){
        parameters.put("stockStatusDate", param.getDate());
        parameters.put(Constants.REPORT_NAME, "ProductStatusReport");
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        appendGeneralParams(parameters);
        return parameters;
    }

    public ProductStatusReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, ProductStatusReportParam.class);
    }

    private ArrayList<ProductStatusReport> convertToProductStatusReportItems( Map<String , NodeProductDateBalance> npdbMap){
        ArrayList<ProductStatusReport> productStatusReportItems = new ArrayList<>();
        npdbMap.forEach((productCode, nodeProductDateBalance)->{
            productStatusReportItems.add(new ProductStatusReport(productCode,
                    nodeProductDateBalance.getNodeProduct().getProgramProduct().getProduct().getPrimaryname(),
                    nodeProductDateBalance.getBalance().doubleValue(),
                    nodeProductDateBalance.getUsableBalance().doubleValue()));
        });
        return productStatusReportItems;
    }
}
