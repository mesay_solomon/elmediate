package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.PendingPhysicalCountParam;
import org.jsi.elmis.reporting.report.PendingPhysicalCountReport;
import org.jsi.elmis.service.interfaces.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PendingPhysicalCountDataProvider extends ReportDataProvider {



    @Autowired
    NodeService nodeService;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        PendingPhysicalCountParam param = getReportFilterData(filter);
        ArrayList<NodeProduct> nodeProducts = (ArrayList<NodeProduct>) nodeService.getNodeProductsPCNotCompletedFor(param.getPeriodId() ,
                param.getScheduleCode() ,  param.getProgramCode());



        ArrayList<PendingPhysicalCountReport> pendingPhysicalCountReports = new ArrayList<>();
        PendingPhysicalCountReport pendingPCNodeProduct;
        for(NodeProduct np : nodeProducts){

            pendingPCNodeProduct = new PendingPhysicalCountReport();
            pendingPCNodeProduct.setNodeName(np.getNode().getName());
            pendingPCNodeProduct.setProductCode(np.getProgramProduct().getProduct().getCode());
            pendingPCNodeProduct.setProductName(np.getProgramProduct().getProduct().getPrimaryname());

            pendingPhysicalCountReports.add(pendingPCNodeProduct);
        }

/*        pendingPhysicalCountReports.sort((ppcr1, ppcr2)->{
            int res = String.CASE_INSENSITIVE_ORDER.compare(ppcr1.getNodeName(), ppcr2.getNodeName());
            return (res != 0) ? res : ppcr1.getNodeName().compareTo(ppcr2.getNodeName());
        });

        pendingPhysicalCountReports.sort((ppcr1, ppcr2)->{
            int res = String.CASE_INSENSITIVE_ORDER.compare(ppcr1.getProductCode(), ppcr2.getProductCode());
            return (res != 0) ? res : ppcr1.getProductCode().compareTo(ppcr2.getProductCode());
        });*/

        Comparator<PendingPhysicalCountReport> comparator = Comparator.comparing(pendingPhysicalCountReport -> pendingPhysicalCountReport.getNodeName());
        comparator = comparator.thenComparing(pendingPhysicalCountReport -> pendingPhysicalCountReport.getProductCode());

        return pendingPhysicalCountReports.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        parameters.put(Constants.REPORT_NAME, "PendingPhysicalCounts");
        appendGeneralParams(parameters);
        return parameters;
    }

    public PendingPhysicalCountParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, PendingPhysicalCountParam.class);
    }
}
