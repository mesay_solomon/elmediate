package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.dao.GeographicZoneDAO;
import org.jsi.elmis.dao.mappers.GeographicZoneMapper;
import org.jsi.elmis.dao.mappers.ProcessingPeriodMapper;
import org.jsi.elmis.model.*;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.RnRInterfacingReportParam;
import org.jsi.elmis.reporting.report.RnRReport;
import org.jsi.elmis.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Service
public class RnrReportGenerationDataProvider extends ReportDataProvider {
    @Autowired
    private OfflineRnRService rnRService;
    @Autowired
    ProductService productService;
    @Autowired
    MiscellaneousService miscellaneousService;
    @Autowired
    AdjustmentService adjustmentService;
    private Facility facility;
    private ProcessingPeriod period;
    private GeographicZone district;
    private GeographicZone province;
    private  Program program;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        RnRInterfacingReportParam rnRInterfacingReportParam = this.getReportFilterData(filter);
        Requisition requisition = this.rnRService.loadOfflineRequisitionById(rnRInterfacingReportParam.getRnrId());
        this.period = miscellaneousService.getProcessingPeriod(requisition.getPeriodId());
        this.facility = miscellaneousService.getFacilityById(requisition.getFacilityId());
        this.district = miscellaneousService.getGeographicZoneById(this.facility.getGeographiczoneid());
        this.province = miscellaneousService.getGeographicZoneById(this.district.getParentid());
        this.program=miscellaneousService.getProgramById(requisition.getProgramId());
        List<RnRReport> rnRReportList = this.adaptRequistionToRnrReport(requisition);
        Comparator<RnRReport> comparator = Comparator.comparing(rnr -> rnr.getProductCategory());
        comparator = comparator.thenComparing(rnr -> rnr.getProductCode());
        rnRReportList.sort(comparator);
        return rnRReportList;
    }

    public RnRInterfacingReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, RnRInterfacingReportParam.class);
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        parameters.put(Constants.REPORT_NAME, "R&R Reporting");
        parameters.put(Constants.FACILITY,this.facility.getName());
        parameters.put(Constants.PROVINCE,this.province.getName());
        parameters.put(Constants.DISTRICT,this.district.getName());
        parameters.put(Constants.PERIOD,this.period.getName());
        parameters.put(Constants.PERIOD_STATRT,period.getStartdate());
        parameters.put(Constants.PERIOD_END,period.getEnddate());
        parameters.put(Constants.PROGRAM,program.getName());
        appendGeneralParams(parameters);
        return parameters;

    }

    public List<RnRReport> adaptRequistionToRnrReport(Requisition requisition) {
        List<RnRReport> rnRReportList = new ArrayList<>();
        List<LossAdjustmentType> adjustmentTypeList=adjustmentService.getAllAdjustments();
        requisition.getRequisitionLineItems().stream().forEach(l -> {
            if(l.getRliLossesNAdjustments()!=null && !l.getRliLossesNAdjustments().isEmpty()){
          l.getRliLossesNAdjustments().stream().forEach(la-> {
            LossAdjustmentType lossAdjustmentType=      adjustmentTypeList.stream().filter(a->
                            a.getName().trim().equalsIgnoreCase(la.getLossesAndAdjustmentType().trim())
                      ).findAny().orElse(null);
            la.setAdjustmentType(lossAdjustmentType);
                });
            }
            Product product = productService.getProductByCode(l.getProductCode());
            System.out.println("category id" + product.getCategoryid());
            ProductCategory productCategory = productService.getCategoryByProductCode(requisition.getProgramId(), l.getProductId());
            RnRReport rnRReport = new RnRReport();
            rnRReport.setProductCode(l.getProductCode());
            rnRReport.setProductId(l.getProductId());
            rnRReport.setBeginningBal(l.getBeginningBalance() != null ? l.getBeginningBalance().intValue() : 0);
            rnRReport.setDaysStockedOut(l.getStockOutDays());
            rnRReport.setDrugProduct(l.getProductName());
            rnRReport.setLossAdjustment(l.getTotalAdjustment() != null ? l.getTotalAdjustment().intValue() : 0);
            rnRReport.setExplnLossAdj(l.getRemarks());
            rnRReport.setMaxQty(l.getMaxStockQuantity() != null ? l.getMaxStockQuantity().intValue() : 0);
            rnRReport.setOrderQty(l.getCalculatedOrderQuantity() != null ? l.getCalculatedOrderQuantity().intValue() : 0);
            rnRReport.setPhyCount(l.getStockOnHand() != null ? l.getStockOnHand().intValue() : 0);
            rnRReport.setQtyDispensed(l.getQuantityDispensed() != null ? l.getQuantityDispensed().intValue() : 0);
            rnRReport.setTotalReceived(l.getQuantityReceived() != null ? l.getQuantityReceived().intValue() : 0);
            rnRReport.setUnit(l.getDispensingUnit());
            //rnRReport.setTypeTest(l.getTest);
            rnRReport.setAmc(l.getAmc() != null ? l.getAmc().doubleValue() : 0);
            rnRReport.setProductCategory(productCategory != null ? productCategory.getName() : "");
//            rnRReport.setCurrentPrice(l.getpri);
            rnRReport.setSkipped(l.getSkipped());
            rnRReportList.add(rnRReport);

        });
        return rnRReportList;
    }
}
