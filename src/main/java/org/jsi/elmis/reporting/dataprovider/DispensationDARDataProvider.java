package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.model.dto.ARVActivityRegister;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.DispensationDARReportParam;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class DispensationDARDataProvider extends ReportDataProvider {


    @Autowired
    ARVDispensingService arvDispensingService;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        DispensationDARReportParam param = getReportFilterData(filter);
        ARVActivityRegister arvActivityRegister = arvDispensingService.generateARVActivityRegister(param.getNodeId().intValue(), param.getFromDate(), param.getToDate());
        return arvActivityRegister.getActivityRegisterItems();
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        parameters.put(Constants.REPORT_NAME, "DailyActivityRegister");
        appendGeneralParams(parameters);
        return parameters;
    }

    public DispensationDARReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, DispensationDARReportParam.class);
    }
}
