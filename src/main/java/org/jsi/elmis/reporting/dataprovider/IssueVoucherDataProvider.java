package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.IssueVoucherParam;
import org.jsi.elmis.reporting.report.IssueVoucherReport;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class IssueVoucherDataProvider extends ReportDataProvider {



    @Autowired
    NodeService nodeService;

    @Autowired
    ReportingService reportingService;

    List<IssueVoucherReport> issueVoucherReportItems;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        IssueVoucherParam param = getReportFilterData(filter);
        issueVoucherReportItems = reportingService.getIssueVoucherItems(param.getTxnId());
        if(issueVoucherReportItems != null){
            return issueVoucherReportItems;
        }
        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        parameters.put(Constants.REPORT_NAME, "IssueVoucher");


        if(issueVoucherReportItems != null && issueVoucherReportItems.size() > 0){
            parameters.put(Constants.RECEIVED_BY, issueVoucherReportItems.get(0).getReceivedBy());
            parameters.put(Constants.RECEIVING_NODE, issueVoucherReportItems.get(0).getIssuedTo());
            parameters.put(Constants.ISSUED_BY, issueVoucherReportItems.get(0).getIssuedBy());
            parameters.put(Constants.CREATED_DATE, issueVoucherReportItems.get(0).getTxnDate());
        }



        appendGeneralParams(parameters);
        return parameters;
    }

    public IssueVoucherParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, IssueVoucherParam.class);
    }
}
