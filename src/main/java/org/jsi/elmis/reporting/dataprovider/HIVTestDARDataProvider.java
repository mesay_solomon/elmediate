package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.ProgramProductDAO;
import org.jsi.elmis.exceptions.UnavailableHIVTestProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.NodeProduct;
import org.jsi.elmis.model.NodeProductDateBalance;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.dto.HIVProductAvailabilityDTO;
import org.jsi.elmis.model.dto.HIVTestDARItem;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.HIVDARReportParam;
import org.jsi.elmis.reporting.report.HIVTestDARReport;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.jsi.elmis.rest.result.HIVTestDARResult;
import org.jsi.elmis.service.interfaces.HIVTestingService;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Component
public class HIVTestDARDataProvider extends ReportDataProvider {

    @Autowired
    HIVTestingService hivTesting;
    @Autowired
    MiscellaneousService miscellaneousService;
    @Autowired
    NodeService nodeService;
    @Autowired
    ProgramProductDAO ppDAO;
    @Autowired
    TransactionService txnService;

    HIVTestDARResult hivTestDarResult;
    FacilityDistrictAndProvinceResult myFacilityDistrictAndProvince;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {
        HIVDARReportParam param = getReportFilterData(filter);
        try {
            hivTestDarResult = hivTesting.generateHIVDAR(param.getFromDate(), param.getToDate(), param.getNodeId());
            setExtraReportParameters(param);
            return convertHivTestDarList(hivTestDarResult);
        } catch (UnavailableHIVTestProductException e) {
            e.printStackTrace();
        } catch (UnavailableNodeProductException e) {
            e.printStackTrace();
        } catch (UnavailablePhysicalCountException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private void setExtraReportParameters(HIVDARReportParam param){

        myFacilityDistrictAndProvince =  miscellaneousService.selectMyDistrictAndProvince();
        Node node = nodeService.getNodeById(param.getNodeId());
        HIVProductAvailabilityDTO screeningProductAvailability = hivTesting.checkAvailabilityOfProduct(ELMISConstants.SCREENING.getValue(), param.getNodeId());
        HIVProductAvailabilityDTO confirmatoryProductAvailability = hivTesting.checkAvailabilityOfProduct(ELMISConstants.CONFIRMATORY.getValue(), param.getNodeId());
        String screeningProductDispName = screeningProductAvailability.getProduct().getPrimaryname();
        String confirmatoryProductDispName = confirmatoryProductAvailability.getProduct().getPrimaryname();
        ProgramProduct screeningPP = ppDAO.getProgramProduct(screeningProductAvailability.getProduct().getId(), ELMISConstants.HIV.getValue());
        ProgramProduct confrimatoryPP = ppDAO.getProgramProduct(confirmatoryProductAvailability.getProduct().getId(), ELMISConstants.HIV.getValue());
        NodeProductDateBalance screeningProductBalance = null;
        NodeProduct screeningNP = nodeService.findProductByNodeAndProduct(param.getNodeId(), screeningPP.getId());
        if (screeningNP == null) {
            screeningProductBalance = new NodeProductDateBalance(BigDecimal.ZERO, BigDecimal.ZERO, null);
        }else {
            Integer nppId = screeningNP.getId();
            screeningProductBalance = txnService.getBalanceOnDate(param.getToDate(), nppId);
        }
        Integer screeningCalculatedBalance = screeningProductBalance.getBalance().intValue();
        NodeProductDateBalance confirmatoryProductBalance = null;
        NodeProduct confirmatoryNP = nodeService.findProductByNodeAndProduct(param.getNodeId(), confrimatoryPP.getId());
        if (confirmatoryNP == null) {
            confirmatoryProductBalance = new NodeProductDateBalance(BigDecimal.ZERO, BigDecimal.ZERO, null);
        }else {
            Integer nppId = confirmatoryNP.getId();
            confirmatoryProductBalance = txnService.getBalanceOnDate(param.getToDate(), nppId);
        }
        Integer confirmatoryCalculatedBalance = confirmatoryProductBalance.getBalance().intValue();
        parameters.put("quantity_rmkeceived_screening",0);
        parameters.put("district", myFacilityDistrictAndProvince.getDistrictName());
        parameters.put("facilityname", myFacilityDistrictAndProvince.getFacilityName());
        parameters.put("siteName", node.getName());
        parameters.put("screenDisplayName", screeningProductDispName);
        parameters.put("confirmatoryDisplayName", confirmatoryProductDispName);
        parameters.put("screenBalance", hivTestDarResult.getScreeningBeginningBalance() != null ? hivTestDarResult.getScreeningBeginningBalance().intValue() : 0);
        parameters.put("confirmatoryBalance", hivTestDarResult.getConfirmatoryProductBeginningBalance() != null ? hivTestDarResult.getConfirmatoryProductBeginningBalance().intValue() : 0);
        parameters.put("screeningAdjustment", hivTestDarResult.getScreeningProductTotalLnA() != null ? hivTestDarResult.getScreeningProductTotalLnA().intValue() : 0);
        parameters.put("confirmatoryAdjustment", hivTestDarResult.getConfirmatoryProductTotalLnA() != null ? hivTestDarResult.getConfirmatoryProductTotalLnA().intValue() : 0);
        parameters.put("screeningPhysicalCount", hivTestDarResult.getScreeningProductLatestPhysicalCount() != null ? hivTestDarResult.getScreeningProductLatestPhysicalCount().intValue() : 0);
        parameters.put("confirmatoryPhysicalCount", hivTestDarResult.getConfirmatoryProductLatestPhysicalCount() != null ? hivTestDarResult.getConfirmatoryProductLatestPhysicalCount().intValue() : 0);
        parameters.put("screeningQtyReceived", hivTestDarResult.getScreeningProductQtyReceived() != null ? hivTestDarResult.getScreeningProductQtyReceived() : BigDecimal.valueOf(0));
        parameters.put("confirmatoryQtyReceived", hivTestDarResult.getConfirmatoryProductQtyReceived() != null ? hivTestDarResult.getConfirmatoryProductQtyReceived() : BigDecimal.valueOf(0));
        parameters.put("screeningCalculatedBalance", screeningCalculatedBalance);
        parameters.put("confirmatoryCalculatedBalance", confirmatoryCalculatedBalance);
        parameters.put("darDate", param.getToDate());
        parameters.put(Constants.REPORT_NAME, "HIVTestDAR");
    }

    @Override
    public Map<String, Object> getExtraReportParams() {
        appendGeneralParams(parameters);
        return parameters;
    }

    public HIVDARReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, HIVDARReportParam.class);
    }

    public ArrayList<HIVTestDARReport> convertHivTestDarList(
            HIVTestDARResult hivTestDarResult) {

        ArrayList<HIVTestDARReport> hivTestDarList = new ArrayList<HIVTestDARReport>();
        HIVTestDARReport hivTestDar = null;

        for (HIVTestDARItem hivTestDarItem : hivTestDarResult.getDarItems()) {
            hivTestDar = new HIVTestDARReport();

            String confirmatoryResult ;
            String screeningResult ;
            String finalResult ;

            String confirmatoryResultKey = hivTestDarItem.getConfirmatoryResult();
            String screeningResultKey = hivTestDarItem.getScreeningResult();
            String finalResultKey = hivTestDarItem.getFinalResult();
            if(confirmatoryResultKey == null){
                confirmatoryResult = "";
            }else if(confirmatoryResultKey.equals(ELMISConstants.REACTIVE.getValue())){
                confirmatoryResult = "Reactive";
            }else if(confirmatoryResultKey.equals(ELMISConstants.NON_REACTIVE.getValue())){
                confirmatoryResult = "Non Reactive";
            }else{
                confirmatoryResult = "Invalid";
            }

            if(screeningResultKey == null){
                screeningResult = "";
            }else if(screeningResultKey.equals(ELMISConstants.REACTIVE.getValue())){
                screeningResult = "Reactive";
            }else if(screeningResultKey.equals(ELMISConstants.NON_REACTIVE.getValue())){
                screeningResult = "Non Reactive";
            }else{
                screeningResult = "Invalid";
            }

            if(hivTestDarItem.getPurpose().equals(ELMISConstants.QUALITY_CONTROL.getValue())
                    || hivTestDarItem.getPurpose().equals(ELMISConstants.OTHER.getValue())){
                finalResult = "";
            }else{
                if(finalResultKey == null){
                    finalResult = "";
                }else if(finalResultKey.equals(ELMISConstants.REACTIVE.getValue())){
                    finalResult = "Reactive";
                    hivTestDar.setPositiveResult("X");
                }else if(finalResultKey.equals(ELMISConstants.NON_REACTIVE.getValue())){
                    finalResult = "Non Reactive";
                    hivTestDar.setNegativeResult("X");
                }else if(finalResultKey.equals(ELMISConstants.INVALID.getValue())){
                    finalResult = "";
                }else{
                    hivTestDar.setIndeterminateResult("X");
                    finalResult = "Invalid";
                }
            }

            String clientNo = hivTestDarItem.getClientNo();
            if(clientNo != null){
                hivTestDar.setClient_id(clientNo);
                hivTestDar.setTestId(clientNo);
            }

            hivTestDar.setConfirmatoryResult(confirmatoryResult);
            hivTestDar.setScreeningResult(screeningResult);
            hivTestDar.setFinalResult(finalResult);
            hivTestDar.setTestDate(hivTestDarItem.getConfirmatoryTime());
            hivTestDar.setPurpose(hivTestDarItem.getPurpose());

            hivTestDarList.add(hivTestDar);

        }

        return hivTestDarList;
    }
}
