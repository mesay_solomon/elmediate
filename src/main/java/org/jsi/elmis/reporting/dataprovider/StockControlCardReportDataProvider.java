package org.jsi.elmis.reporting.dataprovider;

import org.jsi.elmis.dao.FacilityDAO;
import org.jsi.elmis.dao.ProgramProductDAO;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.ProgramProduct;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.reporting.Constants;
import org.jsi.elmis.reporting.ReportDataProvider;
import org.jsi.elmis.reporting.ResultRow;
import org.jsi.elmis.reporting.helper.ParameterAdaptor;
import org.jsi.elmis.reporting.param.StockControlCardReportParam;
import org.jsi.elmis.reporting.report.StockControlCardReport;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.jsi.elmis.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class StockControlCardReportDataProvider extends ReportDataProvider{

    @Autowired
    StoreManagementService storeMgmt;

    @Autowired
    AdjustmentService adjService;

    @Autowired
    ProgramProductDAO programProductDAO;

    @Autowired
    MiscellaneousService miscellaneousService;

    @Override
    public List<? extends ResultRow> getReportBody(Map<String, String[]> filter, Map<String, String[]> sorter, int page, int pageSize) {

        StockControlCardReportParam param = getReportFilterData(filter);
        setExtraReportParams(param);
        Node n = new Node();
        n.setId(param.getNodeId().intValue());
        List<StockControlCard> sccList = storeMgmt.generateStockControlCard(n , param.getFromDate(), param.getToDate() , param.getPpbId().intValue());
        return convertToStockControlCardReport(sccList);
    }

    public StockControlCardReportParam getReportFilterData(Map<String, String[]> filterCriteria) {
        return ParameterAdaptor.parse(filterCriteria, StockControlCardReportParam.class);
    }


    private List<StockControlCardReport> convertToStockControlCardReport(List<StockControlCard> sccList){
        List<StockControlCardReport> stockControlCardReports = new ArrayList<>();
        StockControlCardReport stockControlReport = null;
        for(StockControlCard scc : sccList){
            stockControlReport = new StockControlCardReport(scc.getDate(),
                    scc.getRefNo(), getFromOrToDescription(scc.getFromOrTo()), scc.getQtyReceived(), scc.getQtyIssued(),
                    scc.getLossesNAdjustments(), scc.getBalance(),scc.getRemark(), scc.getUser());
            stockControlCardReports.add(stockControlReport);
        }
        return stockControlCardReports;
    }


    private void setExtraReportParams(StockControlCardReportParam param){
        ProgramProduct pp = programProductDAO.getProgramProduct(param.getPpbId().intValue());
        FacilityDistrictAndProvinceResult facilityDistrictAndProvinceResult = miscellaneousService.selectMyDistrictAndProvince();
        parameters.put("primaryname", pp.getProduct().getPrimaryname());
        parameters.put("packsize", pp.getProduct().getPacksize());
        parameters.put("facilityname", facilityDistrictAndProvinceResult.getFacilityName());
        parameters.put("productcode", pp.getProduct().getCode());
        parameters.put("generalStrength", pp.getProduct().getStrength());
        parameters.put("district", facilityDistrictAndProvinceResult.getDistrictName());
        parameters.put(Constants.REPORT_NAME, "StockControlCard");
    }

    public Map<String, Object> getExtraReportParams(){
        appendGeneralParams(parameters);
        return parameters;
    }

    public String getFromOrToDescription(String fromOrTo){
        List<LossAdjustmentType> filteredList = adjService.getLossAdjustmentTypes()
                .stream().filter(lat -> lat.getName().equalsIgnoreCase(fromOrTo)).collect(Collectors.toList());
        return filteredList.isEmpty() ? fromOrTo : filteredList.get(0).getDescription();
    }

}
