package org.jsi.elmis.reporting;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 *  Defines API for exporting reports.
 *  Implementation can be done for different Java reporting frameworks.(Jasper, BIRT)
 */
public interface IReportExporter {
    /**
     *
     * @param reportInputStream -   <b>The report being exported</b>
     * @param reportExtraParams  -  <b>Extra report parameters that can be passed to the report to fill report header and footer details</b>
     * @param reportData   - <b>DataSource used to fill the report</b>
     * @param outputOption  -   <b>Report out put option </b>
     * @param response - <b>HttpServletResponse for writing the report to OutputStream</b>
     */
    void exportReport(InputStream reportInputStream, Map<String, Object> reportExtraParams, List<? extends ResultRow> reportData, ReportOutputOption outputOption, HttpServletResponse response);
}
