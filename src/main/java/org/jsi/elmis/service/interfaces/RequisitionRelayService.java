package org.jsi.elmis.service.interfaces;

import org.jsi.elmis.model.criteria.RequisitionSearchCriteria;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rest.result.RestResponse;
import org.jsi.elmis.rnr.RnR;
import org.springframework.http.ResponseEntity;

public interface RequisitionRelayService {
    ResponseEntity<RestResponse> submitSdpReport(RnR report, Long userId);
    ResponseEntity<RestResponse> initiateRnr(Integer facilityId, Integer programId, Boolean emergency, Integer periodId, String sourceApplication);
    ResponseEntity<RestResponse> searchRnrs(RequisitionSearchRequest requisitionSearchRequest);
    ResponseEntity<RestResponse> getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(RequisitionSearchCriteria criteria);
}
