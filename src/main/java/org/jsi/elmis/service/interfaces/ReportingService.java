package org.jsi.elmis.service.interfaces;

import org.jsi.elmis.reporting.report.IssueVoucherReport;

import java.util.List;

public interface ReportingService {
    public List<IssueVoucherReport> getIssueVoucherItems(Integer txnId);
}
