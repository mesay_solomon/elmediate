package org.jsi.elmis.service.interfaces;

import org.springframework.context.MessageSource;

import java.util.Locale;
import java.util.Map;

/**
 * Extends {@link org.springframework.context.MessageSource} interface to provide a way to get all key/message pairs
 * known.
 */
public interface ExposedMessageSource extends MessageSource {
    /**
     * Returns all key/message pairs for the given localeCode.
     * @param locale the desired localeCode of the messages.
     * @return a map of all key/message pairs for the given localeCode.
     */
    public Map<String,String> getAll(Locale locale);
}
