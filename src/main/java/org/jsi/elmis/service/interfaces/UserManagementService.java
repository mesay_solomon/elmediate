/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.Right;
import org.jsi.elmis.model.Role;
import org.jsi.elmis.model.RoleRight;
import org.jsi.elmis.model.User;
import org.jsi.elmis.model.UserNodeRole;
import org.jsi.elmis.rest.result.UserNodeRoleRightResult;

/**
 * @author mesay
 *
 */
public interface UserManagementService {

	public List<User> getUsers();
	
	public Integer saveUserInfo(User user);
	
	public Integer updateUser(User user);

	public Integer updateUserPassword(User user);
	
	public User selectUserById(int id);
	
	public User selectUserByEmail(String email);
	
	public Integer assignUserNodeRoles(User user , Node node , List<Role> nodeRoles);
	
	public Integer saveRole(Role role);
	
	public List<Role> getRoles();
	
	public Integer updateRole(Role role);
	
	public Integer saveRoleRight(RoleRight roleRight);
	
	public List<RoleRight> getRoleRights();
	
	public List<RoleRight> getRoleRightsByRoleId(Integer roleId);
	
	public Integer updateRoleRights(RoleRight roleRight);
	
	public Integer updateUserNodeRoles( User user , Node node , List<Role> nodeRoles, int userNodeRoleId);
	
	public List<Right> getRights();

	public Integer saveRoleRights(List<RoleRight> roleRights);

	public Integer deleteUser(int userId);
	
	public Integer deleteRole(int roleId);

	public Boolean insertUserNodeRoles(List<UserNodeRole> userNodeRoles);
	
	public ArrayList<UserNodeRoleRightResult> authenticate(User user) throws Exception;

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserId(
			Integer userId);
	
	public Boolean revokeRightsFromRole(Integer roleId, ArrayList<String> rightList);
	
	public Boolean revokeRolesFromUser(ArrayList<UserNodeRole> userNodeRoles);
	
	public Boolean revokeNodesFromUser(ArrayList<UserNodeRole> userNodeRoles);

	public ArrayList<UserNodeRoleRightResult> getUserNodeRoleRightsByUserAndNode(
			UserNodeRole unr);

	public Boolean isUserAuthenticatedOnCentralELMIS(User user) throws Exception;

	public List<User> getUsersV2();

	User getUserV2(Long userId);

	Role getRoleById(Integer roleId);
}
