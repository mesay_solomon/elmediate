/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.interfaces;

import java.util.List;
import java.util.Map;

import org.jsi.elmis.model.OfflineRnRStatus;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.reporting.param.RnRInterfacingReportParam;
import org.jsi.elmis.reporting.report.RnRInterfacingReport;

public interface OfflineRnRService {

	public Integer saveRnR(Requisition rnr);
	
	public Integer updateRnR(Requisition rnr);

	public List<Requisition> getOfflineRequisitionsByPeriodId(Integer periodId);
	
	public List<Requisition> getOfflineRequisitionsByStatus(String status);
	
	public Integer deleteRnR(Integer id);

	public Integer saveOfflineRnRStatus(OfflineRnRStatus offlineRnRStatus);

	public OfflineRnRStatus getOfflineRnRStatusByRequisitionId(Integer requisitionId);
	public Map<String, List> getOfflineRequisitionsByFacilityProgram(Integer program, Integer facility,Boolean emergency) ;
	public List<ProcessingPeriod> loadFacilityProgramPeriods(Integer program, Integer facility);
	List<RnRInterfacingReport> loadRnRInterfacingStatus(RnRInterfacingReportParam rnRInterfacingReportParam);
	public ProcessingPeriod loadPreviosPeriod(Integer period);
	public  Requisition loadOfflineRequisitionById(Integer rnrId);
	public  void deleteTempRequisitions();

    ProcessingPeriod getPeriodInfo(ProcessingPeriod processingPeriod);
}
