/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.result.StockRequisitionResult;

/**
 * @author Mesay S. Taye
 *
 */
public interface StoreManagementService {

	public int issue(Node store, Node dispensingPoint,
			List<TransactionProduct> productsIssued, Date txnDate,  Integer stockRequestId  , Integer userId , ActionOrder actionOrder, String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException;


	public int receive(Node store , List<TransactionProduct> productsReceived , Date txnDate ,
			Integer userId , String dispatchNo , Integer sourceId , ActionOrder actionOrder, String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException;

    public int receiveProducts(Node store, List<TransactionProduct> productsReceived,
                               Date txnDate, Integer userId, String dispatchNo, Integer nodeId,
                               ActionOrder actionOrder, String remarks, Boolean batchAware) throws Exception;
	
	public  List<StockControlCard> generateStockControlCard(Node node , Date from , Date to , Integer ppbId);

	public int issueToOtherFacility(Node store,
			List<TransactionProduct> productsIssued, Date txnDate,
			Integer userId , Facility facility, ActionOrder actionOrder, Boolean batchAware) throws UnavailableNodeProductException,
			InsufficientNodeProductException;
	
	public int requestProducts(Integer requesterNodeId , Integer supplierNodeId , List<StockRequistionItem> stockRequistionItems , Date requestTime , Integer userId,Integer pathId);

	public int updateStockRequisitionStatus(Integer stockRequisitionId, String status);
	
	public List<StockRequisitionResult> getStockRequestsBySupplierId(Integer supplierNodeId, Date afterTime);

	public List<StockControlCard> selectMostRecentPhysicalCount(Integer nodeId, Date from,
			Date to, Integer ppbId);
 
	public BigDecimal getTotalLnA(Node node, Date from, Date to, Integer ppbId);
	
	public BigDecimal getTotalProductReceipts(Node node, Date from, Date to,
			Integer ppbId);

	public List<BigDecimal> selectMostRecentPC(Integer nodeId, Date from, Date to,
			Integer ppbId);

	public Boolean pcToBeDoneOnProdInPeriod(Integer nodeId,
											Date from, Date to, Integer ppId);

	public BigDecimal getFacilityPhysicalCount(Integer periodId, Integer productId)
			throws UnavailablePhysicalCountException;

	public BigDecimal getBeginningBalance(Integer periodId, Integer ppbId);

	public int issueToConsumptionPoint(Node supplier, Node recipient,
			List<TransactionProduct> productsIssued, Date txnDate,
			Integer stockRequestId, Integer userId, ActionOrder actionOrder, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException;


	public Integer saveStockTransferTransaction(StockTransferTransaction stockTransferTransaction);

} 
