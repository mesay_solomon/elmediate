package org.jsi.elmis.service.interfaces;

import org.jsi.elmis.model.FEUpdateInfo;

import java.util.List;

public interface FEUpdateInfoService {
    void insert(FEUpdateInfo feUpdateInfo);
    List<FEUpdateInfo> findAll();
    List<FEUpdateInfo> findByFacilityCode(String facilityCode);
}
