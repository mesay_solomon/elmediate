/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import org.jsi.elmis.backup.BackupFactory;
import org.jsi.elmis.backup.IDatabaseBackup;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.Connectivity;
import org.jsi.elmis.dao.*;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.interfacing.openlmis.BaseFactory;
import org.jsi.elmis.interfacing.openlmis.LookupFactory;
import org.jsi.elmis.interfacing.openlmis.RnRSubmissionResult;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.model.hub.rnr.HubRnR;
import org.jsi.elmis.reporting.param.RnRInterfacingReportParam;
import org.jsi.elmis.reporting.report.RnRInterfacingReport;
import org.jsi.elmis.rest.client.WSClientInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rest.request.RequisitionSearchCriteria;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rest.request.RnrReportIntrimRequest;
import org.jsi.elmis.rnr.LossesAndAdjustments;
import org.jsi.elmis.rnr.RegimenLineItem;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.rnr.RnRLineItem;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import us.monoid.json.JSONException;

/**
 * @author Mesay S. Taye
 */
@Component
public class DefaultRnRService implements RnRService {

    @Autowired
    StoreManagementService storeService;
    @Autowired
    TransactionService txnService;
    @Autowired
    MiscellaneousService miscService;
    @Autowired
    ProductDAO productDAO;
    @Autowired
    ProgramDAO programDAO;
    @Autowired
    NodeDAO nodeDAO;
    @Autowired
    ProductPeriodStatusDAO prodPeriodStatusDAO;
    @Autowired
    AdjustmentDAO adjDAO;
    @Autowired
    CommonDAO commonDAO;
    @Autowired
    RnRDAO rnrDAO;
    @Autowired
    ProcessingPeriodDAO periodDAO;
    @Autowired
    RegimenDAO regimenDAO;
    @Autowired
    BaseFactory baseFactory;
    @Autowired
    ProgramProductDAO ppDAO;
    @Autowired
    RequisitionAdapter requisitionAdapter;
    @Autowired
    PropertiesService propServ;
	@Autowired
	ELMISJDBC elmisjdbc;

    @Autowired
    WSClientInterfacing wsClientInterfacing;

    private IDatabaseBackup databaseBackup;

    private String facilityCode;



    @Value("${adjustment.computer.generated.positive}")
    private String computerGeneratedPositiveAdjustment;
    @Value("${adjustment.computer.generated.negative}")
    private String computerGeneratedNegativeAdjustment;

    @Value("${elmis.rnr.send.to.hub}")
    private Boolean sendRnRToHub;

    private static final String ARV = "ARV";
    @Autowired
    LookupFactory lf;

    public DefaultRnRService() {

    }
    @Autowired
    public DefaultRnRService(BackupFactory backupFactory) {
        this.databaseBackup = backupFactory.createDatabaseBackupInstance();
    }

    @Override
    public RnR generateRnR(Integer periodId, String programCode, Boolean emergency, String scheduleCode) {
        facilityCode = propServ.getProperty("facility.code",false).getValue();
        List<ProgramProduct> programProducts = productDAO.getFacilityApprovedProductsInProgram(programCode, facilityCode);

        RnR rnr = new RnR();

        rnr.setAgentCode(facilityCode);
        periodId = (emergency) ? commonDAO.getCurrentPeriod(scheduleCode).getId() : commonDAO.getPreviousPeriod(scheduleCode).getId();
        rnr.setPeriodId(periodId);
        rnr.setApproverName(propServ.getProperty("elmis.central.approver.name", false).getValue());
        rnr.setProgramCode(programCode);
        rnr.setEmergency(emergency);
        rnr.setProducts(new ArrayList<RnRLineItem>());
        for (ProgramProduct programProduct : programProducts) {
            ProductPeriodStatus ppStatus = prodPeriodStatusDAO.getProductPeriodStatus(periodId, programProduct.getProductid());
            if (ppStatus != null) {
                RnRLineItem rnrLineItem = new RnRLineItem();
                rnrLineItem.setProductCode(programProduct.getProduct().getCode());

                rnrLineItem.setBeginningBalance(ppStatus.getBeginningBalance().intValue());
                rnrLineItem.setQuantityReceived(ppStatus.getQuantityReceived().intValue());
//				rnrLineItem.setQuantityDispensed(ppStatus.getQuantityDispensed().intValue());
                rnrLineItem.setQuantityDispensed((int) Math.ceil(ppStatus.getQuantityDispensed().doubleValue()));
                rnrLineItem.setStockInHand(ppStatus.getStockOnHand().intValue());
                rnrLineItem.setStockOutDays(ppStatus.getStockOutDays());
                rnrLineItem.setAmc(calculateAMC(programProduct.getProductid(), ELMISConstants.AMC_NUMBER_OF_MONTHS, scheduleCode).intValue());
                rnrLineItem.setMaxStockQuantity(ELMISConstants.AMC_NUMBER_OF_MONTHS * rnrLineItem.getAmc());
                Integer qtyRequested = rnrLineItem.getMaxStockQuantity() - rnrLineItem.getStockInHand();
                rnrLineItem.setQuantityRequested(qtyRequested = (qtyRequested >= 0) ? qtyRequested : 0);
                rnrLineItem.setQuantityApproved(rnrLineItem.getQuantityRequested());
                rnrLineItem.setReasonForRequestedQuantity("R&R");
                rnrLineItem.setDispensingUnit(programProduct.getProduct().getDispensingunit());

                rnrLineItem.setNewPatientCount(0);

                List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<LossesAndAdjustments>();
                List<ProductPeriodStatusAdjustment> productPeriodAdjustments = prodPeriodStatusDAO.getProductAdjustmentsForPeriod(ppStatus.getId());

                for (ProductPeriodStatusAdjustment productPeriodStatusAdjustment : productPeriodAdjustments) {
                    LossesAndAdjustments lna = new LossesAndAdjustments();
                    lna.setType(adjDAO.findAdjustmentByName(productPeriodStatusAdjustment.getAdjustmentType()));
                    lna.setQuantity(productPeriodStatusAdjustment.getQuantity().intValue());
                    lossesAndAdjustments.add(lna);
                }
                rnrLineItem.setLossesAndAdjustments(lossesAndAdjustments);
                normalizeRnRLineItem(rnrLineItem);
                rnr.getProducts().add(rnrLineItem);
            }
        }
        return rnr;
    }

    @Override
    public RnR generateRnROnTheFly(Integer periodId, String programCode, Boolean emergency, String scheduleCode) throws Exception {
        facilityCode = propServ.getProperty("facility.code",false).getValue();
        String installationType = propServ.getProperty("installation.type",false).getValue();
        String sourceApplication = installationType.equalsIgnoreCase("DM")? "eLMIS_DM":"eLMIS_FE";
        Long a = System.currentTimeMillis();
        List<FacilityApprovedProduct> programProducts = productDAO.getFacilityApprovedProducts(programCode, facilityCode);
        elmisjdbc.refreshMaterializedView("vw_action_timeline");
        elmisjdbc.refreshMaterializedView("vw_transactions_for_rnr");

        RnR rnr = new RnR();

        rnr.setAgentCode(facilityCode);
        ProcessingPeriod period = null;
        if (periodId == null) {
            period = (emergency) ? commonDAO.getCurrentPeriod(scheduleCode) : commonDAO.getPreviousPeriod(scheduleCode);
        } else {
            period = miscService.getProcessingPeriod(periodId);
        }
        if (period == null) {
            throw new Exception("Period Unavailable");
        }
        rnr.setPeriodId(period.getId());
        rnr.setApproverName(propServ.getProperty("elmis.central.approver.name", false).getValue());
        rnr.setProgramCode(programCode);
        rnr.setEmergency(emergency);
        rnr.setProducts(new ArrayList<RnRLineItem>());
        rnr.setSourceApplication(sourceApplication);
        if (programCode.equals("ARV")) {
            rnr.setRegimens(getBlankRegimenLineItems());
        }
        for (FacilityApprovedProduct facilityApprovedProduct : programProducts) {
            ProgramProduct programProduct = facilityApprovedProduct.getProgramProduct();
            ProgramProduct pp = ppDAO.getProgramProduct(programProduct.getProductid(), programCode);
            ProductPeriodStatus ppStatus = buildProductPeriodStatus(pp, periodId);
            if (ppStatus != null) {
                RnRLineItem rnrLineItem = new RnRLineItem();
                rnrLineItem.setProductCode(programProduct.getProduct().getCode());
                rnrLineItem.setProductId(programProduct.getProduct().getId());
                rnrLineItem.setProductName(programProduct.getProduct().getPrimaryname());


                rnrLineItem.setBeginningBalance(ppStatus.getBeginningBalance().intValue());
                rnrLineItem.setQuantityReceived(ppStatus.getQuantityReceived().intValue());
                rnrLineItem.setQuantityDispensed((int) Math.ceil(ppStatus.getQuantityDispensed().doubleValue()));
                rnrLineItem.setStockInHand(ppStatus.getStockOnHand().intValue());
                rnrLineItem.setAmc(calculateAMCOnTheFly(programProduct.getId(), periodId, ELMISConstants.AMC_NUMBER_OF_MONTHS, scheduleCode, ppStatus.getQuantityDispensed()).intValue());
                rnrLineItem.setMaxStockQuantity(facilityApprovedProduct.getMaxmonthsofstock() == null ?
                        ELMISConstants.AMC_NUMBER_OF_MONTHS : facilityApprovedProduct.getMaxmonthsofstock() * rnrLineItem.getAmc());
                Integer qtyRequested = rnrLineItem.getMaxStockQuantity() - rnrLineItem.getStockInHand();
                rnrLineItem.setQuantityRequested(qtyRequested = (qtyRequested >= 0) ? qtyRequested : 0);
                rnrLineItem.setCalculatedOrderQuantity(qtyRequested = (qtyRequested >= 0) ? qtyRequested : 0);
                rnrLineItem.setQuantityApproved(rnrLineItem.getQuantityRequested());
                rnrLineItem.setReasonForRequestedQuantity("SDP submission!");
                rnrLineItem.setFullSupply(programProduct.getProduct().getFullsupply());
                rnrLineItem.setDispensingUnit(programProduct.getProduct().getDispensingunit());
                rnrLineItem.setMaxMonthsOfStock((double) facilityApprovedProduct.getMaxmonthsofstock());
                rnrLineItem.setPackSize((int) programProduct.getProduct().getPacksize());
                rnrLineItem.setDosesPerMonth(programProduct.getDosespermonth());
                rnrLineItem.setDosesPerDispensingUnit((int) programProduct.getProduct().getDosesperdispensingunit());

                rnrLineItem.setNewPatientCount(0);

                List<LossesAndAdjustments> lossesAndAdjustments = new ArrayList<LossesAndAdjustments>();
                List<ProductPeriodStatusAdjustment> productPeriodAdjustments = prodPeriodStatusDAO.getFacilityStockAdjustments(ppStatus.getPpbId(), period.getStartdate(), period.getEnddate());

                for (ProductPeriodStatusAdjustment productPeriodStatusAdjustment : productPeriodAdjustments) {
                    LossesAndAdjustments lna = new LossesAndAdjustments();
                    lna.setType(adjDAO.findAdjustmentByName(productPeriodStatusAdjustment.getAdjustmentType()));
                    lna.setQuantity(productPeriodStatusAdjustment.getQuantity().intValue());
                    lossesAndAdjustments.add(lna);
                }
                rnrLineItem.setLossesAndAdjustments(lossesAndAdjustments);
                rnrLineItem.setStockOutDays(calcuateStockOutDays(rnrLineItem, period, programProduct.getProduct().getId()));
                normalizeRnRLineItem(rnrLineItem);
                rnr.getProducts().add(rnrLineItem);
            }
        }

        Long b = System.currentTimeMillis();
        System.out.println("elapsed time on R&R generation =" + (b - a));
        return rnr;
    }

    private Boolean productIsActive(RnRLineItem lineItem) {
        if (lineItem.getBeginningBalance() == 0 &&
                lineItem.getQuantityReceived() == 0 &&
                lineItem.getQuantityRequested() == 0 &&
                (lineItem.getLossesAndAdjustments() == null || lineItem.getLossesAndAdjustments().isEmpty()) &&
                lineItem.getQuantityDispensed() == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void skipNonActiveLineItems(RnR rnr) {
        for (Iterator<RnRLineItem> iterator = rnr.getProducts().iterator(); iterator.hasNext(); ) {
            RnRLineItem item = iterator.next();
            if (!productIsActive(item)) {
                iterator.remove();
            }
        }
    }

    private RnRLineItem normalizeRnRLineItem(RnRLineItem rnrLineItem) {
        Integer totalLnA = 0;
        for (LossesAndAdjustments lna : rnrLineItem.getLossesAndAdjustments()) {
            totalLnA = (lna.getType().getAdditive()) ? totalLnA + lna.getQuantity() : totalLnA - lna.getQuantity();
        }

        Integer discrepancy = rnrLineItem.getBeginningBalance()
                + rnrLineItem.getQuantityReceived()
                + totalLnA
                - rnrLineItem.getQuantityDispensed()
                - rnrLineItem.getStockInHand();

        if (discrepancy > 0) {
            LossesAndAdjustments generatedLnA = new LossesAndAdjustments();
            generatedLnA.setQuantity(Math.abs(discrepancy));
            generatedLnA.setType(adjDAO.findAdjustmentByName(computerGeneratedNegativeAdjustment));
            if (generatedLnA.getType() == null) {
                rnrLineItem.setQuantityDispensed(rnrLineItem.getQuantityDispensed() + discrepancy);
            } else {
                //	rnrLineItem.getLossesAndAdjustments().add(generatedLnA);
                addLnA(rnrLineItem.getLossesAndAdjustments(), generatedLnA);
            }
        } else if (discrepancy < 0) {
            LossesAndAdjustments generatedLnA = new LossesAndAdjustments();
            generatedLnA.setQuantity(Math.abs(discrepancy));
            generatedLnA.setType(adjDAO.findAdjustmentByName(computerGeneratedPositiveAdjustment));
            if (generatedLnA.getType() == null) {
                rnrLineItem.setStockInHand(rnrLineItem.getStockInHand() - discrepancy);
            } else {
                //	rnrLineItem.getLossesAndAdjustments().add(generatedLnA);
                addLnA(rnrLineItem.getLossesAndAdjustments(), generatedLnA);
            }
        }
        return rnrLineItem;
    }

    private List<LossesAndAdjustments> addLnA(List<LossesAndAdjustments> lnas, LossesAndAdjustments lna) {
        for (LossesAndAdjustments currentLnA : lnas) {
            if (currentLnA.getType().equals(lna.getType())) {
                currentLnA.setQuantity(currentLnA.getQuantity() + lna.getQuantity());
                return lnas;
            }
        }
        lnas.add(lna);
        return lnas;
    }

    private BigDecimal calculateAMC(Integer productId, Integer noOfMonths, String scheduleCode) {
        List<ProductPeriodStatus> ppStatuses = prodPeriodStatusDAO.getPreviousConsumptions(productId, scheduleCode);
        BigDecimal consumptionSum = BigDecimal.ZERO;
        int i;
        for (i = 0; i < noOfMonths && i < ppStatuses.size(); i++) {
            consumptionSum = consumptionSum.add(ppStatuses.get(ppStatuses.size() - 1 - i).getQuantityDispensed());
        }
        if (i == 0) return BigDecimal.ZERO;
        return BigDecimal.valueOf(consumptionSum.doubleValue() / i);
    }

    private BigDecimal calculateAMCOnTheFly(Integer ppbId, Integer periodId, Integer noOfPeriods, String scheduleCode, BigDecimal currentPeriodConsumption) {
        return txnService.getAveragePeriodicConsumption(ppbId, periodId, scheduleCode, noOfPeriods, currentPeriodConsumption);
    }

    public Integer calcuateStockOutDays(RnRLineItem lineItem, ProcessingPeriod period, Integer productId) {
        return 0;
//		if(!productIsActive( lineItem)) return DateCustomUtil.dateDiff(period.getStartdate(), period.getEnddate());
/*		try{

		if(!productIsActive( lineItem)) return 0;

		Date curSOPeriodStart = null;
		Date curSOPeriodEnd = null;
		Boolean soPeriodRecording = false;

		if(lineItem.getBeginningBalance() == 0 ){
			curSOPeriodStart = period.getStartdate();
			soPeriodRecording = true;
		}

		Integer stockOutDays = 0;
		List<TransactionHistoryDTO> txnHistoryItems = txnService.selectTransactionHistory(null, productId, period.getStartdate());

		for(int i = txnHistoryItems.size() - 1  ; i >= 0  && txnHistoryItems.get(i).getTxnTimestamp().before(period.getEnddate()) ; i--){
			if(soPeriodRecording)
			{
				if(txnHistoryItems.get(i).getQtyAfterTxn().compareTo(BigDecimal.ZERO) > 0){
					curSOPeriodEnd = txnHistoryItems.get(i).getTxnTimestamp();
					soPeriodRecording = false;
					stockOutDays += DateCustomUtil.dateDiff(curSOPeriodStart, curSOPeriodEnd);
				}
			} else {
				if(txnHistoryItems.get(i).getQtyAfterTxn().compareTo(BigDecimal.ZERO) == 0){
					curSOPeriodStart = txnHistoryItems.get(i).getTxnTimestamp();
					soPeriodRecording = true;
				}
			}
		}

		if(soPeriodRecording){
			stockOutDays += DateCustomUtil.dateDiff(curSOPeriodStart, period.getEnddate());
		}
				return stockOutDays;
		} catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}*/
    }

    @Override
    public RnRSubmissionResult submitRnR(RnR rnr) {

        Boolean useAuthToken = !(propServ.getProperty("installation.type", false).getValue().equalsIgnoreCase("MEDIATOR"));
        //update physical counts
        RnrReportIntrimRequest reportIntrimRequest= new RnrReportIntrimRequest();
        if( rnr.getProgramCode().equals(ELMISConstants.ARV.getValue()) && sendRnRToHub ){
            if(!Connectivity.isReachable(propServ.getProperty("elmis.hub.url", false).getValue()))
                return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
        } else {
            if(!Connectivity.isReachable(propServ.getProperty("elmis.central.url", false).getValue())){
                return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
            }
        }

        Gson gson = new Gson();
        skipNonActiveLineItems(rnr);
        reportIntrimRequest.setValue(rnr);
        String rnrJSON = gson.toJson(rnr);
        Boolean isSuccessful = false;
        String errorMsg = null;
        System.out.println( "json is "+ rnrJSON);

/*
		if(!sendRnRToHub){
			ArrayList<Program> programs = lf.getPrograms();
		}
		*/
        String programCode=rnr.getProgramCode();
        try {
            if (sendRnRToHub && programCode.equals(ARV)) {

                isSuccessful = (Boolean) baseFactory.uploadJSON(propServ.getProperty("elmis.hub.url", false).getValue(),ELMISConstants.SERVICE_SDP_REQUISITION.getValue(), "R&R", rnrJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            } else {

                isSuccessful = (Boolean) baseFactory.uploadJSON(propServ.getProperty("elmis.central.url", false).getValue(),ELMISConstants.SERVICE_SDP_REQUISITION.getValue(), "R&R", rnrJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            }

        }catch(JSONException ex){
            if(ex.getMessage().contains("must begin with")){
                errorMsg = "User doesn't have permission to submit R&R.";
            }
            try {
                reportIntrimRequest.setErrorDetail(errorMsg);
                String rnrRequestJSON=gson.toJson(reportIntrimRequest);
                baseFactory.uploadJSON(propServ.getProperty("elmis.intrim.rnr.server.url", false).getValue(), ELMISConstants.SERVICE_SDP_INTRIM_REQUISITION.getValue(), "R&R", rnrRequestJSON, Long.class, rnr.getProgramCode(),useAuthToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {

            e.printStackTrace();
            errorMsg = e.getMessage();
            if(e.getMessage().contains("full authentication required")){
                errorMsg = "User doesn't have permission to submit R&R.";
            }
            try {
                reportIntrimRequest.setErrorDetail(errorMsg);
                String rnrRequestJSON=gson.toJson(reportIntrimRequest);
                baseFactory.uploadJSON(propServ.getProperty("elmis.intrim.rnr.server.url", false).getValue(),ELMISConstants.SERVICE_SDP_INTRIM_REQUISITION.getValue(), "R&R", rnrRequestJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        if(isSuccessful){

            rnrDAO.saveRnR(new ReportAndRequisition(null, rnr.getPeriodId() , ReportAndRequisition.SUBMITTED, "R&R successfully submitted!" ,
                    programDAO.selectByCode(rnr.getProgramCode()).getId()));

            rnrDAO.saveRnR(requisitionAdapter.convertRnR(rnr));
            return new RnRSubmissionResult(isSuccessful,ReportAndRequisition.SUBMITTED,"R&R successfully submitted!");
        } else {

            try {
                reportIntrimRequest.setErrorDetail(errorMsg);
                String rnrRequestJSON=gson.toJson(reportIntrimRequest);
                baseFactory.uploadJSON(propServ.getProperty("elmis.intrim.rnr.server.url", false).getValue(), ELMISConstants.SERVICE_SDP_INTRIM_REQUISITION.getValue(), "R&R", rnrRequestJSON, Long.class, rnr.getProgramCode(), useAuthToken);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //	rnrDAO.saveRnR(new ReportAndRequisition(null, rnr.getPeriodId() , ELMISConstants.SUCCESSFUL.getValue(), errorMsg , programDAO.selectByCode(rnr.getProgramCode()).getId()));
            return new RnRSubmissionResult(isSuccessful,ELMISConstants.FAILED.getValue(),errorMsg);
        }

    }
    @Override
    public RnRSubmissionResult submitOfflineRnR(RnR rnr){
    	//update physical counts
        Boolean useAuthToken = !(propServ.getProperty("installation.type", false).getValue().equalsIgnoreCase("MEDIATOR"));
        RnrReportIntrimRequest reportIntrimRequest= new RnrReportIntrimRequest();
        if( rnr.getProgramCode().equals(ELMISConstants.ARV.getValue()) && sendRnRToHub ){
            if(!Connectivity.isReachable(propServ.getProperty("elmis.hub.url", false).getValue()))
                return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
        } else {
            if(!Connectivity.isReachable(propServ.getProperty("elmis.central.url", false).getValue())){
                return new RnRSubmissionResult(false,ELMISConstants.FAILED.getValue(),"Unable to reach server. Please check your connection and try again.");
            }
        }

        Gson gson = new Gson();
        skipNonActiveLineItems(rnr);
        reportIntrimRequest.setValue(rnr);
        String rnrJSON = gson.toJson(rnr);
        Boolean isSuccessful = false;
        String errorMsg = null;

/*
		if(!sendRnRToHub){
			ArrayList<Program> programs = lf.getPrograms();
		}
		*/
        String programCode=rnr.getProgramCode();
        try {
            if (sendRnRToHub && programCode.equals(ARV)) {

                isSuccessful = (Boolean) baseFactory.uploadJSON(propServ.getProperty("elmis.hub.url", false).getValue(),ELMISConstants.SERVICE_SDP_REQUISITION.getValue(), "R&R", rnrJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            } else {

                isSuccessful = (Boolean) baseFactory.uploadJSON(propServ.getProperty("elmis.central.url", false).getValue(),ELMISConstants.SERVICE_SDP_REQUISITION.getValue(), "R&R", rnrJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            }

        }catch(JSONException ex){
            if(ex.getMessage().contains("must begin with")){
                errorMsg = "User doesn't have permission to submit R&R.";
            }
            try {
                reportIntrimRequest.setErrorDetail(errorMsg);
                String rnrRequestJSON=gson.toJson(reportIntrimRequest);
                baseFactory.uploadJSON(propServ.getProperty("elmis.intrim.rnr.server.url", false).getValue(), ELMISConstants.SERVICE_SDP_INTRIM_REQUISITION.getValue(), "R&R", rnrRequestJSON, Long.class, rnr.getProgramCode(), useAuthToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch (Exception e) {

            e.printStackTrace();
            errorMsg = e.getMessage();
            if(e.getMessage().contains("full authentication required")){
                errorMsg = "User doesn't have permission to submit R&R.";
            }
            try {
                reportIntrimRequest.setErrorDetail(errorMsg);
                String rnrRequestJSON=gson.toJson(reportIntrimRequest);
                baseFactory.uploadJSON(propServ.getProperty("elmis.intrim.rnr.server.url", false).getValue(),ELMISConstants.SERVICE_SDP_INTRIM_REQUISITION.getValue(), "R&R", rnrRequestJSON , Long.class , rnr.getProgramCode(), useAuthToken);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        if(isSuccessful){
            return new RnRSubmissionResult(isSuccessful,ReportAndRequisition.SUBMITTED,"R&R successfully submitted!");
        } else {
            return new RnRSubmissionResult(isSuccessful,ELMISConstants.FAILED.getValue(),errorMsg);
        }
    }

    private ProductPeriodStatus buildProductPeriodStatus(ProgramProduct pp, Integer periodId) {
        ProductPeriodStatus ppStatus = new ProductPeriodStatus();
        Integer productId = pp.getProduct().getId();
        ppStatus.setPpbId(pp.getId());
        ppStatus.setPeriodId(periodId);
        ppStatus.setProductId(productId);
        ppStatus.setBeginningBalance(storeService.getBeginningBalance(periodId, pp.getId()));


        try {
            ppStatus.setLatestPhysicalCount(storeService.getFacilityPhysicalCount(periodId, pp.getId()));
        } catch (UnavailablePhysicalCountException e) {
            e.printStackTrace();
        }
        ppStatus.setStockOnHand(ppStatus.getLatestPhysicalCount());
        ppStatus.setStockOutDays(0);//TODO: get a correct figure
        ppStatus.setQuantityReceived(txnService.aggregatedTxnQtyByType(pp.getId(), TransactionType.RECEIVING, periodId));
        ppStatus.setQuantityDispensed(txnService.aggregatedTxnQtyByType(pp.getId(), TransactionType.DISPENSING, periodId));
        return ppStatus;
    }

    public List<RegimenLineItem> getBlankRegimenLineItems() {
        List<RegimenLineItem> regimenLines = new ArrayList<RegimenLineItem>();

        List<Regimen> regimens = regimenDAO.getAllRegimens();
        for (Regimen regimen : regimens) {
            RegimenLineItem regLine = new RegimenLineItem();
            regLine.setCode(regimen.getCode());
            regLine.setName(regimen.getName());
            regimenLines.add(regLine);
        }
        return regimenLines;
    }

    @Override
    public ProcessingPeriod getLatestPeriodRnRisSubmittedOrSkippedFor(
            String programCode) {
        return periodDAO.getLatestPeriodRnRisSubmittedOrSkippedFor(programCode);
    }

    @Override
    public RnRSubmissionResult skipRnR(String programCode, Integer periodId) {
        rnrDAO.saveRnR(new ReportAndRequisition(null, periodId, ReportAndRequisition.SKIPPED, "R&R skipped.",
                programDAO.selectByCode(programCode).getId()));
        return new RnRSubmissionResult(true, ReportAndRequisition.SKIPPED, "R&R skipped.");
    }
	@Override
	public RnRSubmissionResult submitRnR(HubRnR offlineRnR) {
		Gson gsonMarshallUnMarshaller = new Gson();
		RnR rnr = gsonMarshallUnMarshaller.fromJson(offlineRnR.getRnrJSON(), RnR.class);
		if(rnr.getProgramCode().equals("ARV")) {
            rnr.setRegimens(getBlankRegimenLineItems());
        }

        facilityCode = propServ.getProperty("facility.code",false).getValue();

		List<FacilityApprovedProduct> programProducts = productDAO.getFacilityApprovedProducts(rnr.getProgramCode(), facilityCode);
        List<RnRLineItem> activeLineItems = new ArrayList<>();
        Optional<FacilityApprovedProduct>  ppResult = null;
        FacilityApprovedProduct facilityApprovedProduct;
        ProgramProduct programProduct;
        for(RnRLineItem rnrLineItem : rnr.getProducts()){
        	ppResult = programProducts.stream().filter(pp->(pp.getProgramProduct().getProduct().getCode().equals(rnrLineItem.getProductCode())
        			&& pp.getProgramProduct().getProgram().getCode().equals(rnr.getProgramCode()))).findFirst();
        	if(ppResult.isPresent()){
        		facilityApprovedProduct = ppResult.get();
        		programProduct  = facilityApprovedProduct.getProgramProduct();
        		rnrLineItem.setReasonForRequestedQuantity("SDP submission!");
                rnrLineItem.setFullSupply(programProduct.getProduct().getFullsupply());
                rnrLineItem.setDispensingUnit(programProduct.getProduct().getDispensingunit());
                rnrLineItem.setMaxMonthsOfStock((double) facilityApprovedProduct.getMaxmonthsofstock());
                rnrLineItem.setPackSize((int) programProduct.getProduct().getPacksize());
                rnrLineItem.setDosesPerMonth(programProduct.getDosespermonth());
                rnrLineItem.setDosesPerDispensingUnit((int) programProduct.getProduct().getDosesperdispensingunit());

                rnrLineItem.setNewPatientCount(0); //TODO: Remove hard coded value

        		normalizeRnRLineItem(rnrLineItem);
        		activeLineItems.add(rnrLineItem);
        	}
        }
        rnr.setProducts(activeLineItems);
		return submitRnR(rnr);
	}


	@Override
    public ArrayList<Requisition> downloadRequisitions(RequisitionSearchRequest requisitionSearchRequest){
        ArrayList<Requisition> requisitions = new ArrayList<>();
        List<Rnr> rnrs = wsClientInterfacing.downloadRnrs(requisitionSearchRequest);
        requisitions= requisitionAdapter.convertRnR(rnrs);
        return requisitions;
    }
    @Override
    public Map<String,List> downloadLogesticPeriods(RequisitionSearchCriteria searchCriteria){
        ArrayList<Requisition> requisitions = new ArrayList<>();
        List<Rnr> rnrs=null;
        Map<String,List> periodRnrsMap=wsClientInterfacing.downloadLogesticPeriods(searchCriteria);
        if(periodRnrsMap!=null) {
            rnrs = periodRnrsMap.get("requisitions");
            requisitions = requisitionAdapter.convertRnR(rnrs);
            periodRnrsMap.replace("requisitions", requisitions);
        }
        return periodRnrsMap;
    }

    @Override
    public ArrayList<Requisition> getMissingRequisitions(Facility facility, ProcessingPeriod offsetPeriod, Program program, Boolean emergency){
        ArrayList<Requisition> requisitions = new ArrayList<>();
        ArrayList<Requisition> downloadedRequisitions;
        Requisition requisition;
        List<RequisitionSearchRequest> requisitionSearchRequests = getRequisitionSearchRequests(facility, offsetPeriod, program, emergency);
        List<Rnr> rnrs;
        for(RequisitionSearchRequest requisitionSearchRequest : requisitionSearchRequests)
        {
            downloadedRequisitions = downloadRequisitions(requisitionSearchRequest);
            if(downloadedRequisitions != null && !downloadedRequisitions.isEmpty()){
                requisitions.addAll(downloadedRequisitions);
            }
        }
        return requisitions;
    }

    public void downloadAndSaveRnrs(Integer facilityId, Integer endingPeriodId, Integer programId, Boolean emergency){
        ArrayList<Requisition> requisitions = getMissingRequisitions( miscService.getFacilityById(facilityId),
                miscService.getProcessingPeriod(endingPeriodId),
                miscService.getProgramById(programId.intValue()), emergency);
        if(requisitions != null && !requisitions.isEmpty()){
            upsertRnrs(requisitions);
        }
    }
    public  Map<String,List> downloadAndSaveLogesticPeriods(Long facilityId,  Long programId, Boolean emergency){
        RequisitionSearchCriteria searchCriteria= new RequisitionSearchCriteria();
        List<Requisition> requisitions=null;
        searchCriteria.setProgramId(programId);
        searchCriteria.setFacilityId(facilityId);
        searchCriteria.setEmergency(emergency);
        Map<String,List> periodsLogesticsMap= downloadLogesticPeriods( searchCriteria);
        if(periodsLogesticsMap!=null) {
            requisitions = periodsLogesticsMap.get("requisitions");
        }
        if(requisitions != null && !requisitions.isEmpty()){
            upsertRnrs(requisitions);
        }
        return periodsLogesticsMap;
    }


    private List<RequisitionSearchRequest> getRequisitionSearchRequests(Facility facility, ProcessingPeriod endingPeriod, Program program, Boolean emergency) {
        List<RequisitionSearchRequest> requisitionSearchRequests = new ArrayList<>();
        RequisitionSearchRequest requisitionSearchRequest;

        ApplicationProperty numberOfPeriodsToDownloadRnrsFor = propServ.getProperty("common.numberOfPeriodsToDownloadRnrsFor", false);
        Integer noOfPeriods = Integer.valueOf(numberOfPeriodsToDownloadRnrsFor != null ? numberOfPeriodsToDownloadRnrsFor.getValue() : "2");

        List<ProcessingPeriod> processingPeriods = commonDAO.getAllProcessingPeriods(ProcessingSchedule.MONTHLY);
        ProcessingPeriod lastPeriod = endingPeriod != null ? endingPeriod : commonDAO.getCurrentPeriod(ProcessingSchedule.MONTHLY);
        if(lastPeriod == null){
            return Collections.emptyList();
        }
        List<ProcessingPeriod> pastPeriods = processingPeriods.stream().filter(pp->pp.getStartdate().compareTo(lastPeriod.getStartdate()) < 1).collect(Collectors.toList());

        List<ProcessingPeriod> periodsToDownloadRnrsFor = pastPeriods.subList(0, noOfPeriods);

        if(program != null) {
            requisitionSearchRequest = new RequisitionSearchRequest();
            requisitionSearchRequest.setFacilityId(facility.getId().longValue());
            requisitionSearchRequest.setProgramId(program.getId().longValue());
            requisitionSearchRequest.setEmergency(emergency);

            List<Long> periodsRnrsAreMissingFor = getPeriodsForMissingRnrs(facility, periodsToDownloadRnrsFor, program, emergency);
            if (!periodsRnrsAreMissingFor.isEmpty()) {
                requisitionSearchRequest.setPeriodIds(periodsRnrsAreMissingFor);
                requisitionSearchRequests.add(requisitionSearchRequest);
            }
        }else{

            List<Program> programs = programDAO.selectFacilityPrograms();

            for (Program p : programs) {
                requisitionSearchRequest = new RequisitionSearchRequest();
                requisitionSearchRequest.setFacilityId(facility.getId().longValue());
                requisitionSearchRequest.setProgramId(p.getId().longValue());
                requisitionSearchRequest.setEmergency(emergency);

                List<Long> periodsRnrsAreMissingFor = getPeriodsForMissingRnrs(facility, periodsToDownloadRnrsFor, p, emergency);
                if(!periodsRnrsAreMissingFor.isEmpty()){
                    requisitionSearchRequest.setPeriodIds(periodsRnrsAreMissingFor);
                    requisitionSearchRequests.add(requisitionSearchRequest);
                }
            }
        }

        return requisitionSearchRequests;
    }

    private List<Long> getPeriodsForMissingRnrs(Facility facility, List<ProcessingPeriod> targetPeriods, Program program, Boolean emergency){
        List<Requisition> requisitions;
        List<Long> periodsForMissingRnrs = new ArrayList<>();
        for (ProcessingPeriod pp : targetPeriods) {
            requisitions = rnrDAO.getRequisitionsByCombo(facility.getId(), program.getId(), pp.getId(), emergency);
            if (requisitions == null || requisitions.isEmpty()) {
                periodsForMissingRnrs.add(pp.getId().longValue());
            }
        }
        return periodsForMissingRnrs;
    }

    public Requisition saveRnr(Rnr rnr){
        Requisition requisition = requisitionAdapter.convertRnR(new RnR(rnr.getFacility().getCode(), rnr.getPeriod().getId(), rnr.getProgram().getCode(),
                "Approver Name", rnr.getSourceApplication(), rnr.getStatus().name(), rnr.isEmergency(), rnr.getAllLineItems(),
                rnr.getRegimenLineItems()));
        requisition.setRequisitionType(rnr.getSourceApplication().equalsIgnoreCase("eLMIS_FE") || rnr.getSourceApplication().equalsIgnoreCase("WEB_UI") ? Requisition.REGULAR_REQUISITION : Requisition.OFFLINE_REQUISITION);
        ArrayList<Requisition> requisitions = new ArrayList<Requisition>();
        requisition.setSynced(true);
        requisitions.add(requisition);
        upsertRnrs(requisitions);
        List<Requisition> updatedRequistions = rnrDAO.getRequisitionsByCombo(requisition.getFacilityId(), requisition.getProgramId(), requisition.getPeriodId(), requisition.getEmergency());
        Requisition theUpdatedRequisition = updatedRequistions.get(0);
        return theUpdatedRequisition;
    }

    @Override
    public void upsertRnrs(List<Requisition> requisitions) {
        if(requisitions!=null){
            for(Requisition r : requisitions){
                if(!r.getEmergency()){
                    List<Requisition> existingReqs = rnrDAO.getRequisitionsByCombo(r.getFacilityId(), r.getProgramId(), r.getPeriodId(), r.getEmergency());
                    if(existingReqs != null && !existingReqs.isEmpty()){
                        Requisition requisition = existingReqs.get(0);
                        requisition.setSynced(r.getSynced());
                        requisition.setStatus(r.getStatus());
                        for(RequisitionLineItem rli : requisition.getRequisitionLineItems()){
                            Optional<RequisitionLineItem> match = r.getRequisitionLineItems().stream().filter(requisitionLineItem -> requisitionLineItem.getProductId().equals(rli.getProductId())).findFirst();
                            if(match.isPresent()) {
                                rli.setBeginningBalance(match.get().getBeginningBalance());
                                rli.setSkipped(match.get().getSkipped());
                            }
                        }
                        rnrDAO.updateRnR(requisition);
                    }else{
                        rnrDAO.saveRnR(r);
                    }
                }else {
                    rnrDAO.saveRnR(r);
                }
            }
        }

    }


}
