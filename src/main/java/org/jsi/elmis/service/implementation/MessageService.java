package org.jsi.elmis.service.implementation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jsi.elmis.service.interfaces.ExposedMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.*;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;
import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

/**
 * Exposes the services for translating keys to corresponding message according to the selected localeCode.
 */

@Service
@NoArgsConstructor
@Scope(value = SCOPE_SESSION, proxyMode = TARGET_CLASS)
public class MessageService {
    private static String LOCALE = "localeCode";

    private ExposedMessageSource messageSource;

    @Setter
    @Getter
    private Locale currentLocale;

    private String locales;


    @Scope(SCOPE_REQUEST)
    public static MessageService getRequestInstance() {
        ExposedMessageSourceImpl exposedMessageSource = new ExposedMessageSourceImpl();
        exposedMessageSource.setBasename("messages");
        return new MessageService(exposedMessageSource, "en");
    }

    @Autowired
    public MessageService(ExposedMessageSource messageSource, @Value("${locales.supported}") String locales) {
        this.messageSource = messageSource;
        this.locales = locales;
        this.currentLocale = new Locale("en");
    }

    public String message(String key) {
        return message(key, currentLocale, (Object) null);
    }

    @SuppressWarnings("non-varargs")
    public String message(String key, Object... args) {
        return message(key, currentLocale, args);
    }

    private String message(String key, Locale locale, Object... args) {
        return messageSource.getMessage(key, args, key, locale);
    }

    public Set<String> getLocales() {
        Set<String> localeSet = new HashSet<>();

        String[] localeCodes = locales.split(",");

        for (String locale : localeCodes) {
            localeSet.add(locale.trim());
        }

        return localeSet;
    }



    public Map<String, Map<String, String>> getAllMessages(){
        Map<String, Map<String, String>> allMessages = new HashMap<>();
        for (String locale : getLocales()) {
            allMessages.put(locale, allMessages(new Locale(locale)));
        }
        return allMessages;
    }



    /**
     * Return all messages using the current localeCode.
     *
     * @return a map of all messages.
     */
    public Map<String, String> allMessages() {
        return allMessages(currentLocale);
    }


    /**
     * Return all messages of the given localeCode.
     *
     * @param locale the localeCode of the messages to return.
     * @return a map of all messages for the given localeCode.
     */
    public Map<String, String> allMessages(Locale locale) {
        return messageSource.getAll(locale);
    }

    public void setCurrentLocale(String locale){


        this.currentLocale = new Locale(locale);


    }
}
