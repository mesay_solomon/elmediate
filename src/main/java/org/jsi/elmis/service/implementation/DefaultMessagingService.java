/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.jsi.elmis.dao.SMSDAO;
import org.jsi.elmis.interfacing.openlmis.ResponseEntity;
import org.jsi.elmis.model.SMS;
import org.jsi.elmis.service.interfaces.MessagingService;
import org.jsi.elmis.interfacing.openlmis.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultMessagingService implements MessagingService{
	@Autowired
	SMSDAO smsDAO;
	
	private static final String baseURL = "http://smsplus1.routesms.com:8080/bulksms/bulksms";
	private static final String userName = "johnsnow";//TODO: should be kept in properties
	private static final String password = "rtu9fgh7";//TODO: should be kept in properties
	
	private static HttpClient httpClient = new HttpClient();
	
	@Override
	public int sendSMS(SMS sms){
		return 0;
	}
	
	@Override
	public List<SMS> getUnsentMessages(){
		List<SMS> smss = smsDAO.selectUnsent();
		for (SMS sms : smss) {
			System.out.println(sms.getMessage());
		}
		return smss;
	}
	
	@Override
	public int updateSent(SMS sms){
		sms.setSent(true);
		return smsDAO.updateSent(sms);
	}
	
	@Override
	public Boolean sendSMSs(List<SMS> SMSs){
		for (SMS sms : SMSs) {
			sendSMS("eLMIS", sms.getPhonenumber(), sms.getMessage());
		}
		return true;
	}
	
	public Boolean sendSMS(String sender, String destinationNumber, String message ){
		String url = baseURL.concat("?username=").concat(userName).concat("&password=").concat(password).
				concat("&type=0&dlr=1&destination=").concat(destinationNumber).concat("&source=").concat(sender).
				concat("&message=").concat(processMessageForURL(message));
		System.out.println(url);

		try {
			httpClient.createContext();
			ResponseEntity res = httpClient.handleRequest(HttpClient.GET, "", url);
			System.out.println(res.getResponse());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;//TODO: return actual success message.
	}
	
	public static String processMessageForURL(String msg){
		try {
			return URLEncoder.encode(msg, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
