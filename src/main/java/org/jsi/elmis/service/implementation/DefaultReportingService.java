package org.jsi.elmis.service.implementation;

import org.jsi.elmis.dao.CommonDAO;
import org.jsi.elmis.reporting.report.IssueVoucherReport;
import org.jsi.elmis.service.interfaces.ReportingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultReportingService implements ReportingService {

    @Autowired
    CommonDAO commonDAO;

    @Override
    public List<IssueVoucherReport> getIssueVoucherItems(Integer txnId) {
        return commonDAO.getIssueVoucherItems(txnId);
    }
}
