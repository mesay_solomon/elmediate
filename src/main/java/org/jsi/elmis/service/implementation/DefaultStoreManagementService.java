/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.*;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.report.StockControlCard;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.ProductService;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.jsi.elmis.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@/**
 * @author Mesay S. Taye
 *
 */
Component
public class DefaultStoreManagementService implements StoreManagementService {

	@Autowired
	TransactionService txnService;
	@Autowired
	NodeService nodeService;
	@Autowired
	ProductService productService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	StoreDAO storeDAO;
	@Autowired
	NodeProductDAO npDAO;
	@Autowired
	NodeDAO nodeDAO;
	@Autowired
	ProductSourceDAO psDAO;
	@Autowired
	FacilityDAO facilityDAO;

	@Override
	public int issue(Node supplier, Node recipient,
			List<TransactionProduct> productsIssued, Date txnDate,
			Integer stockRequestId, Integer userId, ActionOrder actionOrder, String remarks, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {
		Integer txnId = null;
		if (actionOrder == null) {
			Integer order = txnService.getMaxActionOrderInADay(txnDate);
			ActionOrder ao = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(supplier, recipient,
					productsIssued, TransactionType.ISSUING, txnDate,
					userId, ao, null, remarks, batchAware);
		} else {
			txnId = txnService.performTransaction(supplier, recipient,
					productsIssued, TransactionType.ISSUING, txnDate, userId,
					actionOrder, null, remarks, batchAware);
		}
		// if issuing is initiated by a stock rquest, update request status and
		// link transaction id with request id
		if (stockRequestId != null) {
			StockRequisitionTransaction stockRequisitionTxn = new StockRequisitionTransaction(
					null, stockRequestId, txnId);
			storeDAO.insertStockRequisitionTxn(stockRequisitionTxn);
			storeDAO.updateStockRequisitionStatus(stockRequestId,
					ELMISConstants.COMPLETED.getValue());
		}
		return txnId;
	}

	@Override
	public int issueToConsumptionPoint(Node supplier, Node recipient,
			List<TransactionProduct> productsIssued, Date txnDate,
			Integer stockRequestId, Integer userId, ActionOrder actionOrder, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {
		Integer txnId = null;
		if (actionOrder == null) {
			Integer order = txnService.getMaxActionOrderInADay(txnDate);
			ActionOrder ao = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(supplier, recipient,
					productsIssued, TransactionType.DISPENSING, txnDate,
					userId, ao, null, null, batchAware);
		} else {
			txnId = txnService.performTransaction(supplier, recipient,
					productsIssued, TransactionType.DISPENSING, txnDate,
					userId, actionOrder, null, null, batchAware);
		}
		// if issuing is initiated by a stock rquest, update request status and
		// link transaction id with request id
		if (stockRequestId != null) {
			StockRequisitionTransaction stockRequisitionTxn = new StockRequisitionTransaction(
					null, stockRequestId, txnId);
			storeDAO.insertStockRequisitionTxn(stockRequisitionTxn);
			storeDAO.updateStockRequisitionStatus(stockRequestId,
					ELMISConstants.COMPLETED.getValue());
		}
		return txnId;
	}

	@Override
	public int receive(Node store, List<TransactionProduct> productsReceived,
			Date txnDate, Integer userId, String dispatchNo, Integer sourceId,
			ActionOrder actionOrder, String remarks, Boolean batchAware) throws UnavailableNodeProductException,
			InsufficientNodeProductException {

		Node supplier = nodeDAO.getNodeByName(Node.FACILITY);
		// record receipt
		Integer txnId = null;
		if (actionOrder == null) {
			Integer order = txnService.getMaxActionOrderInADay(txnDate);
			actionOrder = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(supplier, store, productsReceived,
					TransactionType.RECEIVING, txnDate, userId,
					actionOrder, null, remarks, batchAware);
		} else {
			txnId = txnService.performTransaction(supplier, store, productsReceived,
					TransactionType.RECEIVING, txnDate, userId, actionOrder,
					null, remarks, batchAware);
		}

		storeDAO.insertProductReceipt(new ProductReceipt(null, txnId,
				dispatchNo, sourceId, null, null));
		return txnId; // TODO: should you return receipt id instead?
	}

	@Override
	public int receiveProducts(Node store, List<TransactionProduct> productsReceived,
							   Date txnDate, Integer userId, String dispatchNo, Integer nodeId,
							   ActionOrder actionOrder, String remarks, Boolean batchAware) throws Exception {

		Node supplier = null;

		if (nodeId != null){

			supplier = nodeDAO.getNodeById(nodeId);

		}

		// record receipt
		Integer txnId = null;
		if (actionOrder == null) {
			Integer order = txnService.getMaxActionOrderInADay(txnDate);
			actionOrder = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(supplier, store, productsReceived,
					TransactionType.RECEIVING, txnDate, userId,
					actionOrder, null, remarks, batchAware);
		} else {
			txnId = txnService.performTransaction(supplier, store, productsReceived,
					TransactionType.RECEIVING, txnDate, userId, actionOrder,
					null, remarks, batchAware);
		}

		ProductSource ps = psDAO.selectByNodeId(supplier.getId());
		Integer sourceId = (ps != null)? ps.getId(): null;

		storeDAO.insertProductReceipt(new ProductReceipt(null, txnId,
				dispatchNo, sourceId, null, null));
		return txnId; // TODO: should you return receipt id instead?
	}


	/*
	 *
	 * @Override public intsetupInitialStockForNode(Node node ,
	 * List<TransactionProduct> products , Date txnDate , Integer userId )
	 * throws UnavailableNodeProductException, InsufficientNodeProductException{
	 * //record receipt Integer txnId = txnService.performTransaction(node,
	 * products, TransactionType.INITIAL_STOCK_SETUP, txnDate , userId); return
	 * txnId; }
	 *
	 */

	@Override
	public int issueToOtherFacility(Node store,
			List<TransactionProduct> productsIssued, Date txnDate,
			Integer userId, Facility facility, ActionOrder actionOrder, Boolean batchAware)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {

		Integer txnId = null;
		Node recipientFacility = nodeDAO.getNodeByName(Node.FACILITY);

		if (actionOrder == null) {
			Integer order = txnService.getMaxActionOrderInADay(txnDate);
			ActionOrder ao = new ActionOrder(order, ActionOrder.AFTER);
			txnId = txnService.performTransaction(store, recipientFacility, productsIssued,
					TransactionType.ISSUE_OUT, txnDate, userId, ao, null, null, batchAware);
		} else {
			txnId = txnService.performTransaction(store, recipientFacility, productsIssued,
					TransactionType.ISSUE_OUT, txnDate, userId, actionOrder,
					null, null, batchAware);
		}
		if (facility != null && facility.getId() != null) {
			storeDAO.insertIssueToFacility(new IssueToOtherFacility(null,
					txnId, facility.getId()));
		}
		return txnId;
	}

	@Override
	public List<StockControlCard> generateStockControlCard(Node node,
			Date from, Date to, Integer ppbId) {

		List<Map<String, Object>> sccMap = storeDAO
				.selectStockControlCardProducts(node.getId(), ppbId, from, to);
		List<StockControlCard> sccList = new ArrayList<StockControlCard>();


//		BigDecimal currentBalance = npDAO.selectNodeProductByNodeandProduct(
//				node.getId(), ppbId).getQuantityOnHand();// TODO: could be null?
//															// Doesn't seem to
//															// be :)
		NodeProduct np = npDAO.selectNodeProductByNodeandProduct(
				node.getId(), ppbId);
		BigDecimal currentBalance = txnService.getBalanceOnDate(DateCustomUtil.getCurrentTime(), np.getId()).getBalance();

		for (Map<String, Object> sMap : sccMap) {
			StockControlCard scc = new StockControlCard();
			scc.setDate((java.util.Date) sMap.get("date"));
            setFromOrToAndRemark(scc, sMap);
			scc.setLossesNAdjustments((BigDecimal) sMap.get(""));
			// scc.setBalance( (BigDecimal) sMap.get("balance"));
			scc.setRefNo((String) sMap.get("ref_no"));

			scc.setUser((String) sMap.get("username"));

			String txnType = (String) sMap.get("txn_type");
			Boolean txnIsPositive = (Boolean) sMap.get("txn_direction");

			if (txnType.equalsIgnoreCase(TransactionType.RECEIVING)) {
				scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
			} else if (txnType.equalsIgnoreCase(TransactionType.ISSUING)
					|| txnType.equalsIgnoreCase(TransactionType.ISSUE_OUT)) {

				if (txnIsPositive) {
					scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
				} else {
					scc.setQtyIssued((BigDecimal) sMap.get("txn_qty"));
				}
				// TODO: when viewed from the recipient's side, the issued
				// quanity must appear under received column
			} else if (txnType
					.equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT)
					|| txnType
							.equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)) {
				scc.setLossesNAdjustments((BigDecimal) sMap.get("txn_qty"));
			}
			if (txnIsPositive != null) {
				if (txnIsPositive) {
					scc.setBalance(currentBalance);
					currentBalance = currentBalance.subtract((BigDecimal) sMap
							.get("txn_qty"));
				} else {
					scc.setBalance(currentBalance);
					currentBalance = currentBalance.add((BigDecimal) sMap
							.get("txn_qty"));
				}
			} else {
				scc.setBalance((BigDecimal) sMap.get("balance"));
			}
			sccList.add(scc);
		}
		return sccList;
	}


	private void setFromOrToAndRemark(StockControlCard scc, Map<String, Object> sMap){
	    String fromOrTo = (String) sMap.get("from_or_to");
	    Gson gson = new Gson();
        Map<String,String> remarksMap = null;
	    if(fromOrTo.equalsIgnoreCase(ELMISConstants.TRANSFER_IN.getValue())){
            remarksMap = gson.fromJson((String) sMap.get("remarks"), Map.class);
            if(remarksMap != null){
                scc.setFromOrTo(remarksMap.get(ELMISConstants.TRANSFER_IN_FACILITY.getValue()));
                scc.setRemark(remarksMap.get(ELMISConstants.REMARK.getValue()));
            }
        }else if(fromOrTo.equalsIgnoreCase(ELMISConstants.TRANSFER_OUT.getValue())){
            remarksMap = gson.fromJson((String) sMap.get("remarks"), Map.class);
            if(remarksMap != null) {
                scc.setFromOrTo(remarksMap.get(ELMISConstants.TRANSFER_OUT_FACILITY.getValue()));
                scc.setRemark(remarksMap.get(ELMISConstants.REMARK.getValue()));
            }
        }else{
            scc.setFromOrTo(fromOrTo);
            scc.setRemark((String) sMap.get("remarks"));
        }
    }

	// TODO: should be moved to node service, along with corresponsing
	// modifications in the StoreController
	@Override
	public List<StockControlCard> selectMostRecentPhysicalCount(Integer nodeId,
			Date from, Date to, Integer ppId) {

		List<Map<String, Object>> sccMap = storeDAO
				.selectMostRecentActionOnNodeProduct(nodeId, ppId, from, to);
		List<StockControlCard> sccList = new ArrayList<>();

		for (Map<String, Object> sMap : sccMap) {
			String txnType = (String) sMap.get("txn_type");
			if (txnType.equalsIgnoreCase(ELMISConstants.PHYSICAL_COUNT
					.getValue())) {
				StockControlCard scc = new StockControlCard();

				scc.setDate((java.util.Date) sMap.get("date"));
				scc.setFromOrTo((String) sMap.get("from_or_to"));
				scc.setLossesNAdjustments((BigDecimal) sMap.get(""));
				scc.setBalance((BigDecimal) sMap.get("balance"));
				scc.setRefNo((String) sMap.get("ref_no"));
				scc.setRemark((String) sMap.get(""));
				scc.setUser((String) sMap.get("username"));

				if (txnType.equalsIgnoreCase(TransactionType.RECEIVING)) {
					scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
				} else if (txnType.equalsIgnoreCase(TransactionType.ISSUING)) {

					Boolean txnIsPositive = (Boolean) sMap.get("txn_direction");
					if (txnIsPositive) {
						scc.setQtyReceived((BigDecimal) sMap.get("txn_qty"));
					} else {
						scc.setQtyIssued((BigDecimal) sMap.get("txn_qty"));
					}
				} else if (txnType
						.equalsIgnoreCase(TransactionType.NEGATIVE_ADJUSTMENT)
						|| txnType
								.equalsIgnoreCase(TransactionType.POSITIVE_ADJUSTMENT)) {
					scc.setLossesNAdjustments((BigDecimal) sMap.get("txn_qty"));
				}
				sccList.add(scc);
			}
		}
		return sccList;
	}

	@Override
	public Boolean pcToBeDoneOnProdInPeriod(Integer nodeId, Date from, Date to, Integer ppId) {
		List<Map<String, Object>> sccMap = storeDAO
				.selectMostRecentActionOnNodeProductv2(nodeId, ppId, null, to);

		Boolean pcPending = false;
		if(sccMap != null && !sccMap.isEmpty()){
			String txnType = (String) sccMap.get(0).get("txn_type");
			if(!txnType.equalsIgnoreCase(ELMISConstants.PHYSICAL_COUNT
					.getValue())){
				pcPending=true;
			}
		}
		return pcPending;
	}

	@Override
	public List<BigDecimal> selectMostRecentPC(Integer nodeId, Date from,
			Date to, Integer ppId) {

		List<Map<String, Object>> sccMap = storeDAO
				.selectMostRecentActionOnNodeProductv2(nodeId, ppId, from, to);
		List<BigDecimal> pcList = new ArrayList<>();

		for (Map<String, Object> sMap : sccMap) {
			String txnType = (String) sMap.get("txn_type");
			if (txnType.equalsIgnoreCase(ELMISConstants.PHYSICAL_COUNT
					.getValue())) {
				pcList.add((BigDecimal) sMap.get("balance"));
			}
		}

		Boolean pcPending = false;

        if (sccMap == null || sccMap.isEmpty()){
            pcPending = false;
        } else {
            if (pcList.isEmpty()){
                pcPending = true;
            }
        }

/*        if (pcPending == false){
        	pcList.add(BigDecimal.ZERO);
        }  */

		return pcList;
	}

	@Override
	public BigDecimal getTotalLnA(Node node, Date from, Date to, Integer ppId) {

		BigDecimal totalLnA = storeDAO.getTotalLossesAndAdjustments(
				node.getId(), ppId, from, to);
		if (totalLnA == null)
			return BigDecimal.ZERO;
		return totalLnA;
	}

	@Override
	public BigDecimal getTotalProductReceipts(Node node, Date from, Date to,
			Integer ppbId) {

		BigDecimal totalLnA = storeDAO.getTotalProductReceipts(node.getId(),
				ppbId, from, to);
		if (totalLnA == null)
			return BigDecimal.ZERO;
		return totalLnA;
	}

	@Override
	public int requestProducts(Integer requesterNodeId, Integer supplierNodeId,
			List<StockRequistionItem> stockRequistionItems, Date requestTime,
			Integer userId,Integer pathId) {
		// insert
		StockRequestPath requestPath= new StockRequestPath();
		requestPath.setId(pathId);
		StockRequisition stockRequisition = new StockRequisition(null, null,
				requesterNodeId, supplierNodeId, requestTime,
				ELMISConstants.PENDING.getValue(), userId, null,null);
		stockRequisition.setRequestPath(requestPath);
		Integer stockRequisitionId = storeDAO
				.insertStockRequisition(stockRequisition);
		for (StockRequistionItem stockRequistionItem : stockRequistionItems) {
			stockRequistionItem.setStockRequisitionId(stockRequisitionId);
			storeDAO.insertStockRequisitionItem(stockRequistionItem);
		}

		return stockRequisitionId;
	}

	@Override
	public int updateStockRequisitionStatus(Integer stockRequisitionId,
			String status) {
		// insert
		storeDAO.updateStockRequisitionStatus(stockRequisitionId, status);
		return stockRequisitionId;
	}

	@Override
	public List<StockRequisitionResult> getStockRequestsBySupplierId(
			Integer supplierNodeId, Date afterTime) {
		return storeDAO
				.selectRequisitionBySupplierId(supplierNodeId, afterTime);
	}

	@Override
	public BigDecimal getFacilityPhysicalCount(Integer periodId,
			Integer productId) throws UnavailablePhysicalCountException {
		ProcessingPeriod period = miscService.getProcessingPeriod(periodId);
		List<NodeProduct> nodeProducts = npDAO
				.selectNodeProductsByProductId(productId);
		BigDecimal pcSum = BigDecimal.ZERO;
		for (NodeProduct nodeProduct : nodeProducts) {
			List<BigDecimal> pcList = selectMostRecentPC( nodeProduct.getNodeId(), null, period.getEnddate(), nodeProduct.getProgramProductId());
			if (pcList != null && !pcList.isEmpty()) {
				pcSum = pcSum.add(pcList.get(0));
			} else {
				//TODO: If physical count is pending, throw exception
				/*throw new UnavailablePhysicalCountException(
						"Physical count missing for "
								+ nodeProduct.getProgramProduct().getProduct()
										.getPrimaryname() + " in "
								+ nodeProduct.getNode().getName()); */

					pcSum = pcSum.add(BigDecimal.ZERO);//pcSum = pcSum.add(txnService.getBalanceOnDate(period.getEnddate(), nodeProduct.getProgramProductId()).getBalance());
			}
		}
		return pcSum;
	}

	@Override
	public BigDecimal getBeginningBalance(Integer periodId, Integer ppbId) {
		ProcessingPeriod period = miscService
				.getPrecedingProcessingPeriod(periodId);
		if (period == null)
			return BigDecimal.ZERO;

		BigDecimal beginningBalance = BigDecimal.ZERO;
		try {
			beginningBalance = getFacilityPhysicalCount(period.getId(), ppbId);
		} catch (UnavailablePhysicalCountException e) {
			// //TODO: what if previous physical count is unavailable
			e.printStackTrace();
		}
		return beginningBalance;
	}

	@Override
	public Integer saveStockTransferTransaction(StockTransferTransaction stockTransferTransaction) {
		return storeDAO.saveStockTransferTransaction(stockTransferTransaction);
	}
}
