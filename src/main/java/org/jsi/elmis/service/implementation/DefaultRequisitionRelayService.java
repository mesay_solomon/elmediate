package org.jsi.elmis.service.implementation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.criteria.RequisitionSearchCriteria;
import org.jsi.elmis.rest.client.model.WSClientRestRequisitionRelay;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rest.result.RestResponse;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RequisitionRelayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DefaultRequisitionRelayService implements RequisitionRelayService {

    @Autowired
    WSClientRestRequisitionRelay restRequisitionClient;
    @Autowired
    MiscellaneousService miscellaneousService;
    @Autowired
    PropertiesService propertiesService;

    protected final Gson jSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();

    private static final String CE_SUBMIT_SDP_REQUISITIONS_URL = "CE_SUBMIT_SDP_REQUISITIONS_URL";
    private static final String CE_INITIATE_SDP_REQUISITION_URL = "CE_INITIATE_SDP_REQUISITION_URL";
    private static final String CE_SEARCH_RNRS_URL = "CE_SEARCH_RNRS_URL";
    private static final String CE_LOGISTICS_PERIODS_URL = "CE_LOGISTICS_PERIODS_URL";
    private static final String INVALID_PERIOD_ID = "invalid.period.id";

    @Override
    public ResponseEntity<RestResponse> submitSdpReport(RnR report, Long userId) {
        Integer fePeriodId = report.getPeriodId();

        ProcessingPeriod processingPeriod = miscellaneousService.getProcessingPeriod(fePeriodId);

        if(processingPeriod == null){
            return RestResponse.error(INVALID_PERIOD_ID, HttpStatus.BAD_REQUEST);
        }


        if(processingPeriod.getCeEquivalentPeriodId() != null) {
            report.setPeriodId(processingPeriod.getCeEquivalentPeriodId());
        }

        String rnrAsJSON = jSONMarshaller.toJson(report);
        String url = propertiesService.getProperty(CE_SUBMIT_SDP_REQUISITIONS_URL, true).getValue();

        return restRequisitionClient.SendJSON(rnrAsJSON, url);
    }

    @Override
    public ResponseEntity<RestResponse> initiateRnr(Integer facilityId, Integer programId, Boolean emergency, Integer periodId, String sourceApplication) {
        String url = propertiesService.getProperty(CE_INITIATE_SDP_REQUISITION_URL, true).getValue();


        ProcessingPeriod processingPeriod = miscellaneousService.getProcessingPeriod(periodId);

        if(processingPeriod == null){
            return RestResponse.error(INVALID_PERIOD_ID, HttpStatus.BAD_REQUEST);
        }


        if(processingPeriod.getCeEquivalentPeriodId() != null) {
            periodId = processingPeriod.getCeEquivalentPeriodId();
        }

        return restRequisitionClient.initiateRnr(url, facilityId, programId, emergency, periodId, sourceApplication);
    }

    @Override
    public ResponseEntity<RestResponse> searchRnrs(RequisitionSearchRequest requisitionSearchRequest) {
        String url = propertiesService.getProperty(CE_SEARCH_RNRS_URL, true).getValue();
        return restRequisitionClient.searchRnrs(url, requisitionSearchRequest);
    }

    @Override
    public ResponseEntity<RestResponse> getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(RequisitionSearchCriteria criteria) {
        String url = propertiesService.getProperty(CE_LOGISTICS_PERIODS_URL, true).getValue();
        return restRequisitionClient.getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(url, criteria);
    }
}
