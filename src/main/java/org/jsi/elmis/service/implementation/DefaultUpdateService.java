/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import org.jsi.elmis.dao.UpdateDAO;
import org.jsi.elmis.model.UpdateHistory;
import org.jsi.elmis.service.interfaces.UpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultUpdateService implements UpdateService {
	
	@Autowired
	UpdateDAO updateDAO;
	
    @Value("${update.directory}")
    private String updateDirectory;
    
    @Value("${execute.update.bash.path}")
    private String executeUpdateBashPath;
    
    @Value("${download.update.bash.path}")
    private String downloadUpdateBashPath;
	
	@Override
	public UpdateHistory selectCurrentVersion(){
		return updateDAO.selectCurrentVersion();
	}
	
	private String latestUpdateFolderName = "0.0";
	
	public String getLatestAvailableUpdate(){
		UpdateHistory currentVersion = this.selectCurrentVersion();
        Double currentVersionNumber = (currentVersion != null)? currentVersion.getVersionNumberNumericValue():0.0;
        Double latestVersion = getLatestUpdateVersionNumber();
        if(currentVersionNumber < latestVersion) {
        	return"v_" + latestVersion;
        } else {
        	return "v_0.0";
        } 
	}
	
	private Double getLatestUpdateVersionNumber(){
        File folder = new File(updateDirectory);
        File[] listOfFiles = folder.listFiles();
        Double maxVersion = 0.0;
            for (int i = 0; i < listOfFiles.length; i++) {
            	if(listOfFiles[i].getName().contains("v_")){
            		if(Double.valueOf(listOfFiles[i].getName().substring(2)) > maxVersion){
            			latestUpdateFolderName = listOfFiles[i].getName();
            			maxVersion = Double.valueOf(listOfFiles[i].getName().substring(2));
            		}
            	}
            }
           
            return maxVersion;
	}
	
	@Override
	public void downloadUpdates(){
		executeCommand(true, downloadUpdateBashPath);
	}
	@Override
	public void executeUpdates(){
		executeCommand(true, executeUpdateBashPath);
	}
	
	@Override
	public void systemExit() {
		System.exit(0);
	}
	public static void executeCommand(Boolean runAsRoot, String command) {
		// try {
		// Runtime.getRuntime().exec("echo \"p@ssw0rd\" | sudo -S
		// /opt/elmis_fe/./test.sh");
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		String cmd = (runAsRoot)?"sudo "+command:command;
		Runtime runtime = Runtime.getRuntime();
		try {
			Process process = runtime.exec(cmd);
			System.out.println(cmd);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getChangelog() {
		StringBuilder changeLogContent = new StringBuilder("");
		File changeLogFile = new File(updateDirectory + "/" + latestUpdateFolderName + "/change-log.txt");
		BufferedReader bufferedReader = null;
		if(changeLogFile.exists()){
			try {
				bufferedReader = new BufferedReader(new FileReader(changeLogFile));
				String line = null;
				while((line=bufferedReader.readLine())!=null){
					changeLogContent.append(line).append("\n");
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				if(bufferedReader != null)
				{
					try {
						bufferedReader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return changeLogContent.toString();
	}


}
