package org.jsi.elmis.service.implementation;

import org.jsi.elmis.service.interfaces.ExposedMessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Implements {@link org.jsi.elmis.service.interfaces.ExposedMessageSource} and
 * extends {@link org.springframework.context.support.ReloadableResourceBundleMessageSource} to provide a way to get
 * all key/message pairs known.
 */
@Component
public class ExposedMessageSourceImpl extends ReloadableResourceBundleMessageSource implements ExposedMessageSource {

	protected Properties getAllProperties(Locale locale) {
		clearCacheIncludingAncestors();
		PropertiesHolder propertiesHolder = getMergedProperties(locale);
		Properties properties = propertiesHolder.getProperties();

		return properties;
	}


	public Map<String, String> getAll(Locale locale) {
		Properties p = getAllProperties(locale);
		Enumeration<String> keys = (Enumeration<String>) p.propertyNames();
		Map<String, String> asMap = new HashMap<>();
		while(keys.hasMoreElements()) {
			String key = keys.nextElement();
			asMap.put(key, p.getProperty(key));
		}
		return asMap;
	}
}
