/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.interfacing.openlmis.BaseFactory;
import org.jsi.elmis.model.DailyStockStatusSubmission;
import org.jsi.elmis.model.Program;
import org.jsi.elmis.model.dto.StockStatusDTO;
import org.jsi.elmis.model.dto.StockStatusLineItemDTO;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.StockStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

@Service
public class DefaultStockStatusService implements StockStatusService{

	@Autowired
	NodeService nodeService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
    PropertiesService propertyService;
    @Autowired
    BaseFactory baseFactory;


	private final Logger log = Logger.getLogger(getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private FacilityDistrictAndProvinceResult facilityDistrictAndProvinceResult;
	private final Gson jSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
	private String dailyStockStatusJSON = "{}";
	private JSONObject response = null;

	@Override
	public void sendDailyStockStatusToOpenELMIS(){
        Boolean useAuthToken = !(propertyService.getProperty("installation.type", false).getValue().equalsIgnoreCase("MEDIATOR"));
		facilityDistrictAndProvinceResult = miscService.selectMyDistrictAndProvince();
		StockStatusDTO stockStatusDTO;

        List<Program> allPrograms = miscService.getAllPrograms();

		for(DailyStockStatusSubmission failedSubmission : nodeService.getFailedDailyStockStatusSubmissions()){

            StockStatusDTO failedStockStatusDTO  = jSONMarshaller.fromJson(failedSubmission.getRemark(), StockStatusDTO.class);
            Optional<Program> programMatch = allPrograms.stream().filter(p -> p.getId().equals(failedStockStatusDTO.getProgramId())).findFirst();
            if(programMatch.isPresent()){
                Program program = programMatch.get();
                try {
                    response = baseFactory.uploadDailyStockStatusJSON(propertyService.getProperty("elmis.central.url", false).getValue(),
                            "daily-stock-status.json",
                            "Stock Status",
                            failedSubmission.getRemark(),
                            JSONObject.class,
                            program.getCode(), useAuthToken);
                    if(response.getString("success") != null){
                        failedSubmission.setStatus(ELMISConstants.SUCCESSFUL.getValue());
                        failedSubmission.setRemark(response.getString("success"));
                        nodeService.saveDailyStockStatusSubmission(failedSubmission);
                    }
                } catch (JSONException e) {

                }
            }
		}

		for(Program program : allPrograms){
			List<Map<String, Object>> stockStatusMapList = nodeService.getFacilityStockOnHand(program.getCode());
			stockStatusDTO = convertToStockStatusDTOList(stockStatusMapList);
			stockStatusDTO.setProgramId(program.getId());

			dailyStockStatusJSON = jSONMarshaller.toJson(stockStatusDTO);

			log.info("Pushing daily stock status @ " + dateFormat.format(new Date()) + " JSON = "+ dailyStockStatusJSON);
			try{
				response = baseFactory.uploadDailyStockStatusJSON(propertyService.getProperty("elmis.central.url", false).getValue(), "daily-stock-status.json", "Stock Status", dailyStockStatusJSON, JSONObject.class, program.getCode(), useAuthToken);
				if(response.getString("success") != null){
					nodeService.saveDailyStockStatusSubmission(new DailyStockStatusSubmission(null, program.getId(), ELMISConstants.SUCCESSFUL.getValue(), stockStatusDTO.getDate(), response.getString("success")));
				}else{
					nodeService.saveDailyStockStatusSubmission(new DailyStockStatusSubmission(null, program.getId(), ELMISConstants.FAILED.getValue(), stockStatusDTO.getDate(), dailyStockStatusJSON));
				}
				log.info("Response " + response);
			}catch(Exception ex){
				log.error("An Error Has Occurred - " + ex.getMessage());
				nodeService.saveDailyStockStatusSubmission(new DailyStockStatusSubmission(null, program.getId(), ELMISConstants.FAILED.getValue(), stockStatusDTO.getDate(), dailyStockStatusJSON));
			}
		}
	}

	StockStatusDTO convertToStockStatusDTOList(List<Map<String, Object>> stockStatusMapList){
		ArrayList<StockStatusLineItemDTO> lineItems = new ArrayList<>();
		for(Map<String,Object> stockStatusMap : stockStatusMapList){
			lineItems.add( new StockStatusLineItemDTO( (Integer)stockStatusMap.get("product_id"), ((BigDecimal)stockStatusMap.get("sum")).intValue(), null) );
		}
		return new StockStatusDTO(facilityDistrictAndProvinceResult.getFacilityId(), null, new Date(), "ELMIS_FE", lineItems);
	}

}
