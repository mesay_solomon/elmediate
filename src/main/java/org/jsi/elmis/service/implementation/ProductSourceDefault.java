/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import java.util.List;

import org.jsi.elmis.dao.NodeDAO;
import org.jsi.elmis.dao.ProductSourceDAO;
import org.jsi.elmis.model.Node;
import org.jsi.elmis.model.ProductSource;
import org.jsi.elmis.service.interfaces.IProductSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductSourceDefault implements IProductSource{
	@Autowired
	ProductSourceDAO prodsourceDAO;
	@Autowired
	NodeDAO nodeDAO;

	private Integer myFacilityId;


	@Override
	public List<ProductSource> getAllProductSources() {
				return this.prodsourceDAO.getAllProductSources();
	}


	@Override
	public Integer create(ProductSource prodSource) {
		prodSource.setFacilityid(myFacilityId);

		Node productSourceNode = new Node();
		productSourceNode.setName(prodSource.getSourcename());
		productSourceNode.setIsExternal(true);
		productSourceNode.setIsVirtual(false);
		productSourceNode.setVisibleForLogin(false);

		nodeDAO.saveNode(productSourceNode);

		prodSource.setId(productSourceNode.getId());
		prodSource.setNodeId(productSourceNode.getId());

		return this.prodsourceDAO.insertProductSource(prodSource);
	}


	@Override
	public Integer edit(ProductSource prodSource) {
		Node n = nodeDAO.getNodeById(prodSource.getId());
		n.setName(prodSource.getSourcename());
		return this.nodeDAO.updateNode(n);
//		return  this.prodsourceDAO.editProductSource(prodSource);
	}


	@Override
	public Integer delete(ProductSource request) {

		return this.prodsourceDAO.deleteProductSource(request);
	}

}
