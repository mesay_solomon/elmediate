package org.jsi.elmis.service.implementation;

import org.jsi.elmis.dao.FEUpdateInfoDAO;
import org.jsi.elmis.model.FEUpdateInfo;
import org.jsi.elmis.service.interfaces.FEUpdateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultFEUpdateInfoService implements FEUpdateInfoService {

    @Autowired
    FEUpdateInfoDAO feUpdateInfoDAO;

    @Override
    public void insert(FEUpdateInfo feUpdateInfo) {
        feUpdateInfoDAO.insert(feUpdateInfo);
    }

    @Override
    public List<FEUpdateInfo> findAll() {
        return feUpdateInfoDAO.selectAll();
    }

    @Override
    public List<FEUpdateInfo> findByFacilityCode(String facilityCode) {
        return feUpdateInfoDAO.selectByFacilityCode(facilityCode);
    }
}
