/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.jsi.elmis.dao.SmartCareIntegrationDAO;
import org.jsi.elmis.rest.client.WSClientSmartCare;
import org.jsi.elmis.rest.client.model.SmartCarePatientRegistration;
import org.jsi.elmis.service.interfaces.ARVDispensingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * @author Adugna Worku
 * 
 *  This service takes care of any integration services that is related with
 *  SmartCare integration
 *  
 **/

@Service
public class SmartCareIntegrationService {

	@Autowired
	private SmartCareIntegrationDAO smartCareIntegrationDAO;
	@Autowired
	private WSClientSmartCare wsClientSmartCare;

	@Autowired
	private ARVDispensingService  arvDispensingService;

	@Value("${smartcare.integration.numberOfDaysToPullRegistrationsFor}")
	private String numberOfDaysToPullRegistrationsFor;
	
	private Integer WORKING_WEEK_DAYS_OFFSET = 2;
	
	public List<Map<String, Object>> getDispensationDataByDate(Date startDate, Date endDate){
		return smartCareIntegrationDAO.getDispensation(startDate, endDate);
	}
	
	
	public void importPatientRegFromSmartCare(){
		
		Date endDate = new Date();
		Date startDate = (Date) endDate.clone();
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		Integer dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
		cal.set(Calendar.DAY_OF_YEAR, (dayOfYear - Integer.valueOf(numberOfDaysToPullRegistrationsFor)));
		startDate = cal.getTime();
		
		
		//TODO : subject to change when SmartCare implementation removes explicit parameters 0 and 1
		ArrayList<SmartCarePatientRegistration> newRegistrations = wsClientSmartCare.pullClientRegistrationFromSmartCare(0, startDate, endDate);
		ArrayList<SmartCarePatientRegistration> updatedRegistrations = wsClientSmartCare.pullClientRegistrationFromSmartCare(1, startDate, endDate);

		/*Gson smartCareJSONMarshaller = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").create();
		String regJSON = "[{\n" +
                "   \"RegistrationDate\":\"2018-07-02T10:56:10.0000+02:00\",\n" +
                "   \"DateOfBirth\":\"1993-07-11T00:00:00.0000+02:00\",\n" +
                "   \"ArtNumber\":\"5006-01460-333333-8\",\n" +
                "   \"NrcNumber\":null,\n" +
                "   \"Vitals\":{\n" +
                "      \"Height\":null,\n" +
                "      \"HeightDateCollected\":null,\n" +
                "      \"Weight\":null,\n" +
                "      \"WeightDateCollected\":null,\n" +
                "      \"BloodPressure\":null,\n" +
                "      \"BloodPressureDateCollected\":null\n" +
                "   },\n" +
                "   \"CD4\":null,\n" +
                "   \"ViralLoad\":null,\n" +
                "   \"RegimenId\":null,\n" +
                "   \"RegimenCode\":\"ABC + 3TC +EFV (ADFL)\",\n" +
                "   \"Prescription\":{\n" +
                "      \"Date\": \"2018-07-11T00:00:00.0000+02:00\",\n" +
                "      \"PrescriptionDrugs\":[\n" +
                "         {\n" +
                "            \"DrugCode\":\"EM0006\",\n" +
                "            \"QuantityPerDose\":\"1\",\n" +
                "            \"DosageUnit\": \"tab\",\n" +
                "            \"Frequency\":\"bd\", \n" +
                "            \"Duration\":\"45\" \n" +
                "         },\n" +
                "         {\n" +
                "            \"DrugCode\":\"EM0001\",\n" +
                "            \"QuantityPerDose\":\"1\",\n" +
                "            \"DosageUnit\": \"tab\",\n" +
                "            \"Frequency\":\"bd\", \n" +
                "            \"Duration\":\"90\" \n" +
                "         },\n" +
                "         {\n" +
                "            \"DrugCode\":\"EM0524\",\n" +
                "            \"QuantityPerDose\":\"1\",\n" +
                "            \"DosageUnit\": \"tab\",\n" +
                "            \"Frequency\":\"bd\", \n" +
                "            \"Duration\":\"90\" \n" +
                "         }\n" +
                "      ]\n" +
                "   },\n" +
                "   \"FirstName\":\"RON\",\n" +
                "   \"LastName\":\"MWANSA\",\n" +
                "   \"PatientGUID\":\"83937162b92042fd8785d1ff0067e3d5\",\n" +
                "   \"PatientID\":\"5006-01460-00009-9\",\n" +
                "   \"Sex\":\"M\",\n" +
                "   \"BirthDay\":\"11\",\n" +
                "   \"BirthMonth\":\"7\",\n" +
                "   \"BirthYear\":\"1993\",\n" +
                "   \"Sequence\":-1\n" +
                "}]";

		System.out.println(regJSON);

		ArrayList<SmartCarePatientRegistration> newRegistrations = smartCareJSONMarshaller.fromJson(regJSON,
				new TypeToken<ArrayList<SmartCarePatientRegistration>>() {
				}.getType());*/

		if(newRegistrations != null){
			arvDispensingService.upsertARVClientsFromSmartCare(newRegistrations);
		}
		if(updatedRegistrations != null){
			arvDispensingService.upsertARVClientsFromSmartCare(updatedRegistrations);
		}
	}
	
	public Integer getMaxSequenceNumber(){
		return smartCareIntegrationDAO.getMaxSequenceNumber();
	}
	
}
