/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.service.implementation;

import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.common.util.DateCustomUtil;
import org.jsi.elmis.dao.*;
import org.jsi.elmis.dao.mappers.TransactionProgramProductBatchMapper;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.dto.TransactionHistoryDTO;
import org.jsi.elmis.model.dto.transaction.details.TransactionDetails;
import org.jsi.elmis.model.dto.transaction.details.TransactionDetailsFactory;
import org.jsi.elmis.rest.client.WSClientMiddleWare;
import org.jsi.elmis.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.*;

/**
 * @author Mesay S. Taye
 *
 */
@Component
public class DefaultTransactionService implements TransactionService {

	@Autowired
	ProductService productService;
	@Autowired
	ProductPeriodStatusService productPeriodStatusService;
	@Autowired
	MiscellaneousService miscService;
	@Autowired
	TransactionDetailsFactory txnDetailsFactory;
	@Autowired
	NodeService nodeService;
	@Autowired
	TransactionDAO txnDAO;
	@Autowired
	NodeDAO nodeDAO;
	@Autowired
	ActionDAO actionDAO;
	@Autowired
	TransactionProductDAO txnProdDAO;
	@Autowired
	NodeProductDAO nodeProdDAO;
	@Autowired
	PhysicalCountDAO pcDAO;
	@Autowired
	WSClientMiddleWare wsClientMiddleWare;
	@Autowired
	UserDAO userDAO;

	@Value("${rnr.schedule.weekly}")
	Boolean rnrSubmittedWeekly;
	@Value("${rnr.schedule.monthly}")
	Boolean rnrSubmittedMonthly;
	@Value("${delete.pc.backlog.entry}")
	Boolean deletePCOnRegisterBacklogTxn;

	public BigDecimal getUsableBalance(Integer nodeid,Integer ppid,Date date){
		NodeProduct np = nodeService.findProductByNodeAndProduct(nodeid,ppid);
		Integer nppId = np.getId();
		NodeProductDateBalance bal = getBalanceOnDate(date, nppId);
		if(bal == null || bal.getBalance() == null || bal.getUsableBalance() == null){
			return BigDecimal.ZERO;
		} else {
			return bal.getUsableBalance();
		}
	}


	private Boolean isReversal(String txnTypeName){
		return txnTypeName.equals(TransactionType.DISPENSING_REVERSAL) ||
				txnTypeName.equals(TransactionType.RECEIVING_REVERSAL) ||
				txnTypeName.equals(TransactionType.ISSUING_REVERSAL) ||
				txnTypeName.equals(TransactionType.POSITIVE_ADJUSTMENT_REVERSAL) ||
				txnTypeName.equals(TransactionType.NEGATIVE_ADJUSTMENT_REVERSAL) ;

	}

	@Override
	public Integer reverseTransaction(Transaction transaction ,  TransactionType txnType , List<NodeTransaction> nodeTxns ,Integer userId ) throws UnavailableNodeProductException, InsufficientNodeProductException{
		Map<String , NodeTransaction> nodeTxnsMap = getNodeTxnWithDirections(nodeTxns);
		Integer reverseTxnId = null;

					reverseTxnId = performTransaction(nodeTxnsMap.get(NodeTransaction.RECIPIENT).getNode(),
							nodeTxnsMap.get(NodeTransaction.SUPPLIER).getNode(),
							transaction.getTxnProducts(),
							TransactionType.getReverseTransactionType(txnType.getName()),
							transaction.getTransactionTimestamp(),
							userId,
							null,
							transaction.getId(),
							null, true);

		ReversalTransaction revTxn = new ReversalTransaction(null, transaction.getId(), reverseTxnId);
		//txnDAO.saveReversalTransaction(revTxn);
		txnDAO.deleteAssociatedInformationWithReversedTxn(transaction.getId());
		return reverseTxnId;
	}

	@Override
	public Integer reverseTransaction(Integer transactionId , Integer userId) throws UnavailableNodeProductException, InsufficientNodeProductException{
		Transaction txn = txnDAO.getTransaction(transactionId);
		TransactionType txnType = txnDAO.getTransactionType(txn.getTransactionType());
		List<NodeTransaction> nodeTxns = txnDAO.selectNodeTxnsByTxnId(transactionId);

		Integer revTxnId = null;

			revTxnId = reverseTransaction(txn , txnType , nodeTxns , userId);

		for (NodeTransaction nodeTransaction : nodeTxns) {
			if(deletePCOnRegisterBacklogTxn){
				for (TransactionProduct txnProduct : txn.getTxnProducts()) {
					try{
					pcDAO.deletePhysicalCountsAfterTimeStamp(txn.getTransactionTimestamp(), nodeTransaction.getNodeId(), txnProduct.getProgramProductId());
					} catch (Exception ex){
						ex.printStackTrace();
					}
				}
			}
		}
		Integer stockTransferId = txnDAO.getStockTransferIdByTxnId(transactionId);
		if(stockTransferId != null)
		{
			StockTransfer stockTransfer = wsClientMiddleWare.getStockTransferById(stockTransferId);
			if(stockTransfer!=null){
				if(!stockTransfer.getStatus().getStatus().equalsIgnoreCase(StockTransferStatus.REVERSAL_INITIATED)
						&& !stockTransfer.getStatus().getStatus().equalsIgnoreCase(StockTransferStatus.REVERSAL_COMPLETED)){
					stockTransfer.getStatus().setUserUUID(userDAO.selectUserById(userId).getUuid());
					stockTransfer.getStatus().setStatus(StockTransferStatus.REVERSAL_INITIATED);
					wsClientMiddleWare.updateStockTransfer(stockTransfer);
				}
			}
		}
		return revTxnId;
	}



	@Override
	public Map<String , NodeTransaction> getNodeTxnWithDirections(List<NodeTransaction> nodeTxns){
		Map<String , NodeTransaction> nodeTxnsMap = new HashMap<String, NodeTransaction>();
		if(nodeTxns != null && nodeTxns.size() == 2){
			nodeTxnsMap = new HashMap<String, NodeTransaction>();
			if(nodeTxns.get(0).getPositive()){
				nodeTxnsMap.put(NodeTransaction.RECIPIENT, nodeTxns.get(0));
				nodeTxnsMap.put(NodeTransaction.SUPPLIER, nodeTxns.get(1));
			} else {
				nodeTxnsMap.put(NodeTransaction.RECIPIENT, nodeTxns.get(1));
				nodeTxnsMap.put(NodeTransaction.SUPPLIER, nodeTxns.get(0));
			}
		}
		return nodeTxnsMap;
	}

	// Better implementation - parametrize with txnProduxts
	private NodeProduct findTxnProductInNode(List<NodeProduct> nodeProducts,
			TransactionProduct txnProduct) {

		NodeProduct np = null;
		for (NodeProduct nodeProduct : nodeProducts) {

			if (nodeProduct.getProgramProductId()!= null && nodeProduct.getProgramProductId().equals(txnProduct.getProgramProductId())) {

				np = new NodeProduct(nodeProduct.getId(), nodeProduct.getProgramProductId(), nodeProduct.getProgramProduct(), nodeProduct.getTotalInflow(), nodeProduct.getTotalOutflow(),
						nodeProduct.getTotalAdjustments(), nodeProduct.getLatestPhysicalCountId(), nodeProduct.getNodeId(), nodeProduct.getNode(), nodeProduct.getQuantityOnHand(), new ArrayList<>());

                for (TransactionProgramProductBatch tProBatch: txnProduct.getBatchQuantities()) {
                    for (NodeProgramProductBatch nProBatch: nodeProduct.getNodeProgramProductBatches()) {

                        if (tProBatch.getProgramProductBatchId().equals(nProBatch.getProgramProductBatchId())){
                            np.getNodeProgramProductBatches().add(nProBatch);
                        }
                    }

                }
				return np;
			}
		}
		return null;
	}

	@Override
	public TransactionType getTransactionTypeByName(String name) {
		return txnDAO.getTransactionTypeByName(name);
	}

	@Override
	public List<TransactionHistoryItem> getTransactionsToReverse(Integer nodeId , Integer limit , Integer offset , Date fromTxnDate, Date toTxnDate, String programCode, String productCode, String scheduleCode) {
        List<TransactionHistoryItem> transactionHistoryItems = txnDAO.getTransactionsToReverse(nodeId , limit , offset , scheduleCode, fromTxnDate, toTxnDate, programCode, productCode);
        for(TransactionHistoryItem transactionHistoryItem : transactionHistoryItems){
            for(Transaction transaction : transactionHistoryItem.getTransactions()){
                TransactionDetails txnDetails = getTransactionDetails(transaction.getId());
                transaction.setTxnProducts(txnDetails.getTransaction().getTxnProducts());
            }
        }
        return transactionHistoryItems;
	}

	@Override
	public String getTransactionDescription(Integer txnId){
		return txnDAO.getTransactionDescription(txnId);
	}

	@Override
	public Integer getTransactionsToReverseCount(Integer nodeId, Date fromTxnDate, Date toTxnDate, String programCode, String productCode, String scheduleCode) {
		return txnDAO.selectTransactionsForReversalCount(nodeId, fromTxnDate, toTxnDate, programCode, productCode, scheduleCode);
	}

	@Override
	public String getScheduleCodeForReversal(){
		if(rnrSubmittedWeekly){
			return ProcessingSchedule.WEEKLY;
		} else {
			return ProcessingSchedule.MONTHLY;
		}
	}

	@Override
	public BigDecimal aggregatedTxnQtyByType(Integer ppbId, String txnType,
			Integer periodId) {
		ProcessingPeriod period = miscService.getProcessingPeriod(periodId);
		BigDecimal sum = txnDAO.aggregatedTxnQtyByType(ppbId, txnType, period.getStartdate(), period.getEnddate());
		return (sum == null)?BigDecimal.ZERO:sum;
	}

	@Override
	public void refreshView4RnR(){
		txnDAO.refreshView4RnR();
	}

	@Override
	public void refreshActionTimeline(){
		txnDAO.refreshActionTimeline();
	}

	public BigDecimal getAveragePeriodicConsumption(Integer ppbId,
			Integer periodId, String scheduleCode, Integer noOfPeriods, BigDecimal currentPeriodConsumption) {
		Integer noOfMonthsWithNonZeroConsumption = txnDAO.getCountOfNonZeroConsumptionBefore(periodId, ppbId, scheduleCode, noOfPeriods);
		if(currentPeriodConsumption.compareTo(BigDecimal.ZERO) > 0){
			noOfMonthsWithNonZeroConsumption += 1;
		}
		try {
			BigDecimal pcsum = txnDAO.selectAveragePeriodicConsumption(periodId, ppbId, scheduleCode, noOfPeriods, currentPeriodConsumption);
			if(pcsum == null){
				return currentPeriodConsumption;
			}else{
				MathContext mc = new MathContext(10, RoundingMode.HALF_UP);
				BigDecimal totalConsumption = pcsum.add(currentPeriodConsumption);
				BigDecimal remainder = totalConsumption.remainder(BigDecimal.valueOf(noOfMonthsWithNonZeroConsumption));
				if(remainder.compareTo(BigDecimal.ZERO) > 0){
					 return BigDecimal.valueOf(totalConsumption.divide(BigDecimal.valueOf(noOfMonthsWithNonZeroConsumption), mc).intValue() + 1);
				}
				return totalConsumption.divide(BigDecimal.valueOf(noOfMonthsWithNonZeroConsumption), mc);
			}
		} catch(Exception ex) {
	        ex.printStackTrace();
	        return currentPeriodConsumption;
	    }
	}

	@Override
	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,Integer productId,Date from){
		List<TransactionHistoryDTO> txnHistoryItems = txnDAO.selectTransactionHistory(nodeId, productId, from);
		BigDecimal stockOnHand = null;
		if(nodeId == null){
			stockOnHand = nodeProdDAO.getFacilityStockOnHand(productId);
		} else {
			NodeProduct nodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, productId);
			stockOnHand = nodeProd.getQuantityOnHand();
		}
		if(txnHistoryItems != null && !txnHistoryItems.isEmpty()){
			txnHistoryItems.get(0).setQtyAfterTxn(stockOnHand);
			txnHistoryItems.get(0).setQtyBeforeTxn(stockOnHand.subtract(txnHistoryItems.get(0).getSignedTxnQuantity()));
			for(int i = 1; i < txnHistoryItems.size() ; i++){
				txnHistoryItems.get(i).setQtyAfterTxn(txnHistoryItems.get(i - 1).getQtyBeforeTxn());
				txnHistoryItems.get(i).setQtyBeforeTxn(txnHistoryItems.get(i).getQtyAfterTxn().subtract(txnHistoryItems.get(i).getSignedTxnQuantity()));
			}
		}
		return txnHistoryItems;
	}

	@Override
	public List<TransactionHistoryDTO> selectTransactionHistory(Integer nodeId,Integer productId,Date from , Date to){//TODO: Should be unified with the method above
		List<TransactionHistoryDTO> txnHistoryItems = txnDAO.selectTransactionHistoryInRange(nodeId, productId, from , to);
		BigDecimal stockOnHand = null;
		if(nodeId == null){
			stockOnHand = nodeProdDAO.getFacilityStockOnHand(productId);
		} else {
			NodeProduct nodeProd = nodeProdDAO.selectNodeProductByNodeandProduct(nodeId, productId);
			stockOnHand = nodeProd.getQuantityOnHand();
		}
		if(txnHistoryItems != null && !txnHistoryItems.isEmpty()){
			txnHistoryItems.get(0).setQtyAfterTxn(stockOnHand);
			txnHistoryItems.get(0).setQtyBeforeTxn(stockOnHand.subtract(txnHistoryItems.get(0).getSignedTxnQuantity()));
			for(int i = 1; i < txnHistoryItems.size() ; i++){
				txnHistoryItems.get(i).setQtyAfterTxn(txnHistoryItems.get(i - 1).getQtyBeforeTxn());
				txnHistoryItems.get(i).setQtyBeforeTxn(txnHistoryItems.get(i).getQtyAfterTxn().subtract(txnHistoryItems.get(i).getSignedTxnQuantity()));
			}
		}
		return txnHistoryItems;
	}

	private void ensureProductExistenceInNodes(Node supplier, Node recipient, List<TransactionProduct> txnProducts){
		for (TransactionProduct txnProduct: txnProducts) {

			List<NodeProgramProductBatch> nppbs =  TPPB2NPPB(txnProduct.getBatchQuantities());


			NodeProduct supplierProduct = nodeProdDAO.selectNodeProductByNodeandProduct(supplier.getId(), txnProduct.getProgramProductId());
			if (supplierProduct == null) supplierProduct = new NodeProduct(null, txnProduct.getProgramProductId(), null, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null, supplier.getId(), null, BigDecimal.ZERO, nppbs);
			nodeProdDAO.createIfNotExists(supplierProduct, txnProduct);

			NodeProduct recipientProduct =  nodeProdDAO.selectNodeProductByNodeandProduct(recipient.getId(), txnProduct.getProgramProductId());
			if (recipientProduct == null) recipientProduct = new NodeProduct(null, txnProduct.getProgramProductId(), null, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null, recipient.getId(), null, BigDecimal.ZERO, nppbs);
			nodeProdDAO.createIfNotExists(recipientProduct, txnProduct);
		}
	}

	private List<NodeProgramProductBatch> TPPB2NPPB(List<TransactionProgramProductBatch> tppbs){
		List<NodeProgramProductBatch> nppbs = new ArrayList<>();
		for (TransactionProgramProductBatch tppb:
			 tppbs) {
			NodeProgramProductBatch nppb = new NodeProgramProductBatch(null, null, tppb.getProgramProductBatchId(), BigDecimal.ZERO);
			nppbs.add(nppb);
		}
		return nppbs;
	}

	//    supplier, recipient, txnProducts, txnTypeName, txnDate, userId , actionOrder, reversedTxnId , remarks
	@Override
	public Integer performTransaction(Node supplier , Node recipient, List<TransactionProduct> txnProducts, String txnTypeName,
				Date txnDate ,Integer userId , ActionOrder actionOrder, Integer reversedTxnId, String remarks, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException{

			TransactionType txnType = txnDAO.getTransactionTypeByName(txnTypeName);

			ensureProductExistenceInNodes(supplier, recipient, txnProducts);
			// Check for nulls in supplier fields

			if (supplier.getIsExternal() == null || supplier.getIsVirtual() == null ) supplier = nodeDAO.getNodeById(supplier.getId());
			if(recipient.getIsExternal() == null || supplier.getIsVirtual() == null)  recipient = nodeDAO.getNodeById(recipient.getId());

			supplier.setProducts(nodeProdDAO.selectNodeProducts(supplier.getId()));
            recipient.setProducts(nodeProdDAO.selectNodeProducts(recipient.getId()));

			List<NodeProduct> supplierProducts = new ArrayList<>();
			List<NodeProduct> recipientProducts = new ArrayList<>();

        //<editor-fold desc="txn-prelim">
        for (TransactionProduct txnProduct : txnProducts) {

				NodeProduct supplierProduct = findTxnProductInNode(supplier.getProducts(), txnProduct);// a copy of np with batches filtered
				NodeProduct recipientProduct = findTxnProductInNode(recipient.getProducts(), txnProduct); //a copy of np with batches filtered

				Boolean supplierIsABlackHole = nodeService.nodeIsDispensingPointForProduct(supplier.getId(), txnProduct.getProgramProductId());
				Boolean skipStockValidation = (reversedTxnId != null && supplierIsABlackHole) || supplier.getIsExternal() || supplier.getIsVirtual(); // will there ever be a case where virtual (or even external) may have to be validated?

				if(!skipStockValidation){
					if(supplierProduct == null){
						throw new UnavailableNodeProductException("Product with id " + txnProduct.getProgramProductId() +" not available in " + supplier.getName()+".");
					} else {
							BigDecimal usableBalance = getUsableBalance(supplier.getId(), supplierProduct.getProgramProductId(), txnDate);
							if(usableBalance.compareTo(txnProduct.getQuantity()) < 0){

								throw new InsufficientNodeProductException("Product with id " +
							    txnProduct.getProgramProductId() + " is unavailable in " +
								supplier.getName() + " in the required amount.");
						}
					}
				}	else {
							if(supplierProduct == null){
							    supplierProduct = initNodeProd(supplier.getId(), txnProduct);
								//initializeBatchProducts
							}
				}
				if(recipientProduct == null){
                    recipientProduct = initNodeProd(recipient.getId(), txnProduct);
					//initializeBatchQuantities
				}

				supplierProducts.add(supplierProduct);
				recipientProducts.add(recipientProduct);
				// TODO: #txn : Here, nodeProducts should be  nodeProducts with their filteredBatchQuantities?
			}
        //</editor-fold>

            Action action = createAction(txnDate, userId,  reversedTxnId,  actionOrder);

			Transaction txn = new Transaction( null ,txnType.getId() , txnDate , userId, action.getId(), txnProducts, remarks);
            Integer txnID = txnDAO.saveTransaction(txn);

            if(reversedTxnId != null) txnDAO.saveReversalTransaction(new ReversalTransaction(null, reversedTxnId, txnID));

			NodeTransaction supplierTxn = new NodeTransaction(null, supplier.getId(),  false , txnID , null);
			NodeTransaction recipientTxn = new NodeTransaction(null, recipient.getId(),  true , txnID , null);

			Integer supTxnId = txnDAO.saveNodeTransaction(supplierTxn);
			Integer recTxnId = txnDAO.saveNodeTransaction(recipientTxn);

			Integer i = 0;
			for (TransactionProduct txnProduct : txnProducts) {

				Boolean recipientIsABlackHole = nodeService.nodeIsDispensingPointForProduct(recipient.getId(), txnProduct.getProgramProductId());//A black hole for a program is a node where any product in that program received by the node gets lost forever(not tracked anymore) save txnProduct
				Boolean supplierIsABlackHole = nodeService.nodeIsDispensingPointForProduct(supplier.getId(), txnProduct.getProgramProductId());

				txnProduct.setTransactionId(txnID);

				if (txnProduct.getRemarks() == null || txnProduct.getRemarks().trim().equals("")){
				    if (txnProduct.getBatchQuantities() != null & !txnProduct.getBatchQuantities().isEmpty()){
				        txnProduct.setRemarks(txnProduct.getBatchQuantities().get(0).getRemark());
                    }
                }
				txnProdDAO.saveTransactionProduct(txnProduct);

				NodeProduct supplierProduct = supplierProducts.get(i);//findTxnProductInNode(supplier.getProducts(), txnProduct);
				NodeProduct recipientProduct = recipientProducts.get(i++);//findTxnProductInNode(recipient.getProducts(), txnProduct);

                recipientProduct.setQuantitiesForTxn(txnProduct, true);

				if(!supplierIsABlackHole){
                    supplierProduct.setQuantitiesForTxn(txnProduct, false);
					nodeProdDAO.updateNodeProduct(supplierProduct);

	                saveNodeProductDailyStatusSummary(supplierProduct,txnDate,txnProduct, false, isReversal(txnTypeName));
	                saveNodeProductDailyTransactionTotals(txnDate, supplierProduct.getId(), txnType.getId(),txnProduct.getQuantity(),false);
                }

				if(!recipientIsABlackHole) {

                    nodeProdDAO.saveNodeProduct(recipientProduct);

                    saveNodeProductDailyStatusSummary(recipientProduct,txnDate,txnProduct,true, isReversal(txnTypeName));
                    saveNodeProductDailyTransactionTotals(txnDate, recipientProduct.getId(), txnType.getId(),txnProduct.getQuantity(),true);

                }

				if(actionOrder != null && deletePCOnRegisterBacklogTxn){

					if(recipientProduct.getNode() != null && recipientProduct.getProgramProductId() != null){
						pcDAO.deletePhysicalCountsAfterDate(txnDate, recipientProduct.getNode().getId(), recipientProduct.getProgramProductId());
					}

					if(!supplierIsABlackHole){
						pcDAO.deletePhysicalCountsAfterDate(txnDate, supplierProduct.getNode().getId(), supplierProduct.getProgramProductId());
					}

				}
			}

			return txnID;
		}
		// End performTransaction

		private NodeProduct initNodeProd(Integer nodeId, TransactionProduct txnProduct){
		    NodeProduct np = new NodeProduct();

            np.setNodeId(nodeId);
            np.setProgramProductId(txnProduct.getProgramProductId());
            np.setQuantityOnHand(BigDecimal.ZERO);
            np.setTotalAdjustments(BigDecimal.ZERO);
            np.setTotalInflow(BigDecimal.ZERO);
            np.setTotalOutflow(BigDecimal.ZERO);

            return np;
        }

        private Action createAction(Date txnDate, Integer userId, Integer reversedTxnId, ActionOrder actionOrder){
            Action action = new Action();
            action.setTimestamp(txnDate);
            action.setType(ELMISConstants.TRANSACTION.getValue());
            action.setUserId(userId);

            if(reversedTxnId != null){
                actionOrder = new ActionOrder();
                actionOrder.setPrecedence(ActionOrder.AFTER);
                actionOrder.setNeignboringOrder(getActionOrder4Txn(reversedTxnId));
            }

            actionDAO.saveAction(action , actionOrder);
            return action;

        }

	    public Integer getActionOrder4Txn(Integer txnId){
	    	return txnDAO.getActionOrder4Txn(txnId);
	    }

		private void saveNodeProductDailyStatusSummary(NodeProduct nodeProd, Date date , TransactionProduct txnProduct, Boolean txnIsPositive, Boolean isReversal) { // Assmes #TNPA

	        Integer nodeProgProdId = nodeProd.getId(); BigDecimal txnQty = txnProduct.getQuantity(); BigDecimal signedTxnQty = (txnIsPositive)?txnQty:txnQty.negate();

			NodeProgramProductDailyStatus nppDailyStatus = getNPPDailyNodeProgProdStatus(date, nodeProd);
			if(nppDailyStatus == null){

                nppDailyStatus = createDailyStatusForTxn(date, nodeProd,signedTxnQty, txnProduct, txnIsPositive);
                nodeProdDAO.saveNodeProdDailyStatus(nppDailyStatus);
                updateSubsequentStatuses(nodeProd, date, signedTxnQty, txnProduct, txnIsPositive);
			} else {
                nppDailyStatus.setFinalBalance(nppDailyStatus.getFinalBalance().add(signedTxnQty));
				if(!isReversal){

	                if(nppDailyStatus.getFinalBalance().compareTo(nppDailyStatus.getMinimumBalance()) < 0){

	                	nppDailyStatus.setMinimumBalance(nppDailyStatus.getFinalBalance());
	                }
				} else {
					if(txnIsPositive){
	                	nppDailyStatus.setMinimumBalance(calculateMinForDay(nodeProgProdId, date, nppDailyStatus.getBeginningBalance()));
					}
				}

                updateNodeProdDailyStatus(nppDailyStatus, nodeProd, txnProduct, txnIsPositive, date, isReversal);
                updateSubsequentStatuses(nodeProd, date, signedTxnQty, txnProduct, txnIsPositive);
            }
		}

		private void updateSubsequentStatuses(NodeProduct nodeProd, Date date, BigDecimal signedTxnQty, TransactionProduct txnProd, Boolean txnIsPositive){
            nodeProdDAO.updateAllDailyBeginningBalancesAfterDate(nodeProd, date, signedTxnQty, txnProd, txnIsPositive);
            nodeProdDAO.updateAllDailyFinalBalancesSinceDate(nodeProd, DateCustomUtil.addDays(date,1) , signedTxnQty, txnProd, txnIsPositive);
            nodeProdDAO.updateAllDailyMinBalancesSinceDate(nodeProd,DateCustomUtil.addDays(date,1), signedTxnQty, txnProd, txnIsPositive);
        }

    private void updateNodeProdDailyStatus(NodeProgramProductDailyStatus nppDailyStatus, NodeProduct nodeProduct, TransactionProduct txnProduct, Boolean txnIsPositive, Date date, Boolean isReversal){
        nodeProdDAO.updateNodeProdDailyStatus(nppDailyStatus);

        for(int i = 0; i < nodeProduct.getNodeProgramProductBatches().size(); i++){
            BigDecimal signedBatchQty = (txnIsPositive)?txnProduct.getBatchQuantities().get(i).getQuantity():txnProduct.getBatchQuantities().get(i).getQuantity().negate();
            NodeProgramProductBatch nppb =  nodeProduct.getNodeProgramProductBatches().get(i);

            NodeProgramProductBatchDailyStatus nppbDS = findNPPBDailyStatusForNPBinNPPDailyStatus(nppDailyStatus, nppb);

            if (nppbDS == null){
                NodeProgramProductBatchDailyStatus newNPPBDS = createNPPBDS(nppb, signedBatchQty, date);
                nodeProdDAO.insertNodeProdBatchDailyStatus(newNPPBDS);
            } else {
                nppbDS.setFinalBalance(nppbDS.getFinalBalance().add(signedBatchQty));
                if(!isReversal) {
                    if (nppbDS.getFinalBalance().add(signedBatchQty).compareTo(nppbDS.getMinimumBalance()) < 0) {
                        nppbDS.setMinimumBalance(nppbDS.getFinalBalance());
                    }
                } else {
                    if(txnIsPositive) {
                        nppbDS.setMinimumBalance(calculateBatchMinForDay(nppb.getId(), date, nppbDS.getBeginnigBalance()));
                    }
                }
                nodeProdDAO.updateNodeProdBatchDailyStatus(nppbDS);
            }
        }
    }

    private NodeProgramProductBatchDailyStatus createNPPBDS(NodeProgramProductBatch nppb, BigDecimal signedBatchQty, Date date){
        NodeProgramProductBatchDailyStatus newNPPBDS = null;
        NodeProgramProductBatchDailyStatus lastNPPBDS = nodeProdDAO.getPreviousDailyNPPBatchStatus(nppb.getId(), date);

        if (lastNPPBDS == null){

            newNPPBDS = new NodeProgramProductBatchDailyStatus(null, nppb.getId(), BigDecimal.ZERO, signedBatchQty, null, null, date, BigDecimal.ZERO);

        } else {
            newNPPBDS = new NodeProgramProductBatchDailyStatus(null, nppb.getId(), lastNPPBDS.getFinalBalance(), lastNPPBDS.getFinalBalance().add(signedBatchQty),
                    null, null, date,
                    (signedBatchQty.compareTo(BigDecimal.ZERO) > 0 ? lastNPPBDS.getFinalBalance():lastNPPBDS.getFinalBalance().add(signedBatchQty)));
        }
        return  newNPPBDS;
    }

    private NodeProgramProductBatchDailyStatus findNPPBDailyStatusForNPBinNPPDailyStatus(NodeProgramProductDailyStatus nppds, NodeProgramProductBatch npb){
        for (NodeProgramProductBatchDailyStatus nppbds :
             nppds.getNodeProgramProductBatchDailyStatuses()) {

            if(nppbds.getNodeProgramProductBatchId().equals(npb.getId())){
                return nppbds;
            }
        }
        return null;
    }

		private NodeProgramProductDailyStatus createDailyStatusForTxn(Date date,NodeProduct nodeProd, BigDecimal signedTxnQty, TransactionProduct txnProduct,Boolean txnIsPositive){

            Integer nodeProgProdId = nodeProd.getId();

            NodeProgramProductDailyStatus nppDailyStatus = new NodeProgramProductDailyStatus();
            nppDailyStatus.setDate(date);
            nppDailyStatus.setNodeProgramProductId(nodeProgProdId);


            NodeProgramProductDailyStatus lastNPPDailyStatus = getLastNppDailyStatus(nodeProgProdId,date);// isn't getting the batch statuses

            if (lastNPPDailyStatus == null){  // No transaction on that nodeProd
                nppDailyStatus.setBeginningBalance(BigDecimal.ZERO);
                nppDailyStatus.setFinalBalance(signedTxnQty);
                nppDailyStatus.setMinimumBalance((nppDailyStatus.getFinalBalance().compareTo(nppDailyStatus.getBeginningBalance()) < 0)?nppDailyStatus.getFinalBalance():nppDailyStatus.getBeginningBalance());//TODO: Should be signedTxnQty? Probably not

                for (int i = 0; i < txnProduct.getBatchQuantities().size(); i++) {
                    //Assumes #TNPA
                    // (TNPA = TransactionProduct NodeProduct alignment, batches included)
                    NodeProgramProductBatch nppb = nodeProd.getNodeProgramProductBatches().get(i);
                    BigDecimal signedBatchQty = (txnIsPositive)?txnProduct.getBatchQuantities().get(i).getQuantity():txnProduct.getBatchQuantities().get(i).getQuantity().negate();

                    NodeProgramProductBatchDailyStatus nppBatchDailyStatus = new NodeProgramProductBatchDailyStatus(null, nppb.getId(), BigDecimal.ZERO, signedBatchQty, null, null, date, (signedBatchQty.compareTo(BigDecimal.ZERO) < 0)?signedBatchQty:BigDecimal.ZERO);
                    nppDailyStatus.getNodeProgramProductBatchDailyStatuses().add(nppBatchDailyStatus);
                }


            } else {
                nppDailyStatus.setBeginningBalance(lastNPPDailyStatus.getFinalBalance());
                nppDailyStatus.setFinalBalance(nppDailyStatus.getBeginningBalance().add(signedTxnQty));
                nppDailyStatus.setMinimumBalance((nppDailyStatus.getFinalBalance().compareTo(nppDailyStatus.getBeginningBalance()) < 0) ? nppDailyStatus.getFinalBalance() : nppDailyStatus.getBeginningBalance());

                for (int i = 0; i < txnProduct.getBatchQuantities().size(); i++) {
                    BigDecimal signedBatchQty = (txnIsPositive)?txnProduct.getBatchQuantities().get(i).getQuantity():txnProduct.getBatchQuantities().get(i).getQuantity().negate();

                    NodeProgramProductBatch nppb = nodeProd.getNodeProgramProductBatches().get(i);// Assumes #TNPA
                    NodeProgramProductBatchDailyStatus nppBatchDailyStatus = nodeProdDAO.getNPPBatchDAilyProgProdStatus(date, nppb);


                    if(nppBatchDailyStatus == null){
                        NodeProgramProductBatchDailyStatus lastNPPBatchDailyStatus = nodeProdDAO.getPreviousDailyNPPBatchStatus(nppb.getId(), date);

                        if (lastNPPBatchDailyStatus == null)
                            nppBatchDailyStatus = new NodeProgramProductBatchDailyStatus(null, nppb.getId(), BigDecimal.ZERO, signedBatchQty, null, null, date, (signedBatchQty.compareTo(BigDecimal.ZERO) < 0)?signedBatchQty:BigDecimal.ZERO);
                        else {
                            nppBatchDailyStatus = new NodeProgramProductBatchDailyStatus(null, nppb.getId(), lastNPPBatchDailyStatus.getFinalBalance(), lastNPPBatchDailyStatus.getFinalBalance().add(signedBatchQty),
                                    null, null, date,
                                    (signedBatchQty.compareTo(BigDecimal.ZERO) < 0 ? lastNPPBatchDailyStatus.getFinalBalance().add(signedBatchQty):lastNPPBatchDailyStatus.getFinalBalance()));
                        }
                        nppDailyStatus.getNodeProgramProductBatchDailyStatuses().add(nppBatchDailyStatus);

                    } else {
                        nppBatchDailyStatus.setFinalBalance(nppBatchDailyStatus.getFinalBalance().add(signedBatchQty));
                        if (nppBatchDailyStatus.getFinalBalance().compareTo(nppBatchDailyStatus.getMinimumBalance()) < 0){
                            nppBatchDailyStatus.setMinimumBalance(nppBatchDailyStatus.getFinalBalance());
                        }
                    }
                }

            }

            return nppDailyStatus;
        }





		private BigDecimal calculateMinForDay(Integer nppId, Date date, BigDecimal beginningBalance){
			List<BigDecimal> orderedTxnList = getOrderedTxnList(nppId, date);
			BigDecimal minBal = beginningBalance;
			BigDecimal curBal = beginningBalance;
			for (BigDecimal txnQty : orderedTxnList) {
				curBal = curBal.add(txnQty);
				minBal = (curBal.compareTo(minBal) < 0)?curBal:minBal;
			}
			return minBal;
		}

    private BigDecimal calculateBatchMinForDay(Integer nppbId, Date date, BigDecimal beginningBalance){
        List<BigDecimal> orderedTxnList = getOrderedTxnBatchList(nppbId, date);
        BigDecimal minBal = beginningBalance;
        BigDecimal curBal = beginningBalance;
        for (BigDecimal txnQty : orderedTxnList) {
            curBal = curBal.add(txnQty);
            minBal = (curBal.compareTo(minBal) < 0)?curBal:minBal;
        }
        return minBal;
    }

		private List<BigDecimal> getOrderedTxnList(Integer nppId, Date date){
			return txnDAO.getOrderedTxnQtyList(nppId, date);
		}

    private List<BigDecimal> getOrderedTxnBatchList(Integer nppbId, Date date){
        return txnDAO.getOrderedTxnQtyBatchList(nppbId, date);
    }

		private void saveNodeProductDailyTransactionTotals(Date date, Integer nodeProgProdId, Integer txnTypeId, BigDecimal txnQty, Boolean txnIsPositive){

		    BigDecimal signedTxnQuantity = (txnIsPositive)? txnQty.abs():txnQty.negate();

			NodeProgramPRoductDailyTransactionSummary nppDailyTxnSummary = getNPPDailyTxnSummary(date, txnTypeId, nodeProgProdId);

			if(nppDailyTxnSummary == null){

				nppDailyTxnSummary = new NodeProgramPRoductDailyTransactionSummary();

				nppDailyTxnSummary.setDate(date);
				nppDailyTxnSummary.setNodeProgramProductId(nodeProgProdId);
				nppDailyTxnSummary.setQuantity(signedTxnQuantity);
				nppDailyTxnSummary.setTxnTypeId(txnTypeId);

				nodeProdDAO.insertNPPDailyTxnSummary(nppDailyTxnSummary);

			} else {
				nppDailyTxnSummary.setQuantity(nppDailyTxnSummary.getQuantity().add(signedTxnQuantity));
                nodeProdDAO.updatePPDailyTxnSummary(nppDailyTxnSummary);
			}
		}

		private NodeProgramPRoductDailyTransactionSummary getNPPDailyTxnSummary(Date date, Integer txnTypeId, Integer nodeProgProdId){
			return nodeProdDAO.getNPPDailyTxnTotal(date , nodeProgProdId, txnTypeId);
		}

		private NodeProgramProductDailyStatus getNPPDailyNodeProgProdStatus(Date date, NodeProduct nodeProgProd){
		    return nodeProdDAO.getNPPDAilyProgProdStatus(date, nodeProgProd);
		}

		private NodeProgramProductDailyStatus getLastNppDailyStatus(Integer nodeProgProdId, Date date){
			// Is there ever a scenario where there isn't a previous one?
			return nodeProdDAO.getPreviousDailyStatus(nodeProgProdId, date);
		}

	private NodeProgramProductBatchDailyStatus getLastNppBatchDailyStatus(Integer nodeProgProdBatchId, Date date){
		// Is there ever a scenario where there isn't a previous one?
		return nodeProdDAO.getPreviousDailyNPPBatchStatus(nodeProgProdBatchId, date);//TODO:
	}

	@Override
	public Integer registerBulkConsumption(Node node , List<TransactionProduct> txnProducts , String txnType , Date txnDate , Integer userId , ActionOrder actionOrder, Boolean batchAware) throws UnavailableNodeProductException, InsufficientNodeProductException{

		Node consumer = nodeService.getNodeByName(Node.GENERIC_CONSUMER);
		Integer txnId = null;
		if(actionOrder == null){
			Integer order = getMaxActionOrderInADay(txnDate);
			ActionOrder ao  = new ActionOrder(order , ActionOrder.AFTER);
			txnId = performTransaction(node,consumer, txnProducts,txnType,txnDate ,  userId , ao , null, null, batchAware);
		} else {
			txnId = performTransaction(node, consumer, txnProducts,txnType,txnDate ,  userId , null, null, null, batchAware );
		}
		return txnId;
	}

	@Override
	public NodeProductDateBalance getBalanceOnDate(Date date , Integer nppId){
/*		Integer order = selectMaxNonReversalActionOrderOnADate(date);
		BigDecimal stockOnHand = nodeDAO.getPreviousStockOnHand(ppId, nodeId, order, ActionOrder.AFTER);
		List<Integer> actionOrders = actionDAO.getActionsAfterAction(nodeId, ppId , order);
		BigDecimal minSoH = stockOnHand;

		for (Integer aOrder : actionOrders) {
			BigDecimal stockOnHandAfterAction = nodeDAO.getPreviousStockOnHand(ppId, nodeId, aOrder, ActionOrder.AFTER);
			if(stockOnHandAfterAction.compareTo(minSoH) < 0 ){
				minSoH = stockOnHandAfterAction;
			}
		}
		return new NodeProductDateBalance(stockOnHand, minSoH);*/
        BigDecimal stockOnHand = nodeProdDAO.getBalance(date, nppId);
        BigDecimal minSoH = nodeProdDAO.getUsableBalance(date, nppId);

        List<NodeProgramProductBatchBalance> nppbBatchBalances = nodeProdDAO.getBatchBalancesOnDate(date, nppId);
        NodeProduct np = nodeProdDAO.selectNodeProductByPrimaryKey(nppId);
        return new NodeProductDateBalance(np,stockOnHand, (minSoH == null)?stockOnHand:minSoH, nppbBatchBalances);
	}

	public Map<String, NodeProductDateBalance> getNodeProductBalancesOnDate(
			Date date, Integer nodeId, Integer programId){
		// get node products
		List<NodeProduct> nodeProducts = nodeProdDAO.selectNodeProductsByProgram(nodeId, programId);
		Map<String, NodeProductDateBalance> balancesMap = new HashMap<>();
		for (NodeProduct nodeProduct : nodeProducts) {
			System.out.println(nodeProduct.getProgramProduct().getProduct().getCode());
			balancesMap.put(nodeProduct.getProgramProduct().getProduct().getCode(), getBalanceOnDate(date ,  nodeProduct.getId()));//TODO: prod code should be changed to nppid
		}
		return balancesMap;
	}

	@Override
	public Action getAction(Integer id){
		return actionDAO.getAction(id);
	}

	@Override
	public Integer getTransactionCountOnNodeProductAfterDate(Integer nodeId,
			Integer productId, Date txnDate) {
		return nodeProdDAO._selectTxnCountForNodeProductAfterDate(nodeId, productId, txnDate);
	}

	@Override
	public Integer getMaxActionOrderInADay(Date date){
		return txnDAO.maxActionOrderOnDate(date);
	}

	@Override
	public Integer selectMaxNonReversalActionOrderOnADate(Date date){
		return txnDAO.selectMaxNonReversalActionOrderOnADate(date);
	}

	@Override
	public TransactionDetails getTransactionDetails(Integer txnId) {

		Transaction txn = txnDAO.getTransaction(txnId);
		TransactionType txnType = txnDAO.getTransactionType(txn.getTransactionType());
		TransactionDetails txnDetails = txnDetailsFactory.createInstance(txnType.getName() , txn);
		txnDetails.setTransactionType(txnType);
//		List<NodeTransaction> nodeTxns = txnDAO.selectNodeTxnsByTxnId(txnId);
		return txnDetails;
	}

	@Override
	public Integer reverseStockTransferTransaction(Integer stockTransferId, Integer userId) throws InsufficientNodeProductException, UnavailableNodeProductException {
		for(Integer txnId : txnDAO.getTxnIdByStockTransferId(stockTransferId)){
			reverseTransaction(txnId, userId);
		}
		return 0;
	}

}
