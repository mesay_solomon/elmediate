/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.service.implementation;

import com.google.gson.Gson;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.dao.RegimenDAO;
import org.jsi.elmis.interfacing.openlmis.RnRSubmissionResult;
import org.jsi.elmis.model.*;
import org.jsi.elmis.model.adapters.RequisitionAdapter;
import org.jsi.elmis.quartz.job.facade.OfflineRequisitionFacade;
import org.jsi.elmis.rest.client.RnRInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rnr.OfflineRnRDTO;
import org.jsi.elmis.rnr.RegimenLineItem;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.interfaces.*;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.plaf.synth.SynthTextAreaUI;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by abebe on 4/17/2018.
 */
public abstract class FacilityRnRIntializerServiceAbs implements RnRIntializerService {
    @Autowired
    PropertiesService propertiesService;

    @Autowired
    RnRInterfacing rnRInterfacing;
    @Autowired
    RequisitionAdapter adapter;
    @Autowired
    OfflineRequisitionFacade offlineRequisitionFacade;
    @Autowired
    RnRService rnrService;
    @Autowired
    OfflineRnRService offlineRnRService;
    @Autowired
    RegimenDAO regimenDAO;
    @Autowired
    AdjustmentService adjustmentService;
    @Override
    public Requisition intializeRequisitionFromCentral(Long facilityId, Long programId, Long periodId, String sourceApplication, Boolean emergency) {
       System.out.println("emergency "+ emergency);
        Requisition intiatedRequisition = null;
        Rnr initiatedRnr = null;
        initiatedRnr = rnRInterfacing.initiateRnr(facilityId, programId, periodId, sourceApplication, emergency, "eLMIS_FE");
        if (initiatedRnr != null) {
            intiatedRequisition = adapter.convertRnr(initiatedRnr);
            intiatedRequisition = this.synchConsumptionPoints(initiatedRnr, intiatedRequisition);
        }
        return intiatedRequisition;
    }

    @Override
    public Requisition synchBalanceValue(Requisition generated, Requisition intiated) {

        if (generated != null && intiated != null) {
            generated.getRequisitionLineItems().forEach(generatedLineItem ->
                    intiated.getRequisitionLineItems().stream().filter(iLineItem -> iLineItem.getProductCode()
                            .equals(generatedLineItem.getProductCode())).map(li -> {
                                generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                                generatedLineItem.setBeginningBalance(li.getBeginningBalance());
                                return li;
                            }
                    ).collect(Collectors.toList())
            );
        }
        return generated;
    }

    protected Requisition synchConsumptionPoints(Rnr rnr, Requisition requisition) {
        if (requisition != null && rnr != null) {
            requisition.getRequisitionLineItems().forEach(generatedLineItem ->
                    rnr.getAllLineItems().stream().filter(iLineItem -> iLineItem.getProductCode()
                            .equals(generatedLineItem.getProductCode())).map(li -> {
                                generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                                return li;
                            }
                    ).collect(Collectors.toList())
            );
        }
        return requisition;
    }

    public RnRSubmissionResult submitOfflineRnR(Requisition requisition) {

        OfflineRnRDTO offlineRnRDTO = offlineRequisitionFacade.getOfflineRequisition(requisition);
        String installationType = propertiesService.getProperty("installation.type", false).getValue();
        String sourceApplication = installationType.equalsIgnoreCase("DM") ? "eLMIS_DM" : "eLMIS_FE";
        offlineRnRDTO.getRnr().setSourceApplication(sourceApplication);
        if(offlineRnRDTO.getRnr().getProgramCode().equals("ARV")) {
            offlineRnRDTO.getRnr().setRegimens(getBlankRegimenLineItems());
        }
        RnRSubmissionResult result = rnrService.submitOfflineRnR(offlineRnRDTO.getRnr());
        System.out.println("\n\n\n\n==============================\n\n\n" + new Gson().toJson(result) + "\n\n\n\n==============================\n\n\n");
        OfflineRnRStatus offlineRnRStatus = new OfflineRnRStatus(null, result.isSuccess() ? ELMISConstants.SUCCESSFUL.getValue() :
                ELMISConstants.FAILED.getValue(), offlineRnRDTO.getOfflineRequisition().getId(), result.getRemark());
        offlineRnRService.saveOfflineRnRStatus(offlineRnRStatus);
        if (result.isSuccess() || result.getRemark().equalsIgnoreCase("error.rnr.already.submitted.for.this.period")) {
            offlineRnRDTO.getOfflineRequisition().setStatus(ELMISConstants.SUBMITTED.getValue());
            offlineRnRService.updateRnR(offlineRnRDTO.getOfflineRequisition());
        }else{

        }

        return result;
    }
    public List<RegimenLineItem> getBlankRegimenLineItems() {
        List<RegimenLineItem> regimenLines = new ArrayList<RegimenLineItem>();

        List<Regimen> regimens = regimenDAO.getAllRegimens();
        for (Regimen regimen : regimens) {
            RegimenLineItem regLine = new RegimenLineItem();
            regLine.setCode(regimen.getCode());
            regLine.setName(regimen.getName());
            regimenLines.add(regLine);
        }
        return regimenLines;
    }

    protected Requisition intiateAmcValue(Requisition generated, Requisition intiated) {
System.out.println(" INTIATED VALUE IS "+ intiated.getRequisitionLineItems());
        if (generated != null && intiated != null) {
            generated.getRequisitionLineItems().forEach(generatedLineItem ->
                    intiated.getRequisitionLineItems().stream().filter(iLineItem -> iLineItem.getProductCode()
                            .equals(generatedLineItem.getProductCode())).map(li -> {
                        generatedLineItem.setPreviousNormalizedConsumptions(li.getPreviousNormalizedConsumptions());
                        generatedLineItem.setAmc(calculateAmc(generatedLineItem.getQuantityDispensed(), li.getPreviousNormalizedConsumptions(), li.getAmc()));
                       // generatedLineItem.setBeginningBalance(li.getBeginningBalance());

                        generatedLineItem.setMaxStockQuantity(generatedLineItem.getAmc().multiply(generatedLineItem.getMaxMonthsOfStock()));
                        BigDecimal qtyRequested = generatedLineItem.getMaxStockQuantity().subtract(generatedLineItem.getStockOnHand());
//                        generatedLineItem.setQuantityRequested(qtyRequested);
                        generatedLineItem.setCalculatedOrderQuantity(qtyRequested.intValue() >= 0 ? qtyRequested : BigDecimal.ZERO);
                        generatedLineItem.getRliLossesNAdjustments().stream().
                                forEach(rli->{
                                    LossAdjustmentType adjustmentType=getAdjustementByName(rli.getLossesAndAdjustmentType());
                                    rli.setAdjustmentType(adjustmentType);

                                });
                                return li;
                            }
                    ).collect(Collectors.toList())
            );
        }

        return generated;

    }

    private BigDecimal calculateAmc(BigDecimal dispensedQuantity, List<Integer> consumptions, BigDecimal intiatedAmc) {

        BigDecimal calcAmc = new BigDecimal(0);
        Integer sumOfConsumption = 0;
        BigDecimal denominator = new BigDecimal(3);
        if (consumptions != null && !consumptions.isEmpty()) {
            int size = consumptions.size();

            BigDecimal previousValues;
            for (int i = 0; i < size; i++) {

                sumOfConsumption += consumptions.get(i);
            }
            sumOfConsumption += dispensedQuantity.intValueExact();

            previousValues = new BigDecimal(sumOfConsumption);
            calcAmc = sumOfConsumption != null ? previousValues.divide(denominator, RoundingMode.UP) : new BigDecimal(0);

        }else
            calcAmc=intiatedAmc;
        return calcAmc;
    }

    public LossAdjustmentType getAdjustementByName(String name){
        System.out.println("name is " +name);
        List<LossAdjustmentType> lossAdjustmentTypeList=adjustmentService.getAllAdjustments();
      return  lossAdjustmentTypeList.stream()
              .filter(li->{
                  String adjName= li.getName().trim();
               return adjName.equalsIgnoreCase(name.trim());}).findAny().orElse(null);


    }
    public ProcessingPeriod getPeriod(ProcessingPeriod processingPeriod){
        ProcessingPeriod fullProcessingPeriod= null;
        fullProcessingPeriod= offlineRnRService.getPeriodInfo(processingPeriod);
        return  fullProcessingPeriod;
    }

}
