package org.jsi.elmis.controllers;

import org.jsi.elmis.reporting.ReportManager;
import org.jsi.elmis.reporting.ReportOutputOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ReportController {

    private static final String USER_ID = "USER_ID";

    private static final String PDF = "PDF";
    private static final String XLS = "XLS";
    private static final String HTML = "HTML";
    private static final String CSV = "CSV";

    @Autowired
    public ReportManager reportManager;


    @RequestMapping(value = "/report/download/{reportKey}/{outputOption}")
    public void exportReport(@PathVariable(value = "reportKey") String reportKey
            , @PathVariable(value = "outputOption") String outputOption
            , HttpServletRequest request
            , HttpServletResponse response){

        Integer userId = 1;//Integer.parseInt(request.getSession().getAttribute(USER_ID).toString());

        switch (outputOption.toUpperCase()) {
            case PDF:
                reportManager.showReport(userId, reportKey, request.getParameterMap(), ReportOutputOption.PDF, response);
                break;
            case XLS:
                reportManager.showReport(userId, reportKey, request.getParameterMap(), ReportOutputOption.XLS, response);
                break;
            case HTML:
                reportManager.showReport(userId, reportKey, request.getParameterMap(), ReportOutputOption.HTML, response);
                break;
            case CSV:
                reportManager.showReport(userId, reportKey, request.getParameterMap(), ReportOutputOption.CSV, response);
                break;
            default:
        }

    }

}
