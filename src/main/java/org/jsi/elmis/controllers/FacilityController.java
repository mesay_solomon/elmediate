/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.model.Facility;
import org.jsi.elmis.model.GeographicLevel;
import org.jsi.elmis.model.GeographicZone;
import org.jsi.elmis.model.LossAdjustmentType;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.rest.result.FacilityDistrictAndProvinceResult;
import org.jsi.elmis.service.interfaces.AdjustmentService;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.jsi.elmis.service.interfaces.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mesay S. Taye
 *
 */

@RestController
public class FacilityController {

	@Autowired
	NodeService nodeService;
	@Autowired
	AdjustmentService adjService;
	@Autowired
	MiscellaneousService cmnServices;

	@RequestMapping(value = "/rest-api/facility/{id}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
	public  Facility getFacilityById(@PathVariable(value = "id")Integer facilityId) {
		return cmnServices.getFacilityById(facilityId);
	}

	@RequestMapping(value = "/rest-api/facility/code/{code}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
	public  Facility getFacilityByCode(@PathVariable(value = "code")String code) {
		return cmnServices.getFacilityByCode(code);
	}

	@RequestMapping(value = "/all-facilities/{geoZoneId}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
	public  List<Facility> getAllFacilitiesByZoneId(@PathVariable(value = "geoZoneId")Integer geoZoneId) {
		return cmnServices.getAllFacilities(geoZoneId);
	}

    @RequestMapping(value = "/all-facilities", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
    public  List<Facility> getAllFacilities() {
        return cmnServices.getAllFacilities(null);
    }

	@RequestMapping(value = "/all-adjustments")
	public @ResponseBody List<LossAdjustmentType> getAllAdjustments() {
		return adjService.getAllAdjustments();
	}

	@RequestMapping(value = "/rest-api/adjustments/non-computer-generated")
	public @ResponseBody List<LossAdjustmentType> getNonComputerGeneratedLossAdjustmentTypes() {
		return adjService.getLossAdjustmentTypes();
	}

	@RequestMapping(value = "/rest-api/facility/my-district-and-province")
	public @ResponseBody FacilityDistrictAndProvinceResult getFacilityDistrictAndProvince() {
		return cmnServices.selectMyDistrictAndProvince();
	}

	@RequestMapping(value = "/rest-api/facility/all-periods/{scheduleCode}")
	public @ResponseBody List<ProcessingPeriod> getAllProcessingPeriods(@PathVariable(value = "scheduleCode") String scheduleCode) {
		return cmnServices.getAllProcessingPeriods(scheduleCode);
	}

    @RequestMapping(value = "/rest-api/facility/current-period/{scheduleCode}")
	public @ResponseBody ProcessingPeriod getCurrentProcessingPeriod(@PathVariable(value = "scheduleCode") String scheduleCode) {
		return cmnServices.getCurrentPeriod(scheduleCode);
	}

    @RequestMapping(value = "/rest-api/facility/previous-period/{scheduleCode}")
	public @ResponseBody ProcessingPeriod getPreviousProcessingPeriod(@PathVariable(value = "scheduleCode") String scheduleCode) {
		return cmnServices.getPreviousPeriod(scheduleCode);
	}

	@RequestMapping(value = "/rest-api/facility/all-geo-levels")
	public @ResponseBody List<GeographicLevel> getAllGeographicLeves() {
		return cmnServices.getAllGeographicLeves();
	}

	@RequestMapping(value = "/rest-api/facility/all-geo-zones")
	public @ResponseBody List<GeographicZone> getAllGeographicZones() {
		return cmnServices.getAllGeographicZones();
	}
}
