/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.jsi.elmis.notification.ELMISNotificationType;
import org.jsi.elmis.notification.reader.StockRequisitionNotification;
import org.jsi.elmis.notification.thread.ELMISNotificationThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class NotificationsController {
	
	@Autowired
	StockRequisitionNotification stockRequisitionNotification;
	
	@RequestMapping(value = "/rest-api/notifications/all/{supplierNodeId}/{after-time}", method = RequestMethod.GET, produces= MediaType.APPLICATION_JSON)
    public DeferredResult<List<ELMISNotificationType>>  getNotifications(@PathVariable(value="supplierNodeId") Integer nodeIdToBeNotified,
    		@PathVariable(value="after-time") Long afterTime, final HttpServletResponse response) {
		Date time = new Date();
		time.setTime(afterTime);
    	final DeferredResult<List<ELMISNotificationType>> result = new DeferredResult<>();
    	
    	result.onTimeout(new Runnable(){
    		@Override
    		public void run(){
    			result.setErrorResult("Request Timed out!");
    			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    		}
    	});
    	ELMISNotificationThread notificationsThread = new ELMISNotificationThread(stockRequisitionNotification, result, nodeIdToBeNotified, time);
    	new Thread(notificationsThread).start();
        return result;
    }

}
