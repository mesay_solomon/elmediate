/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.util.Date;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.common.constants.Constant;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.exceptions.InsufficientNodeProductException;
import org.jsi.elmis.exceptions.UnavailableHIVTestProductException;
import org.jsi.elmis.exceptions.UnavailableNodeProductException;
import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.rest.request.HIVTestKitChangeRequest;
import org.jsi.elmis.rest.request.HIVTestRequest;
import org.jsi.elmis.rest.result.HIVProductAvailabilityResult;
import org.jsi.elmis.rest.result.HIVTestDARResult;
import org.jsi.elmis.service.interfaces.HIVTestingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */

@Controller
@ComponentScan
public class HIVTestingController {

	@Autowired
	HIVTestingService hivTesting;

	@RequestMapping(value = "/rest-api/hiv/hiv-test", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
	public @ResponseBody Integer doHIVTest(@RequestBody HIVTestRequest request)
			throws UnavailableNodeProductException,
			InsufficientNodeProductException {
		return hivTesting.doHIVTest(request.getHivTest(), request.getNode(),
				request.getDate(), request.getUserId() , request.getActionOrder(), request.getBatchAware());
	}

	@RequestMapping(value = "/hiv-test-purposes")
	public @ResponseBody List<Constant> getHIVTestPurposes() {
		return hivTesting.getHIVTestPurposes();
	}

	@RequestMapping(value = "/hiv-test-results")
	public @ResponseBody List<Constant> getHIVTestResults() {
		return hivTesting.getHIVTestResults();
	}

	@RequestMapping(value = "/hiv-test-product-availability")
	public @ResponseBody HIVProductAvailabilityResult checkHIVProductsAvailability(
			@RequestParam(value = "nodeId", required = true) Integer nodeId) {

		return new HIVProductAvailabilityResult(
				hivTesting.checkAvailabilityOfProduct(
						ELMISConstants.SCREENING.getValue(), nodeId),
				hivTesting.checkAvailabilityOfProduct(
						ELMISConstants.CONFIRMATORY.getValue(), nodeId));
	}

	@RequestMapping(value = "/hiv-test-types")
	public @ResponseBody List<Constant> getHIVTestTypes() {
		return hivTesting.getHIVTestTypes();
	}

	@RequestMapping(value = "/rest-api/hiv/hiv-dar/{from}/{to}/{nodeId}")
	public @ResponseBody HIVTestDARResult getHIVDAR(
			@PathVariable(value = "from") Date from,
			@PathVariable(value = "to") Date to,
			@PathVariable(value = "nodeId") Integer nodeId)
			throws UnavailableHIVTestProductException,
			UnavailablePhysicalCountException, UnavailableNodeProductException {

		return hivTesting.generateHIVDAR(from, to, nodeId);

	}
	
	@RequestMapping(value = "/rest-api/hiv/next-client-number/{date}")
	public @ResponseBody String getNextClientNumber(
			@PathVariable(value = "date") Date date){
		return hivTesting.getNextClientNumber(date);
	}

    @RequestMapping(value = "/rest-api/hiv/test-kits/change", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody Boolean changeTestKits(@RequestBody HIVTestKitChangeRequest request){
        return hivTesting.saveHIVTestKits(request);
    }
}
