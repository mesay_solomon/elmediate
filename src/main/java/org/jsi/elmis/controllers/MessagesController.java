package org.jsi.elmis.controllers;

import org.jsi.elmis.rest.request.LocaleRequest;
import org.jsi.elmis.service.implementation.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class MessagesController{

  public static final String MESSAGES = "messages";

  @Autowired
  private MessageService messageService;

    @RequestMapping(value = "/messages", method = GET)
    public @ResponseBody Map<String, Map<String, String>> getAllMessages(){
        Map<String, Map<String, String>> result = messageService.getAllMessages();
        return result;
    }

    @RequestMapping(value = "/changeLocale", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody void changeLocale(@RequestBody LocaleRequest localeRequest) {
        messageService.setCurrentLocale(localeRequest.getLocaleCode());
    }

    @RequestMapping(value = "/locales", method = GET)
    public @ResponseBody Object getLocales(HttpServletRequest request) {
        return messageService.getLocales();
    }
}
