/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */

package org.jsi.elmis.controllers;

import java.sql.SQLException;
import java.util.List;

import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.model.TransactionType;
import org.jsi.elmis.service.interfaces.MiscellaneousService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 * 
 */

@Controller
public class CommonController {
	
	@Autowired
	MiscellaneousService cmnServices;
	
    @Value("${client.update.file.path}")
    private String clientUpdateFilePath;
 
	@RequestMapping(value="/rest-api/processing-period/{id}")
	@ResponseBody ProcessingPeriod getProcessingPeriodById(@PathVariable(value="id") Integer periodId){
		return cmnServices.getProcessingPeriod(periodId);
	}
	
	@RequestMapping(value="/rest-api/txn/types")
	@ResponseBody List<TransactionType> getTransactionTypes(){
		return cmnServices.getTransactionTypes();
	}
	
	@RequestMapping(value="/rest-api/misc/reorder-actions")
	@ResponseBody Boolean reorderActions() throws SQLException{
		return cmnServices.reorderActions();
	}
	
}
