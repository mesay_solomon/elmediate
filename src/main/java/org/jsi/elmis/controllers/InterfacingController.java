/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.jsi.elmis.common.constants.ELMISConstants;
import org.jsi.elmis.interfacing.openlmis.RnRSubmissionResult;
import org.jsi.elmis.interfacing.openlmis.SyncDB;
import org.jsi.elmis.interfacing.openlmis.SyncRequestResult;
import org.jsi.elmis.model.ApplicationProperty;
import org.jsi.elmis.model.DataChange;
import org.jsi.elmis.model.Requisition;
import org.jsi.elmis.model.criteria.RequisitionSearchCriteria;
import org.jsi.elmis.rest.client.WSClientInterfacing;
import org.jsi.elmis.rest.client.model.Rnr;
import org.jsi.elmis.rest.client.model.WSClientRestRequisitionRelay;
import org.jsi.elmis.rest.request.RequisitionSearchRequest;
import org.jsi.elmis.rest.result.RestResponse;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.factory.RnRServiceConfiguration;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRIntializerService;
import org.jsi.elmis.service.interfaces.RequisitionRelayService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.jsi.elmis.service.interfaces.StockStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Mesay S. Taye
 */
@Controller
@ComponentScan
public class InterfacingController {

    private final Logger log = Logger.getLogger(getClass());
    @Autowired
    PropertiesService propertiesService;
    @Autowired
    RnRService rnrService;
    @Autowired
    SyncDB syncDB;
    @Autowired
    WSClientInterfacing wsClientInterfacing;

    @Autowired
    WSClientRestRequisitionRelay wsClientRestRequisitionRelay;

    RnRIntializerService intializerService;
    @Autowired
    RnRServiceConfiguration serviceConfiguration;
    @Autowired
    StockStatusService dailyStockStatusUtil;

    private static final String CURRENT_VERSION_NUMBER = "CURRENT_DB_VERSION_NUMBER";

    @Autowired
    RequisitionRelayService requisitionRelayService;

    @RequestMapping(value = "rest-api/interfacing/rnr", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody
    RnRSubmissionResult submitRnR(@RequestBody RnR rnr, HttpServletRequest request) throws Exception {
        Boolean syncData = Boolean.valueOf(request.getHeader(ELMISConstants.SYNC_DB_BEFORE_RNR_IS_SENT));
        saveCentralUserCredsIfNotExists(request);
        if (syncData) {
            syncDB.syncFull();
            log.debug("trying to submit after sync ...");
            return rnrService.submitRnR(rnrService.generateRnROnTheFly(rnr.getPeriodId(), rnr.getProgramCode(), rnr.getEmergency(), "MONTHLY"));//TODO: what if schedule code changes?
        }
        return rnrService.submitRnR(rnr);
    }

    @RequestMapping(value = "rest-api/interfacing/rnr/skip/{programcode}/{periodid}")
    public @ResponseBody
    RnRSubmissionResult submitRnR(@PathVariable(value = "programcode") String programCode,
                                  @PathVariable(value = "periodid") Integer periodId) {
        return rnrService.skipRnR(programCode, periodId);
    }

    @RequestMapping(value = "rest-api/interfacing/sync")
    public @ResponseBody
    SyncRequestResult synchronizeDatabase(HttpServletRequest request) {
//        saveCentralUserCredsIfNotExists(request);
        String version = request.getHeader(CURRENT_VERSION_NUMBER);
        return syncDB.syncDelta(version);
//        return syncDB.syncFull();
    }

    @RequestMapping(value = "rest-api/interfacing/syncv2")
    public @ResponseBody
    SyncRequestResult synchronizeDatabaseV2(HttpServletRequest request) {
        String version = request.getHeader(CURRENT_VERSION_NUMBER);
        if(version != null){

        }
        return syncDB.syncFullTV();
    }

    @RequestMapping(value = "rest-api/interfacing/sync/lookup")
    public @ResponseBody
    List<DataChange> syncFromMediator(HttpServletRequest request) {
        String version = request.getHeader(CURRENT_VERSION_NUMBER);
        return syncDB.lookUpSyncData(version);
    }

	private void saveCentralUserCredsIfNotExists(HttpServletRequest request){
		List<ApplicationProperty> applicationPropertyList = new ArrayList<>();
		if(request.getHeader(ELMISConstants.LMU_APPROVER_USER)!= null){
			applicationPropertyList.add(new ApplicationProperty(ELMISConstants.LMU_APPROVER_USER, request.getHeader(ELMISConstants.LMU_APPROVER_USER), "String"));
		}
		if(request.getHeader(ELMISConstants.LMU_APPROVER_PASSWORD) != null){
			applicationPropertyList.add(new ApplicationProperty(ELMISConstants.LMU_APPROVER_PASSWORD, request.getHeader(ELMISConstants.LMU_APPROVER_PASSWORD),  "String"));
		}
		if(!applicationPropertyList.isEmpty()) {
			propertiesService.saveApplicationProperties(applicationPropertyList);
		}
	}

    @RequestMapping(value = "rest-api/interfacing/initiate-rnr", method = RequestMethod.POST)
    public @ResponseBody
    Requisition initiateRnr(@RequestParam("facilityId") Long facilityId,
                            @RequestParam("programId") Long programId,
                            @RequestParam("periodId") Long periodId,
                            @RequestParam("sourceApplication") String sourceApplication,
                            @RequestParam("emergency") Boolean emergency) {
        this.intializerService=this.serviceConfiguration.getIntializerService();
        return this.intializerService.intializeRequisition(facilityId, programId, periodId, sourceApplication, emergency);
    }

    @RequestMapping(value = "rest-api/interfacing/stock-status-submission", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public @ResponseBody
    void sendDailyStockStatus() throws Exception {
        dailyStockStatusUtil.sendDailyStockStatusToOpenELMIS();
    }

    @RequestMapping(value = "/rest-api/mediator/sdp-requisitions", method = POST, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<RestResponse> submitSDPRequisition(@RequestBody RnR report, Principal principal) {
        return requisitionRelayService.submitSdpReport(report, null);
    }

    @RequestMapping(value = "/rest-api/mediator/requisitions/initiate", method = POST, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<RestResponse> initiateRnr(@RequestParam("facilityId") Integer facilityId,
                                                    @RequestParam("programId") Integer programId,
                                                    @RequestParam("periodId") Integer periodId,
                                                    @RequestParam("sourceApplication") String sourceApplication,
                                                    @RequestParam("emergency") Boolean emergency,
                                                    Principal principal) {
        return requisitionRelayService.initiateRnr(facilityId, programId, emergency, periodId, sourceApplication);
    }


    @RequestMapping(value = "/rest-api/mediator/requisitions/search", method = POST, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<RestResponse> searchRnr(@RequestBody RequisitionSearchRequest requisitionSearchRequest, Principal principal) {
        return requisitionRelayService.searchRnrs(requisitionSearchRequest);
    }
    @RequestMapping(value = "/rest-api/mediator/logistics/periods", method = POST, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<RestResponse> getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(
            @RequestBody RequisitionSearchCriteria criteria, Principal principal) {
        return requisitionRelayService.getAllPeriodsForInitiatingRequisitionWithRequisitionStatus(criteria);
    }

    @RequestMapping(value = "/rest-api/lookup/{lookUpKey}", method = GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<RestResponse> getLookupData(
            @PathVariable("lookUpKey") String lookupKey) {
        return wsClientRestRequisitionRelay.getLookupData(lookupKey, true);
    }

}
