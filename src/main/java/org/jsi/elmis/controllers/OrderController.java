/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.jsi.elmis.model.Order;
import org.jsi.elmis.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@RequestMapping(value="/rest-api/orders/save", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON)
	public @ResponseBody Integer saveOrders(@RequestBody ArrayList<Order> orders){
		return orderService.saveOrders(orders);
	}
	
	@RequestMapping(value="/rest-api/orders/update", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON)
	public @ResponseBody Integer updateOrders(ArrayList<Order> orders){
		return orderService.updateOrders(orders);
	}
	
	@RequestMapping(value="/rest-api/orders/all", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	public @ResponseBody List getOrders(){
		return orderService.getAllOrders();
	}
}
