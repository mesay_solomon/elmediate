package org.jsi.elmis.controllers;

import org.jsi.elmis.model.FEUpdateInfo;
import org.jsi.elmis.service.interfaces.FEUpdateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class FEUpdateInfoController {

    private final String SUCCESSFUL = "Successful";

    @Autowired
    FEUpdateInfoService feUpdateInfoService;

    @RequestMapping(value = "/rest-api/fe-update-info", method = POST)
    public ResponseEntity<String> saveFEUpdateInfo(@RequestBody FEUpdateInfo feUpdateInfo){
        feUpdateInfoService.insert(feUpdateInfo);
        return ResponseEntity.ok(SUCCESSFUL);
    }


    @RequestMapping(value = "/rest-api/fe-update-info/{facilityCode}", method = GET)
    public ResponseEntity<List<FEUpdateInfo>> findByFacilityCode(@PathVariable(value = "facilityCode") String  facilityCode){
        return ResponseEntity.ok(feUpdateInfoService.findByFacilityCode(facilityCode));
    }


    @RequestMapping(value = "/rest-api/fe-update-info/all", method = GET)
    public ResponseEntity<List<FEUpdateInfo>> findAll(){
        return ResponseEntity.ok(feUpdateInfoService.findAll());
    }

}
