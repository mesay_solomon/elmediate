/*
 * This program was produced for the U.S. Agency for International Development. It was prepared 
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally 
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed 
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it 
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either 
 * version 2 of the License, or (at your option) any later version. This program is distributed 
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details. 
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
package org.jsi.elmis.controllers;

import org.jsi.elmis.exceptions.UnavailablePhysicalCountException;
import org.jsi.elmis.model.ProcessingPeriod;
import org.jsi.elmis.rnr.RnR;
import org.jsi.elmis.service.interfaces.PropertiesService;
import org.jsi.elmis.service.interfaces.RnRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mesay S. Taye
 *
 */
@Controller
public class RnRController {

	@Autowired
	RnRService rnrService;
	
	@Autowired
	PropertiesService propertiesService;


@RequestMapping(value = "/rest-api/rnr/generation/{programCode}/{periodId}/{emergency}/{schedulecode}")
	public @ResponseBody RnR generateRnR(
			@PathVariable(value="programCode") String programCode,
			@PathVariable(value="periodId") Integer periodId,
			@PathVariable(value="emergency") Boolean emergency,
			@PathVariable(value="schedulecode") String scheduleCode) throws UnavailablePhysicalCountException, Exception {
	
		return rnrService.generateRnROnTheFly(periodId, programCode, emergency , scheduleCode);
	}

@RequestMapping(value = "/rest-api/rnr/sendtohub")
	public @ResponseBody Boolean getARVDefaultPrefix() {
		return propertiesService.getSendRnRToHub();
	}

@RequestMapping(value = "/rest-api/rnr/latest-period-submitted-or-skipped-for/{programCode}")
public @ResponseBody ProcessingPeriod getLatestPeriodRnRisSubmittedOrSkippedFor(@PathVariable(value="programCode") String programCode) {
	return rnrService.getLatestPeriodRnRisSubmittedOrSkippedFor(programCode);
}

}
