/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jsi.elmis.notification.reader;

import java.util.Date;
import java.util.List;

import org.jsi.elmis.notification.ELMISNotificationReader;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.jsi.elmis.service.interfaces.StoreManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Mekbib
 */
@Component
public class StockRequisitionNotification implements ELMISNotificationReader<StockRequisitionResult> {

	@Autowired
	StoreManagementService storeMgmt;
	
	@Value("${notification.interval}")
	private Long checkTime;
	
    @Override
    public List<StockRequisitionResult> readNotifications(Integer nodeIdToBeNotified, Date afterTime) {
    	return storeMgmt.getStockRequestsBySupplierId(nodeIdToBeNotified, afterTime);
    }
    
    public Long getCheckTime(){
    	return this.checkTime;
    }
}
