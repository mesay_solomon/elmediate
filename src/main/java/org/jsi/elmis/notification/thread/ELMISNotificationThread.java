/*
 * This program was produced for the U.S. Agency for International Development. It was prepared
 * by the USAID | DELIVER PROJECT, Task Order 4. It is part of a project which utilizes code originally
 * licensed under the terms of the Mozilla Public License (MPL) v2 and therefore is licensed
 * under MPL v2 or later. This program is free software: you can redistribute it and/or modify it
 * under the terms of the Mozilla Public License as published by the Mozilla Foundation, either
 * version 2 of the License, or (at your option) any later version. This program is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Mozilla Public License for more details.
 * You should have received a copy of the Mozilla Public License along with this program.
 * If not, see http://www.mozilla.org/MPL/
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jsi.elmis.notification.thread;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsi.elmis.notification.ELMISNotification;
import org.jsi.elmis.notification.ELMISNotificationType;
import org.jsi.elmis.notification.reader.StockRequisitionNotification;
import org.jsi.elmis.rest.result.StockRequisitionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.async.DeferredResult;

/**
 *
 * @author Mekbib
 */
public class ELMISNotificationThread implements ELMISNotification<ELMISNotificationType>{
	private static final Logger logger = LoggerFactory.getLogger(ELMISNotificationThread.class);
	StockRequisitionNotification stockRequisitionNotification;
    DeferredResult<List<ELMISNotificationType>> deferredResult;
    Integer nodeToBeNotified;
    Date afterTime;
    
    List<ELMISNotificationType> allNotifications = new ArrayList<>();
    List<StockRequisitionResult> stockRequisitionResults = null;
    
    
	public ELMISNotificationThread(
			StockRequisitionNotification stockRequisitionNotification,
			DeferredResult<List<ELMISNotificationType>> deferredResult,
			Integer nodeToBeNotified,
			Date afterTime) {
		super();
		this.stockRequisitionNotification = stockRequisitionNotification;
		this.deferredResult = deferredResult;
		this.nodeToBeNotified = nodeToBeNotified;
		this.afterTime = afterTime;
	}

	@Override
    public void run() {
        setResult(deferredResult);
    }

    @Override
    public void setResult(DeferredResult<List<ELMISNotificationType>> deferredResult) {
    	boolean isResultSet =false;
        if(deferredResult.isSetOrExpired()){
        	logger.info("The request has expired");
        }else{
        	List<ELMISNotificationType> notifications = collectAllNotifications();
        	
        	if(notifications.size() > 0){
        		isResultSet = deferredResult.setResult(notifications);
        	}
        	
        	
            if(isResultSet){
            	logger.info("The result has been set successfully");
            }else{
            	logger.error("An error occurred while trying to set the result");
            }
        }
    }

    @Override
    public List<ELMISNotificationType> collectAllNotifications() {
        stockRequisitionResults =  stockRequisitionNotification.readNotifications(nodeToBeNotified, afterTime);
        //stock requisition notifications
        boolean hasNotifications = stockRequisitionResults.size() > 0;
        while(!hasNotifications && !deferredResult.isSetOrExpired()){
        	try {
				Thread.sleep(stockRequisitionNotification.getCheckTime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        	stockRequisitionResults =  stockRequisitionNotification.readNotifications(nodeToBeNotified, afterTime);
        	hasNotifications = stockRequisitionResults.size() > 0;
        }
        allNotifications.addAll(stockRequisitionResults);
        //TODO: add other notification readers here
        return allNotifications;
    }


  
    
}
