package org.jsi.elmis.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath*:applicationContext-core.xml"})
public class ApplicationContext {
}
